function(PARC_GENERATE SOURCE DESTINATION_DIR CODE_GENERATOR QUERY_GENERATOR)

  cmake_parse_arguments(parc_generate_arguments "" "" "GENERATED_FILES;DEPENDS;EXTRA_FILES;PLUGINS_DIRS" ${ARGN})

  
  if(parc_generate_arguments_PLUGINS_DIRS)
    list(APPEND PLUGINS_DIRS_CMD_ARG --plugins-dirs ${parc_generate_arguments_PLUGINS_DIRS})
  endif()
  
  add_custom_command(OUTPUT ${parc_generate_arguments_GENERATED_FILES}
                    DEPENDS ${SOURCE} ${parc_generate_arguments_DEPENDS} ${PARC_EXECUTABLE}
                    COMMAND ${PARC_EXECUTABLE} ${PLUGINS_DIRS_CMD_ARG} ${CODE_GENERATOR} ${QUERY_GENERATOR} ${SOURCE} ${DESTINATION_DIR}
  )

  add_custom_command(OUTPUT ${DESTINATION_DIR}/${SOURCE}
                  COMMAND cmake -E make_directory ${DESTINATION_DIR})

  foreach(extra_file ${parc_generate_arguments_EXTRA_FILES})
    get_filename_component(extra_file_name ${extra_file} NAME)
    
    if(${CMAKE_VERSION} VERSION_LESS "3.19.0") 
      set(extra_file_absolute ${CMAKE_CURRENT_SOURCE_DIR}/${extra_file})
      if(NOT PARC_GENERATE_WARN_VERSION_LESS)
        set(PARC_GENERATE_WARN_VERSION_LESS true CACHE INTERNAL "" FORCE)
        message(WARNING "For cmake < 3.19.0, parc_generate only accept extra files relative to current source dir")
      endif()
    else()
      file(REAL_PATH ${extra_file} extra_file_absolute BASE_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
    endif()
    
    if(${UNIX})
      add_custom_command(OUTPUT ${DESTINATION_DIR}/${extra_file_name}
                        COMMAND ${CMAKE_COMMAND} -E create_symlink ${extra_file_absolute} ${DESTINATION_DIR}/${extra_file_name})
    else()
      add_custom_command(OUTPUT ${DESTINATION_DIR}/${extra_file_name}
                        DEPENDS ${extra_file_absolute}
                        COMMAND ${CMAKE_COMMAND} -E copy ${extra_file_absolute} ${DESTINATION_DIR}/${extra_file_name})
    endif()
    # Need to append the dependency to each generated files
    foreach(generated_file ${parc_generate_arguments_GENERATED_FILES})
      set_property(SOURCE ${generated_file} APPEND PROPERTY OBJECT_DEPENDS ${DESTINATION_DIR}/${extra_file_name} )
    endforeach()
  endforeach()

endfunction()
