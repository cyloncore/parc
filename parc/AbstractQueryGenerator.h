/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _PARC_ABSTRACTQUERYGENERATOR_H_
#define _PARC_ABSTRACTQUERYGENERATOR_H_

class QString;
#include <QList>

namespace parc
{
  namespace definitions
  {
    class AbstractClass;
    class Class;
    class Field;
    class Mapping;
    class View;
  }
  class AbstractQueryGenerator
  {
  public:
    enum class Feature
    {
      Copy
    };
    Q_DECLARE_FLAGS(Features, Feature)
  public:
    class AbstractFactory
    {
    public:
      virtual AbstractQueryGenerator* create() const = 0;
    };
    template<typename _T_>
    class Factory : public AbstractFactory
    {
    public:
      virtual AbstractQueryGenerator* create() const { return new _T_(); }
    };
  public:
    AbstractQueryGenerator(Features _features);
    virtual ~AbstractQueryGenerator();
    Features features() const;
  public:
    virtual QStringList initialisationQueries() const = 0;
    virtual QString versionQuery() const = 0;
    virtual QString setVersionQuery() const = 0;
    virtual QString createInfoTable() const = 0;
    virtual QString createTable(const definitions::Class* _class) const = 0;
    virtual QString dropTable(const definitions::Class* _class) const = 0;
    virtual QString createView(const definitions::View* _view) const = 0;
    virtual QString createIndex(const definitions::Class* _class, const QList<const definitions::Field*>& _field) const = 0;
    virtual QString createUpdatedTrigger(const definitions::Class* _class) const = 0;
    virtual QString deleteUpdatedTrigger(const definitions::Class* _class) const = 0;
    virtual QString checkTableExistence(const definitions::Class* _class) const = 0;
    virtual QString createJournalTable(const definitions::Class* _class) const = 0;
    virtual QString createMappingTable(const definitions::Mapping* _mapping) const = 0;
    virtual QString createMappingJournalTable(const definitions::Mapping* _mapping) const = 0;
    virtual QString createTreeTable(const definitions::Class* klass) const = 0;
    virtual QString createJournalTreeTable(const definitions::Class* klass) const = 0;
    virtual QString generateRemoveAll(const definitions::Class* _class) const = 0;
    virtual QString generateSelect(const definitions::AbstractClass* _class) const = 0;
    virtual QString generateSelectCount(const definitions::AbstractClass* _class) const = 0;
    virtual QString generateSelectOrderBy(const definitions::AbstractClass* _class) const = 0;
    virtual QString generateLimit() const = 0;
    virtual QString generateOffsetLimit() const = 0;
    virtual QString generateOffset() const = 0;
    virtual QString generateRandomOrder() const = 0;
    virtual QString generateOrderAsc(const definitions::Field* _field) const = 0;
    virtual QString generateOrderDsc(const definitions::Field* _field) const = 0;
    virtual QString generateRefreshQuery(const definitions::Class* _class) const = 0;
    virtual QString generateRecordQuery(const definitions::Class* _class) const = 0;
    virtual QString generateDeleteQuery(const definitions::Class* _class) const = 0;
    virtual QString generateSelectJournalQuery(const definitions::Class* _class) const = 0;
    virtual QString generateRecordJournalUpdateQuery(const definitions::Class* _class) const = 0;
    virtual QString generateRecordJournalInsertQuery(const definitions::Class* _class) const = 0;
    virtual QString generateRecordJournalDeleteQuery(const definitions::Class* _class) const = 0;
    virtual QString generateWhereLike() const = 0;
    virtual QString generateWhereNEq() const = 0;
    virtual QString generateWhereGTE() const = 0;
    virtual QString generateWhereLTE() const = 0;
    virtual QString generateWhereLT() const = 0;
    virtual QString generateWhereGT() const = 0;
    virtual QString generateWhereEq() const = 0;
    virtual QString generateDateTimeToQuery() const = 0;
    virtual QString generateExistQuery(const definitions::Class* _class) const = 0;
    virtual QString generateCreateQuery(const definitions::Class* _klass, bool _include_auto_field_in_query) const = 0;
    virtual bool createReturnValues() const = 0;
    virtual QString generateCopyQuery(const definitions::Class* _klass, bool _include_auto_field_in_query) const = 0;
    virtual QString generateTreeCreateQuery(const definitions::Class* _klass) const = 0;
    virtual QString generateTreeChildrenQuery(const definitions::Class* _klass) const = 0;
    virtual QString generateTreeParentsQuery(const definitions::Class* _klass) const = 0;
    virtual QString generateTreeAddChildQuery(const definitions::Class* _klass) const = 0;
    virtual QString generateTreeAddChildcJournalQuery(const definitions::Class* _klass) const = 0;
    virtual QString generateTreeIsChildQuery(const definitions::Class* _klass) const = 0;
    virtual QString generateGetMappingsQuery(const definitions::Mapping* _mapping, const definitions::Class* _class) const = 0;
    virtual QString generateRemoveMappingQuery(const definitions::Mapping* _mapping, const definitions::Class* _class) const = 0;
    virtual QString generateAddMappingQuery(const definitions::Mapping* _mapping, const definitions::Class* _class) const = 0;
    virtual QString generateHasMappingQuery(const definitions::Mapping* _mapping, const definitions::Class* _class) const = 0;
    virtual QString generateMappingUpdateJournalQuery(const definitions::Mapping* _mapping) const = 0;
    virtual QString generateLockQuery() const = 0;
    virtual QString tableName(const definitions::Class* _class) const = 0;
    virtual QString viewName(const definitions::View* _view) const = 0;
    virtual QString fieldName(const definitions::Field* _field) const = 0;
    virtual QString generateTrue() const = 0;
    /**
     * Get the rowid for a class depending on its keys. Only available for SQLite.
     */
    virtual QString generateGetRowID(const definitions::Class* _klass) const = 0;
    /**
     * Get the rowid for a class depending on its keys. Only available for SQLite.
     */
    virtual QString generateSelectFromRowID(const definitions::Class* _klass) const = 0;
  private:
    struct Private;
    Private* const d;
  };
}

#endif
