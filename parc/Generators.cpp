/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Generators.h"
#include <QString>

using namespace parc;

QHash<QString, AbstractQueryGenerator::AbstractFactory*> Generators::s_name2query;
QHash<QString, AbstractCodeGenerator::AbstractFactory*> Generators::s_name2code;


AbstractCodeGenerator* Generators::createCodeGenerator(const QString& _name, AbstractQueryGenerator* _aqg)
{
  const AbstractCodeGenerator::AbstractFactory* f = s_name2code.value(_name, 0);
  if(f)
  {
    return f->create(_aqg);
  } else {
    return 0;
  }
}

AbstractQueryGenerator* Generators::createQueryGenerator(const QString& _name)
{
  const AbstractQueryGenerator::AbstractFactory* f = s_name2query.value(_name, 0);
  if(f)
  {
    return f->create();
  } else {
    return 0;
  }
}

void Generators::add(const QString& _name, AbstractCodeGenerator::AbstractFactory* _factory)
{
  s_name2code[_name] = _factory;
}

void Generators::add(const QString& _name, AbstractQueryGenerator::AbstractFactory* _factory)
{
  s_name2query[_name] = _factory;
}
