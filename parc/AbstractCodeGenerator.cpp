/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractCodeGenerator.h"

#include "AbstractQueryGenerator.h"
#include "definitions/Database.h"
#include "definitions/Class.h"

using namespace parc;

struct AbstractCodeGenerator::Private
{
  AbstractQueryGenerator* queryGenerator;
};

AbstractCodeGenerator::AbstractCodeGenerator(AbstractQueryGenerator* _aqg) : d(new Private)
{
  d->queryGenerator = _aqg;
}

AbstractCodeGenerator::~AbstractCodeGenerator()
{
  delete d->queryGenerator;
  delete d;
}


void AbstractCodeGenerator::generate(const definitions::Database* _database, const QString& _destination)
{
  generateDatabase(_database, _destination);
  for(const definitions::Class* klass : _database->classes())
  {
    Q_ASSERT(not klass->isJournaled() or _database->hasJournal());
    generateClass(_database, klass, _destination);
    for(const definitions::Class* sklass : klass->classOfSubDefinitions())
    {
      generateClass(_database, sklass, _destination);
    }
  }
  for(const definitions::View* view : _database->views())
  {
    generateView(_database, view, _destination);
  }
  if(_database->hasJournal())
  {
    generateJournal(_database, _destination);
  }
}

const AbstractQueryGenerator* AbstractCodeGenerator::queryGenerator() const
{
  return d->queryGenerator;
}
