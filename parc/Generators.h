/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QHash>

#include "AbstractCodeGenerator.h"
#include "AbstractQueryGenerator.h"

namespace parc
{
  class Generators
  {
  public:
    static AbstractQueryGenerator* createQueryGenerator(const QString& _name);
    static AbstractCodeGenerator*  createCodeGenerator (const QString& _name, AbstractQueryGenerator* _aqg);
    static void add(const QString& _name, AbstractQueryGenerator::AbstractFactory* _factory);
    static void add(const QString& _name, AbstractCodeGenerator::AbstractFactory* _factory);
    template<typename _T_, typename... _TArgs_>
    static void add(const QString& _name, _TArgs_... _args)
    {
      add(_name, new typename _T_::template Factory<_T_, _TArgs_...>(_args...));
    }
  private:
    static QHash<QString, AbstractQueryGenerator::AbstractFactory*> s_name2query;
    static QHash<QString, AbstractCodeGenerator::AbstractFactory*> s_name2code;
  };
}
