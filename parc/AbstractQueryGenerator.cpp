/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "AbstractQueryGenerator.h"

using namespace parc;

struct AbstractQueryGenerator::Private
{
  Features features;
};

AbstractQueryGenerator::AbstractQueryGenerator(Features _features) : d(new Private)
{
  d->features = _features;
}

AbstractQueryGenerator::~AbstractQueryGenerator()
{
  delete d;
}
