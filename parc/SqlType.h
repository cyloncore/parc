/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _PARC_SQLTYPE_H_
#define _PARC_SQLTYPE_H_

class QString;

namespace parc
{
  namespace definitions
  {
    class Class;
    class Enum;
    class Flags;
  }

  class SqlType
  {
    friend class definitions::Class;
    friend class definitions::Enum;
    friend class definitions::Flags;
  public:
    static const SqlType* boolean;
    static const SqlType* integer;
    static const SqlType* float32;
    static const SqlType* float64;
    static const SqlType* string;
    static const SqlType* datetime;
    static const SqlType* blob;
  public:
    enum Type
    {
      BOOLEAN, INTEGER, FLOAT32, FLOAT64, STRING, DATETIME, REFERENCE, BLOB, CUSTOM, SQLQUERY, ENUM, FLAG
    };
  private:
    SqlType(Type _type);
    SqlType(Type _type, const QString& _cppType);
  public:
    static SqlType* createCustom(const QString& _sqlType, const QString& _cppType);
    static SqlType* createSqlQuery(const QString& _sqlType, const QString& _cppType);
    virtual ~SqlType();
    Type type() const;
    QString sqlType() const;
    QString sqlQuery() const;
    QString cppType() const;
  private:
    struct Private;
    Private* const d;
  };
}

#endif
