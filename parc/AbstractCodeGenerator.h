/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _PARC_ABSTRACTCODEGENERATOR_H_
#define _PARC_ABSTRACTCODEGENERATOR_H_

#include <tuple>

class QString;

namespace parc
{
  namespace definitions
  {
    class Class;
    class Database;
    class View;
  }

  class AbstractQueryGenerator;
  namespace details
  {
    template < unsigned int N >
    struct FactoryCreateHelper
    {
      template < typename T, typename... ArgsT, typename... Args >
      static T* create(AbstractQueryGenerator* _aqg,
                              const std::tuple<ArgsT...>& t,
                              Args... args )
      {
        return FactoryCreateHelper<N-1>::template create<T>( _aqg, t, std::get<N-1>( t ), args... );
      }
    };

    template <>
    struct FactoryCreateHelper<0>
    {
      template < typename T, typename... ArgsT, typename... Args >
      static T* create( AbstractQueryGenerator* _aqg,
                              const std::tuple<ArgsT...>& /* t */,
                              Args... args )
      {
        return new T(_aqg, args...);
      }
    };
  }
  class AbstractCodeGenerator
  {
  public:
    class AbstractFactory
    {
    public:
      virtual AbstractCodeGenerator* create(AbstractQueryGenerator* _aqg) const = 0;
    };
    template<typename _T_, typename... _TArgs_>
    class Factory : public AbstractFactory
    {
    public:
      Factory(_TArgs_... _args) : m_args(_args...)
      {
      }
      virtual AbstractCodeGenerator* create(AbstractQueryGenerator* _aqg) const { return details::FactoryCreateHelper<sizeof...(_TArgs_)>::template create<_T_>(_aqg, m_args); }
    private:
      std::tuple<_TArgs_...> m_args;
    };
  public:
    AbstractCodeGenerator(AbstractQueryGenerator* _aqg);
    virtual ~AbstractCodeGenerator();
    virtual void generate(const definitions::Database* _database, const QString& _destination);
    virtual bool canGenerate(const definitions::Database* _database, QString* _reason) = 0;
  protected:
    virtual void generateDatabase(const definitions::Database* _database, const QString& _destination) = 0;
    virtual void generateJournal(const definitions::Database* _database, const QString& _destination) = 0;
    virtual void generateClass(const definitions::Database* _database, const definitions::Class* _klass, const QString& _destination) = 0;
    virtual void generateView(const definitions::Database* _database, const definitions::View* _view, const QString& _destination) = 0;
    const AbstractQueryGenerator* queryGenerator() const;
  private:
    struct Private;
    Private* const d;
  };
}

#endif
