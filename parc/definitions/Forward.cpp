/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Forward.h"

using namespace parc::definitions;

struct Forward::Private
{
  QStringList namespaces;
  QString name;
};

Forward::Forward(const QStringList& _namespaces, const QString& _name) : d(new Private)
{
  d->namespaces = _namespaces;
  d->name       = _name;
}

Forward::~Forward()
{
  delete d;
}

QString Forward::name() const
{
  return d->name;
}

QStringList Forward::namespaces() const
{
  return d->namespaces;
}
