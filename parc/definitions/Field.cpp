/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Field.h"

#include <QString>
#include <QVariant>

using namespace parc::definitions;

struct Field::Private
{
  QString               name;
  const parc::SqlType*  sqlType;
  Field::Options        options;
  const Typedef*        typedefinition;
  QString               defaultValue;
  Journaled             journaled;
  Access                access;
};

Field::Field(const QString& _name, const parc::SqlType* _sqlType, const Field::Options& _options, const Typedef* _typedef, Journaled _journaled, const QString& _defaultValue, Access _access) : d(new Private)
{
  d->name           = _name;
  d->sqlType        = _sqlType;
  d->options        = _options;
  d->typedefinition = _typedef;
  d->journaled      = _journaled;
  d->defaultValue   = _defaultValue;
  d->access         = _access;
  Q_ASSERT(d->journaled != HAS_JOURNALED_CHILDREN);
  Q_ASSERT(d->journaled != JOURNALED or not d->options.testFlag(Constant) or not d->options.testFlag(Key));
}

Field::~Field()
{

}

QString Field::name() const
{
  return d->name;
}

Field::Options Field::options() const
{
  return d->options;
}

const parc::SqlType* Field::sqlType() const
{
  return d->sqlType;
}

const Typedef* Field::typedefinition() const
{
  return d->typedefinition;
}

QString Field::defaultValue() const
{
  return d->defaultValue;
}

Journaled Field::journaled() const
{
  return d->journaled;
}

bool Field::isJournaled() const
{
  return d->journaled == JOURNALED;
}
