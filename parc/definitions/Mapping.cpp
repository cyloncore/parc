/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Mapping.h"

#include <QDebug>

using namespace parc::definitions;

struct Mapping::Private
{
  const Class* a;
  const Class* b;
  Journaled journaled;
};

Mapping::Mapping(const Class* _a, const Class* _b, Journaled _journaled) : d(new Private)
{
  d->a = _a;
  d->b = _b;
  d->journaled = _journaled;
}

Mapping::~Mapping()
{
  delete d;
}

const Class* Mapping::a() const
{
  return d->a;
}

const Class* Mapping::b() const
{
  return d->b;
}

const Class* Mapping::other(const Class* _c) const
{
  if(_c == d->a) return d->b;
  Q_ASSERT(_c == d->b);
  return d->a;
}

Journaled Mapping::journaled() const
{
  return d->journaled;
}

bool Mapping::isJournaled() const
{
  return d->journaled == JOURNALED;
}
