/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QList>
#include <QPair>

#include "../Journaled.h"
#include "Interfaces.h"

class QString;

namespace parc
{
  class SqlType;
  namespace definitions
  {
    class Class;
    class Enum;
    class Forward;
    class Mapping;
    class Object;
    class Property;
    class Typedef;
    class View;
    class Database
    {
    public:
      Database();
      ~Database();
      Class* getOrCreateClass(const QString& _className);
      Class* getClass(const QString& _className);
      void add(Typedef* _typeDef);
      void add(Object* _object);
      void add(Forward* _forward);
      void add(Property* _property);
      void add(Class* _motherClass, Class* _childClass);
      void add(Enum* _enum);
      void add(View* _view);
      const SqlType* sqlType(const QString& _name);
      const Typedef* typedef_(const QString& _name);
      void setNamespaces(const QStringList& _namespace);
      QStringList namespaces() const;
      void map(parc::definitions::Class* _a, parc::definitions::Class* _b, Journaled _journaled);
      QList<const Mapping*> mappings() const;
      const Typedef* typedefinition(const QString& _typedefName) const;
      QList<const Typedef*> typedefs() const;
      const Class* klass(QString _className) const;
      QList<const Class*> classes() const;
      QList<const Forward*> forwards() const;
      QList<const Object*> objects() const;
      QList<const Property*> properties() const;
      QList<const Enum*> enums() const;
      QList<const View*> views() const;
      bool hasJournal() const;
      void markImplement(Interface _interface);
      bool doesImplement(Interface _interface) const;
    private:
      struct Private;
      Private* const d;
    };
  }
}
