/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Property.h"

using namespace parc::definitions;

struct Property::Private
{
  const Typedef* definitions;
  const parc::SqlType* sqlType;
  QString name;
  QStringList value;
  bool constant;
};

Property::Property(const parc::SqlType* _sqlType, const QString& _name, const QStringList& _value, bool _constant) : d(new Private)
{
  d->sqlType      = _sqlType;
  d->definitions  = 0;
  d->name         = _name;
  d->value        = _value;
  d->constant     = _constant;
}

Property::Property(const Typedef* _definitions, const QString& _name, const QStringList& _value, bool _constant) : d(new Private)
{
  d->sqlType      = 0;
  d->definitions  = _definitions;
  d->name         = _name;
  d->value        = _value;
  d->constant     = _constant;
}

Property::~Property()
{
  delete d;
}

const Typedef* Property::typedefinition() const
{
  return d->definitions;
}

bool Property::isConstant() const
{
  return d->constant;
}

QString Property::name() const
{
  return d->name;
}

const parc::SqlType* Property::sqlType() const
{
  return d->sqlType;
}

QStringList Property::value() const
{
  return d->value;
}
