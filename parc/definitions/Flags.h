/*
 *  Copyright (c) 2017 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "../SqlType.h"

#include <QStringList>

namespace parc 
{
  namespace definitions
  {
    class Class;
    class Flags : public SqlType
    {
    public:
      Flags(const QString& _name, const QString& _enum_name, const QStringList& _values, const Class* _parent = nullptr);
      virtual ~Flags();
      const Class* parent() const;
      QString enumName() const;
      QString name() const;
      QString fullName() const;
      QStringList values() const;
    private:
      struct Private;
      Private* const d;
    };
  }
}
