/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "../Journaled.h"

namespace parc
{
  namespace definitions
  {
    class Class;
    class Mapping
    {
    public:
      Mapping(const Class* _a, const Class* _b, Journaled _journaled);
      ~Mapping();
      Journaled journaled() const;
      bool isJournaled() const;
      const Class* a() const;
      const Class* b() const;
      const Class* other(const Class* _c) const;
    private:
      struct Private;
      Private* const d;
    };
  }
}
