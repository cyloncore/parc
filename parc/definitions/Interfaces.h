/*
 *  Copyright (c) 2014 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _PARC_DEFINITIONS_INTERFACES_H_
#define _PARC_DEFINITIONS_INTERFACES_H_

namespace parc
{
  namespace definitions
  {
    enum Interface
    { 
      INIT, CLEANUP, EXTENDED, NOTIFICATIONS, UPDATENOTIFICATION,
      
      INVALID_INTERFACE = 255 // Should always be last
    };
  }
}

#endif
