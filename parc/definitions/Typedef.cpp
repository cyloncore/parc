/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Typedef.h"

#include <QString>

using namespace parc::definitions;

struct Typedef::Private
{
  QString alias, cpptype;
  const parc::SqlType* sqltype;
  const Database* database;
};

Typedef::Typedef(const QString& _alias, const QString& _cpptype, const Database* _database, const parc::SqlType* _sqltype) : d(new Private)
{
  d->alias    = _alias;
  d->cpptype  = _cpptype;
  d->sqltype  = _sqltype;
  d->database = _database;
  Q_ASSERT(d->sqltype);
}

Typedef::~Typedef()
{
  delete d;
}

QString Typedef::alias() const
{
  return d->alias;
}

QString Typedef::cpptype() const
{
  return d->cpptype;
}

const parc::SqlType* Typedef::sqltype() const
{
  return d->sqltype;
}

const Database* Typedef::database() const
{
  return d->database;
}
