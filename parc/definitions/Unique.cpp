/*
 *  Copyright (c) 2019 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Unique.h"

using namespace parc::definitions;

struct Unique::Private
{
  QList< const Field* > fields;
};

Unique::Unique(const QList< const Field* > _fields) : d(new Private)
{
  d->fields = _fields;
}

Unique::~Unique()
{
  delete d;
}

QList< const Field* > Unique::fields() const
{
  return d->fields;
}

