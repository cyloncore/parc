/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Class.h"

#include <QBitArray>
#include <QString>
#include <QList>

#include "Enum.h"
#include "Flags.h"
#include "Field.h"

using namespace parc::definitions;

struct Class::Private
{
  const Database* database;
  QString name;
  bool tree;
  QList<const Field*> fields;
  QList<const Class*> classes;
  QList<const Mapping*> mapped;
  QList<const Index*> indices;
  QList<const Unique*> uniques;
  Journaled journaled;
  QList<const Class*> classOfs;
  Class* classOfContainer;
  QBitArray interfaces;
  QList<const Enum*> enums;
  QList<const Flags*> flags;
};

Class::Class(const Database* _database, const QString& _name) : SqlType(SqlType::REFERENCE), d(new Private)
{
  d->database   = _database;
  d->name       = _name;
  d->tree       = false;
  d->journaled  = NOT_JOURNALED;
  d->classOfContainer = nullptr;
  d->interfaces.fill(false, INVALID_INTERFACE);
}

Class::~Class()
{
  qDeleteAll(d->fields);
  qDeleteAll(d->enums);
  qDeleteAll(d->flags);
  delete d;
}

const Database* Class::database() const
{
  return d->database;
}

void Class::setTree(bool _v)
{
  d->tree = _v;
}

bool Class::isTree() const
{
  return d->tree;
}

QString Class::name() const
{
  return d->name;
}

void Class::add(Field* field)
{
  d->fields.append(field);
  if(field->journaled() == JOURNALED and d->journaled != JOURNALED)
  {
    d->journaled = HAS_JOURNALED_CHILDREN;
  }
}

QList<const Field*> Class::fields() const
{
  return d->fields;
}

const Field* Class::field(const QString& _name) const
{
  for(const Field* f : d->fields)
  {
    if(f->name() == _name)
    {
      return f;
    }
  }
  return nullptr;
}

void Class::add(Index* index)
{
  d->indices.append(index);
}

QList< const Index* > Class::indexes() const
{
  return d->indices;
}

void Class::add(Unique* unique)
{
  d->uniques.append(unique);
}

QList<const Unique *> Class::uniques() const
{
  return d->uniques;
}

void Class::addChild(const Class* _child)
{
  d->classes.append(_child);
}

QList<const Class*> Class::children() const
{
  return d->classes;
}

void Class::add(Mapping* mapping)
{
  d->mapped.append(mapping);
}

QList< const Mapping* > Class::mappings() const
{
  return d->mapped;
}

QList< const Field* > Class::keys() const
{
  QList<const Field*> ks;
  foreach(const Field* f, d->fields)
  {
    if(f->options().testFlag(Field::Key))
    {
      ks.append(f);
    }
  }
  return ks;
}

QList< const Field* > Class::parentFields(const Class* _parentType) const
{
  QList< const Field* > parents;
  
  foreach(const Field* field, d->fields)
  {
    if(field->options().testFlag(Field::Parent) and (_parentType == 0 or _parentType == field->sqlType()))
    {
      parents.append(field);
    }
  }
  
  return parents;
}

Journaled Class::journaled() const
{
  return d->journaled;
}

bool Class::isJournaled() const
{
  return d->journaled == HAS_JOURNALED_CHILDREN or d->journaled == JOURNALED;
}

void Class::markJournaled()
{
  d->journaled = JOURNALED;
}

void Class::addClassOfDefinition(Class* _class)
{
  d->classOfs.append(_class);
  if(d->name.isEmpty())
  {
    d->name    = _class->name() + "s";
  }
  _class->d->classOfContainer = this;
}

QList<const Class*> Class::classOfSubDefinitions() const
{
  return d->classOfs;
}

const Class* Class::classOfContainerDefinition() const
{
  return d->classOfContainer;
}

bool Class::doesImplement(Interface _interface) const
{
  return d->interfaces.at(_interface);
}

void Class::markImplement(Interface _interface)
{
  if(_interface != INVALID_INTERFACE)
  {
    d->interfaces.setBit(_interface, true);
  }
}

void Class::addEnum(const Enum* _enum)
{
  d->enums.append(_enum);
}

QList<const Enum *> Class::enums() const
{
  return d->enums;
}

void Class::addFlags(const Flags* _flags)
{
  d->flags.append(_flags);
}

QList<const Flags*> Class::flags() const
{
  return d->flags;
}

const parc::SqlType * Class::type(const QString& _type)
{
  for(const Enum* e : d->enums)
  {
    if(e->name() == _type)
    {
      return e;
    }
  }
  for(const Flags* e : d->flags)
  {
    if(e->name() == _type)
    {
      return e;
    }
  }
  return nullptr;
}

bool Class::hasDependents() const
{
  for(const Field* f : d->fields)
  {
    if(f->options().testFlag(Field::Dependent))
    {
      return true;
    }
  }
  return false;
}
