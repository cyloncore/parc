/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

class QString;

namespace parc 
{

  class SqlType;
  namespace definitions
  {
    class Database;
    class Typedef
    {
    public:
      Typedef(const QString& _alias, const QString& _cpptype, const Database* _database, const SqlType* _sqltype);
      ~Typedef();
      QString alias() const;
      QString cpptype() const;
      const Database* database() const;
      const SqlType* sqltype() const;
    private:
      struct Private;
      Private* const d;
    };
  }  
}
