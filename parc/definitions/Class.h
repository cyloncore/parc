/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QList>

#include "../SqlType.h"
#include "../Journaled.h"
#include "AbstractClass.h"

class QString;

namespace parc 
{
  namespace definitions
  {
    class Database;
    class Field;
    class Mapping;
    class Index;
    class Unique;
    class Class : public SqlType, public AbstractClass
    {
    public:
      Class(const Database* _database, const QString& _name);
      ~Class();
      const Database* database() const override;
      void markJournaled();
      Journaled journaled() const;
      /**
       * @return true if journaled is set to JOURNALED or CHILDREN_HAS_JOURNALED
       */
      bool isJournaled() const;
      QString name() const override;
      void setTree(bool _v);
      bool isTree() const;
      void add(Field* field);
      void add(Mapping* mapping);
      void add(Index* index);
      void add(Unique* unique);
      const Field* field(const QString& _name) const;
      QList<const Field*> fields() const override;
      QList<const Index*> indexes() const;
      QList<const Unique*> uniques() const;
      void addChild(const Class* _child);
      QList<const Class*> children() const;
      QList<const Mapping*> mappings() const;
      QList<const Field*> keys() const;
      QList<const Field*> parentFields(const Class* _parentType = 0) const;
      void addClassOfDefinition(Class* _class);
      QList<const Class*> classOfSubDefinitions() const;
      const Class* classOfContainerDefinition() const;
      void markImplement(Interface _interface);
      bool doesImplement(Interface _interface) const override;
      void addEnum(const Enum* _enum);
      QList<const Enum*> enums() const;
      void addFlags(const Flags* _flags);
      QList<const Flags*> flags() const;
      const SqlType* type(const QString& _type);
      bool hasDependents() const;
    private:
      struct Private;
      Private* const d;
    };
  }  
}
