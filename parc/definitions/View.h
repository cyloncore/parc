#include <QStringList>
#include "AbstractClass.h"

namespace parc
{
  class SqlType;
  namespace definitions
  {
    class Class;
    class Field;
    /**
     * Represent a SQL View.
     */
    class View : public AbstractClass
    {
    public:
      struct Entry
      {
        const Class* klass;
        QStringList fields;
      };
    public:
      View(const Database* _database, const QString& _name);
      ~View();
      const Database* database() const override;
      QString name() const override;
      QList<const Entry*> entries() const;
      void addEntry(const Class* _class, const QStringList& _list);
      QList<const Field*> fields() const override;
      void addField(const QString& _name, const SqlType* _sqlType);
      void markImplement(Interface _interface);
      bool doesImplement(Interface _interface) const override;
    private:
      struct Private;
      Private* const d;
    };
  }
}
