/*
 *  Copyright (c) 2017 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Flags.h"

#include "Class.h"
#include "Database.h"

using namespace parc::definitions;

struct Flags::Private
{
  QString name, enum_name;
  QStringList values;
  const Class* parent;
};

namespace {
  QString make_full_name(const QString& _name, const Class* _parent, bool _with_namespaces)
  {
    return _parent ? (_with_namespaces ? "::" + _parent->database()->namespaces().join("::") + "::" : QString()) + _parent->name() + "::" + _name : _name;
  }
}

Flags::Flags(const QString& _name, const QString& _enum_name, const QStringList& _values, const Class* _parent) : parc::SqlType(parc::SqlType::FLAG, make_full_name(_name, _parent, true)), d(new Private)
{
  d->name   = _name;
  d->enum_name = _enum_name;
  d->values = _values;
  d->parent = _parent;
}

Flags::~Flags()
{
  delete d;
}

QString Flags::name() const
{
  return d->name;
}

QString Flags::enumName() const
{
  return d->enum_name;
}

QString Flags::fullName() const
{
  return make_full_name(d->name, d->parent, false);
}

const Class * Flags::parent() const
{
  return d->parent;
}

QStringList Flags::values() const
{
  return d->values;
}
