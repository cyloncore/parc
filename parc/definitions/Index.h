/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QList>

namespace parc
{
  namespace definitions
  {
    class Field;
    class Index
    {
    public:
      Index(const QList<const Field*> _fields);
      ~Index();
      QList<const Field*> fields() const;
    private:
      struct Private;
      Private* const d;
    };
  }
}
