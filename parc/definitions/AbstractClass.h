#pragma once

#include <QString>
#include "Interfaces.h"

namespace parc::definitions
{
  class Database;
  class Field;
  class AbstractClass
  {
  public:
    virtual ~AbstractClass();
    virtual const Database* database() const = 0;
    virtual QString name() const = 0;
    virtual QList<const Field*> fields() const = 0;
    virtual bool doesImplement(Interface _interface) const = 0;
  };
}
