/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QFlags>

#include "../Journaled.h"

#include "Access.h"

class QString;
class QVariant;

namespace parc 
{
  class SqlType;
  namespace definitions
  {
    class Typedef;
    class Field
    {
    public:
      enum Option
      {
        None      = 0x000,
        Unique    = 0x001,
        Parent    = 0x002,
        Key       = 0x004,
        Many      = 0x008,
        Constant  = 0x010,
        Auto      = 0x020,
        Required  = 0x040,
        Indexed   = 0x080,
        Dependent = 0x100
      };
      Q_DECLARE_FLAGS(Options, Option)
    public:
      Field(const QString& _name, const SqlType* _sqlType, const Options& _options, const Typedef* _typedef, Journaled _journaled, const QString& _defaultValue, Access _access);
      ~Field();
      Journaled journaled() const;
      bool isJournaled() const;
      QString name() const;
      Options options() const;
      const SqlType* sqlType() const;
      const Typedef* typedefinition() const;
      QString defaultValue() const;
    private:
      struct Private;
      Private* const d;
    };
  }  
}
