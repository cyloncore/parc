#include "View.h"

#include <QBitArray>

#include "Field.h"

using namespace parc::definitions;

struct View::Private
{
  const Database* database;
  QString name;
  QBitArray interfaces;
  QList<const Field*> fields;
  QList<const Entry*> entries;
};

View::View(const Database* _database, const QString& _name) : d(new Private)
{
  d->database = _database;
  d->name = _name;
  d->interfaces.fill(false, INVALID_INTERFACE);
}

View::~View()
{
  delete d;
}

const Database* View::database() const
{
  return d->database;
}

QString View::name() const
{
  return d->name;
}

QList<const View::Entry*> View::entries() const
{
  return d->entries;
}

void View::addEntry(const Class* _class, const QStringList& _list)
{
  d->entries.append(new Entry{_class, _list});
}

QList<const Field*> View::fields() const
{
  return d->fields;
}

void View::addField(const QString& _name, const SqlType* _sqlType)
{
  d->fields.append(new Field(_name, _sqlType, Field::Options(), nullptr, NOT_JOURNALED, QString(), Access::Public));
}

bool View::doesImplement(Interface _interface) const
{
  return d->interfaces.at(_interface);
}

void View::markImplement(Interface _interface)
{
  if(_interface != INVALID_INTERFACE)
  {
    d->interfaces.setBit(_interface, true);
  }
}