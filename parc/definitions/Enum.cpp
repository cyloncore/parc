/*
 *  Copyright (c) 2017 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Enum.h"

#include "Class.h"
#include "Database.h"

using namespace parc::definitions;

struct Enum::Private
{
  QString name;
  QStringList values;
  const Class* parent;
};

namespace {
  QString make_full_name(const QString& _name, const Class* _parent, bool _with_namespaces)
  {
    return _parent ? (_with_namespaces ? "::" + _parent->database()->namespaces().join("::") + "::" : QString()) + _parent->name() + "::" + _name : _name;
  }
}

Enum::Enum(const QString& _name, const QStringList& _values, const Class* _parent) : parc::SqlType(parc::SqlType::ENUM, make_full_name(_name, _parent, true)), d(new Private)
{
  d->name   = _name;
  d->values = _values;
  d->parent = _parent;
}

Enum::~Enum()
{
}

QString Enum::name() const
{
  return d->name;
}

QString Enum::fullName() const
{
  return make_full_name(d->name, d->parent, false);
}

const Class * Enum::parent() const
{
  return d->parent;
}

QStringList Enum::values() const
{
  return d->values;
}
