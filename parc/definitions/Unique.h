/*
 *  Copyright (c) 2019 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <QList>

namespace parc
{
  namespace definitions
  {
    class Field;
    class Unique
    {
    public:
      Unique(const QList<const Field*> _fields);
      ~Unique();
      QList<const Field*> fields() const;
    private:
      struct Private;
      Private* const d;
    };
  }
}

