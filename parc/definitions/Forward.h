/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QStringList>

namespace parc 
{
  namespace definitions
  {
    class Forward
    {
    public:
      Forward(const QStringList& _namespaces, const QString& _name);
      ~Forward();
      QStringList namespaces() const;
      QString name() const;
    private:
      struct Private;
      Private* const d;
    };
  }
}
