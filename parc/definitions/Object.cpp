/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Object.h"

using namespace parc::definitions;

struct Object::Private
{
  const Class* klass;
  QString name;
  QStringList arguments;
};

Object::Object(const Class* _class, const QString& _name, const QStringList& _arguments) : d(new Private)
{
  d->klass = _class;
  d->name  = _name;
  d->arguments = _arguments;
}

Object::~Object()
{
  delete d;
}

QStringList Object::arguments() const
{
  return d->arguments;
}

const Class* Object::klass() const
{
  return d->klass;
}

QString Object::name() const
{
  return d->name;
}
