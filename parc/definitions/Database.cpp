/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Database.h"

#include <QBitArray>
#include <QHash>
#include <QList>
#include <QString>

#include "Class.h"
#include "Enum.h"
#include "Forward.h"
#include "Object.h"
#include "Typedef.h"
#include "Mapping.h"
#include "Property.h"

using namespace parc::definitions;

struct Database::Private
{
  Private() : know_has_journal(false) {}
  QHash<QString, Typedef*> typedefs;
  QHash<QString, Class*> classes;
  QHash<QString, const SqlType*> types;
  QStringList namespaces;
  QList<const Mapping*> maps;
  QList<const Object*> objects;
  QList<const Forward*> forwards;
  QList< const Property* > properties;
  QList<const Enum*> enums;
  QList<const View*> views;
  bool has_journal;
  bool know_has_journal;
  QBitArray interfaces;
};

Database::Database() : d(new Private)
{
  d->interfaces.fill(false, INVALID_INTERFACE);
}

Database::~Database()
{
  qDeleteAll(d->objects);
  qDeleteAll(d->forwards);
  qDeleteAll(d->classes);
  qDeleteAll(d->typedefs);
  qDeleteAll(d->properties);
  qDeleteAll(d->views);
  qDeleteAll(d->maps);
}

Class* Database::getClass(const QString& _className)
{
  return d->classes.value(_className, nullptr);
}

Class* Database::getOrCreateClass(const QString& _className)
{
  Class* klass = d->classes.value(_className, nullptr);
  if(klass == 0)
  {
    klass = new Class(this, _className);
    d->classes[_className] = klass;
    d->types[_className] = klass;
  }
  d->know_has_journal = false;
  return klass;
}

const Class* Database::klass(QString _className) const
{
  return d->classes.value(_className, 0);
}

QList<const Class*> Database::classes() const
{
  QList<const Class*> cs;
  foreach(Class* c, d->classes.values())
  {
    cs.append(c);
  }
  return cs;
}

void Database::add(Typedef* _typeDef)
{
  d->typedefs[_typeDef->alias()] = _typeDef;
  d->types[_typeDef->alias()] = _typeDef->sqltype();
}

void Database::add(Object* _object)
{
  d->objects.append(_object);
}

void Database::add(Enum* _enum)
{
  d->types[_enum->fullName()] = _enum;
  d->enums.append(_enum);
}

void Database::add(View* _view)
{
  d->views.append(_view);
}

const parc::SqlType* Database::sqlType(const QString& _name)
{
  return d->types.value(_name, nullptr);
}

const Typedef* Database::typedef_(const QString& _name)
{
  return d->typedefs[_name];
}

QStringList Database::namespaces() const
{
  return d->namespaces;
}

void Database::setNamespaces(const QStringList& _namespace)
{
  d->namespaces = _namespace;
}

void Database::map(Class* _a, Class* _b, Journaled _journaled)
{
  Mapping* mapping = new Mapping(_a, _b, _journaled);
  d->maps.append(mapping);
  _a->add(mapping);
  _b->add(mapping);
}

QList< const Mapping* > Database::mappings() const
{
  return d->maps;
}

const Typedef* Database::typedefinition(const QString& _typedefName) const
{
  return d->typedefs.value(_typedefName, 0);
}

QList< const Typedef* > Database::typedefs() const
{
  QList<const Typedef*> cs;
  foreach(Typedef* c, d->typedefs.values())
  {
    cs.append(c);
  }
  return cs;
}

void Database::add(Forward* _forward)
{
  d->forwards.append(_forward);
}

QList< const Forward* > Database::forwards() const
{
  return d->forwards;
}

QList< const Object* > Database::objects() const
{
  return d->objects;
}

QList<const Enum *> Database::enums() const
{
  return d->enums;
}

QList<const View *> Database::views() const
{
  return d->views;
}

void Database::add(Property* _property)
{
  d->properties.append(_property);
}

void Database::add(Class* _motherClass, Class* _childClass)
{
  _motherClass->addClassOfDefinition(_childClass);
  d->classes[_motherClass->name()] = _motherClass;
  d->types[_motherClass->name()] = _motherClass;
}

QList< const Property* > Database::properties() const
{
  return d->properties;
}

bool Database::hasJournal() const
{
  if(not d->know_has_journal)
  {
    d->has_journal = false;
    foreach(const Class* klass, d->classes)
    {
      if(klass->isJournaled())
      {
        d->has_journal = true;
      }
    }
    d->know_has_journal = true;
  }
  return d->has_journal;
}

bool Database::doesImplement(Interface _interface) const
{
  return d->interfaces.at(_interface);
}

void Database::markImplement(Interface _interface)
{
  if(_interface != parc::definitions::INVALID_INTERFACE)
  {
    d->interfaces.setBit(_interface, true);
  }
}
