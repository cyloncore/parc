/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QStringList>

namespace parc
{
  namespace definitions
  {
    class Class;
    
    class Object
    {
    public:
      Object(const Class* _class, const QString& _name, const QStringList& _arguments);
      ~Object();
      const Class* klass() const;
      QString name() const;
      QStringList arguments() const;
    private:
      struct Private;
      Private* const d;
    };
  }
}
