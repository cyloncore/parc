/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QStringList>

namespace parc
{
  class SqlType;
  namespace definitions
  {

    class Typedef;
    class Property
    {
    public:
      Property(const SqlType* _sqlType, const QString& _name, const QStringList& _value, bool _constant);
      Property(const Typedef* _definitions, const QString& _name, const QStringList& _value, bool _constant);
      ~Property();
      const SqlType* sqlType() const;
      const Typedef* typedefinition() const;
      QString name() const;
      QStringList value() const;
      bool isConstant() const;
    private:
      struct Private;
      Private* const d;
    };
  }
}
