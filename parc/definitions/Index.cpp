/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Index.h"

using namespace parc::definitions;

struct Index::Private
{
  QList< const Field* > fields;
};

Index::Index(const QList< const Field* > _fields) : d(new Private)
{
  d->fields = _fields;
}

Index::~Index()
{
  delete d;
}

QList< const Field* > Index::fields() const
{
  return d->fields;
}
