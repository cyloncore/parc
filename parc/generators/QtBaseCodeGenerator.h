#include "../AbstractCodeGenerator.h"
#include <QtGlobal>

namespace parc
{
  class SqlType;
  namespace definitions
  {
    class AbstractClass;
    class Field;
    class Typedef;
    class Property;
  }
  namespace generators
  {
    class QtBaseCodeGenerator : public AbstractCodeGenerator
    {
    public:
      QtBaseCodeGenerator(AbstractQueryGenerator* _aqg);
      virtual ~QtBaseCodeGenerator();
    protected:
      /**
       * @return the full name of a class (e.g. '::MyDatabase::MyClass')
       */
      QString fullname(const definitions::AbstractClass* _klass) const;
      QString capitalizeFirstLetter(const QString& name) const;
      QString uncapitalizeFirstLetter(const QString& name) const;
      QString constructorArguments(const definitions::AbstractClass* _klass) const;
      QString constructorKeyArguments(const definitions::AbstractClass* _klass) const;
      QString cppType(const definitions::Property* _field) const;
      QString cppType(const definitions::Field* _field) const;
      virtual QString cppType(const SqlType* _sqlType) const;
      QString cppType(const definitions::Typedef* _field) const;
      
      QString cppMemberType(const definitions::Property* _field) const;
      QString cppArgType(const definitions::Property* _field) const;
      QString cppReturnType(const definitions::Property* _field) const;
      QString cppMemberType(const definitions::Field* _field) const;
      QString cppArgType(const definitions::Field* _field) const;
      QString cppReturnType(const definitions::Field* _field) const;
      QString cppMemberType(const definitions::Typedef* _field) const;
      QString cppArgType(const definitions::Typedef* _field) const;
      QString cppReturnType(const definitions::Typedef* _field) const;
      virtual QString cppMemberType(const SqlType* _sqlType) const;
      virtual QString cppArgType(const SqlType* _sqlType) const;
      virtual QString cppReturnType(const SqlType* _sqlType) const;
      bool hasSingleChild(const definitions::Class* _child, const definitions::Class* _parent);
      QString childfunctionname(const QString& name) const;
      QString childrenfunctionname(const QString& name) const;
      QString functiongetallname(const QString& name) const;
      QString tableQueryArg(const definitions::Class* _klass, const QString& _value) const;
      QString valueToMember(const SqlType* _sqlType, const QString& _value) const;
      QString sqlToVariant(const SqlType* _sqlType, const QString& _value) const;
      virtual QString queryResultToMember(const SqlType* _sqlType, const QString& _value) const;
      QString queryResultToMember(const definitions::Field* _field, const QString& _value) const;
      virtual QString queryResultToConstructor(const SqlType* _sqlType, const QString& _value) const;
      QString queryResultToConstructor(const definitions::Field* _field, const QString& _value) const;
      QString sqlTypeToTypeDef(const parc::definitions::Typedef* typedefinition) const;
      QString typeDefTosqlType(const parc::definitions::Typedef* typedefinition) const;
      QString argumentToBinding(const definitions::Field* _field, const QString& _value) const;
      QString memberToBinding(const definitions::Field* _field, const QString& _value) const;
      /**
       * Create the arguments for the create or a constructor function.
       * \param _header indicates if generating for the headers and if it should include default values
       */
      QString createArguments(const definitions::Class* _klass, bool _header) const;
      virtual QString defaultValue(const definitions::Field* _field) const;
      bool isConstantField(const definitions::Field* _field) const;
    };
  }
}
