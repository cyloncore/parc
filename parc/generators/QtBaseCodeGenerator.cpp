#include "QtBaseCodeGenerator.h"

#include <QString>
#include <QVariant>

#include "../definitions/Class.h"
#include "../definitions/Database.h"
#include "../definitions/Enum.h"
#include "../definitions/Field.h"
#include "../definitions/Flags.h"
#include "../definitions/Typedef.h"
#include "../definitions/Forward.h"
#include "../definitions/Object.h"
#include "../definitions/Mapping.h"
#include "../definitions/Property.h"
#include "../AbstractQueryGenerator.h"
#include "../SqlType.h"

using namespace parc::generators;
namespace definitions = parc::definitions;

QtBaseCodeGenerator::QtBaseCodeGenerator(AbstractQueryGenerator* _aqg): AbstractCodeGenerator(_aqg)
{

}

QtBaseCodeGenerator::~QtBaseCodeGenerator()
{

}

QString QtBaseCodeGenerator::fullname(const definitions::AbstractClass* _klass) const
{
  return _klass->database()->namespaces().join("::") + "::" +  _klass->name();
}


QString QtBaseCodeGenerator::capitalizeFirstLetter(const QString& name) const
{
  QString ret = name;
  ret[0] = ret[0].toUpper();
  return ret;
}

QString QtBaseCodeGenerator::uncapitalizeFirstLetter(const QString& name) const
{
  QString ret = name;
  ret[0] = ret[0].toLower();
  return ret;
}

QString QtBaseCodeGenerator::constructorArguments(const definitions::AbstractClass* _klass) const
{
  QString args;
  
  foreach(const definitions::Field* field, _klass->fields())
  {
    // TODO try to use const& when possible
    args += ", " + cppMemberType(field) + " _" + field->name();
  }
  
  return args;
}

QString QtBaseCodeGenerator::constructorKeyArguments(const definitions::AbstractClass* _klass) const
{
  QString args;
  
  foreach(const definitions::Field* field, _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      args += ", " + cppArgType(field) + " _" + field->name();
    }
  }
  
  return args;
}

QString QtBaseCodeGenerator::cppType(const definitions::Typedef* _field) const
{
    return _field->cpptype();
}

QString QtBaseCodeGenerator::cppType(const SqlType* st) const
{
  switch(st->type())
  {
  case SqlType::BOOLEAN:
    return "bool";
  case SqlType::INTEGER:
    return "int";
  case SqlType::FLOAT32:
    return "float";
  case SqlType::FLOAT64:
    return "double";
  case SqlType::STRING:
    return "QString";
  case SqlType::DATETIME:
    return "QDateTime";
  case SqlType::BLOB:
    return "QByteArray";
  case SqlType::REFERENCE:
    return fullname(static_cast<const definitions::Class*>(st));
  case SqlType::ENUM:
  case SqlType::FLAG:
  case SqlType::CUSTOM:
  case SqlType::SQLQUERY:
    return st->cppType();
  }
  qFatal("cppType unimplemented");
}

QString QtBaseCodeGenerator::cppType(const definitions::Field* _field) const
{
  if(_field->typedefinition())
  {
    return cppType(_field->typedefinition());
  }
  Q_ASSERT(_field->sqlType());
  return cppType(_field->sqlType());
}

QString QtBaseCodeGenerator::cppType(const definitions::Property* _field) const
{
  if(_field->typedefinition())
  {
    return cppType(_field->typedefinition());
  }
  Q_ASSERT(_field->sqlType());
  return cppType(_field->sqlType());
}
 
QString QtBaseCodeGenerator::cppMemberType(const definitions::Typedef* _field) const
{
  return _field->cpptype();
}
 
QString QtBaseCodeGenerator::cppMemberType(const SqlType* st) const
{
  switch(st->type())
  {
  case SqlType::BOOLEAN:
    return "bool";
  case SqlType::INTEGER:
    return "int";
  case SqlType::REFERENCE:
  {
    auto klass = dynamic_cast<const definitions::Class*>(st);
    Q_ASSERT(klass); 
    if(klass->keys().size() == 1)
    {
      return cppMemberType(klass->keys().first()->sqlType());
    } else {
      QStringList types;
      for(const definitions::Field* f : klass->keys())
      {
        types.append(cppMemberType(f->sqlType()));
      }
      return "std::tuple<" + types.join(",") + ">";
    }
  }
  case SqlType::FLOAT32:
    return "float";
  case SqlType::FLOAT64:
    return "double";
  case SqlType::STRING:
    return "QString";
  case SqlType::DATETIME:
    return "QDateTime";
  case SqlType::BLOB:
    return "QByteArray";
  case SqlType::ENUM:
  case SqlType::FLAG:
  case SqlType::SQLQUERY:
  case SqlType::CUSTOM:
    return st->cppType();
  }
  qFatal("cppArgType unimplemented");

}

QString QtBaseCodeGenerator::cppMemberType(const definitions::Field* _field) const
{
  if(_field->typedefinition())
  {
    return cppMemberType(_field->typedefinition());
  }
  Q_ASSERT(_field->sqlType());
  return cppMemberType(_field->sqlType());
}

QString QtBaseCodeGenerator::cppMemberType(const definitions::Property* _field) const
{
  if(_field->typedefinition())
  {
    return cppMemberType(_field->typedefinition());
  }
  Q_ASSERT(_field->sqlType());
  return cppMemberType(_field->sqlType());
}
 
QString QtBaseCodeGenerator::cppArgType(const definitions::Typedef* _typedef) const
{
  return "const " + _typedef->cpptype() + "&";
}

QString QtBaseCodeGenerator::cppArgType(const SqlType* _sqlType) const
{
  switch( _sqlType->type())
  {
  case SqlType::BOOLEAN:
    return "bool";
  case SqlType::INTEGER:
    return "int";
  case SqlType::FLOAT32:
    return "float";
  case SqlType::FLOAT64:
    return "double";
  case SqlType::STRING:
    return "const QString&";
  case SqlType::DATETIME:
    return "const QDateTime&";
  case SqlType::BLOB:
    return "const QByteArray&";
  case SqlType::REFERENCE:
    return "const " + fullname(static_cast<const definitions::Class*>(_sqlType)) + "&";
  case SqlType::ENUM:
  case SqlType::FLAG:
    return _sqlType->cppType();
  case SqlType::CUSTOM:
  case SqlType::SQLQUERY:
    return "const " + _sqlType->cppType() + "&";
  }
  qFatal("cppArgType unimplemented");
}

QString QtBaseCodeGenerator::cppArgType(const definitions::Field* _field) const
{
  if(_field->typedefinition())
  {
    return cppArgType(_field->typedefinition());
  }
  Q_ASSERT(_field->sqlType());
  return cppArgType(_field->sqlType());
}

QString QtBaseCodeGenerator::cppArgType(const definitions::Property* _property) const
{
  if( _property->typedefinition())
  {
    return cppArgType( _property->typedefinition());
  }
  Q_ASSERT( _property->sqlType());
  return cppArgType( _property->sqlType());
}

QString QtBaseCodeGenerator::cppReturnType(const definitions::Typedef* _typedef) const
{
  return _typedef->cpptype();
}

QString QtBaseCodeGenerator::cppReturnType(const SqlType* st) const
{
  switch(st->type())
  {
  case SqlType::BOOLEAN:
    return "bool";
  case SqlType::INTEGER:
    return "int";
  case SqlType::FLOAT32:
    return "float";
  case SqlType::FLOAT64:
    return "double";
  case SqlType::STRING:
    return "QString";
  case SqlType::DATETIME:
    return "QDateTime";
  case SqlType::BLOB:
    return "QByteArray";
  case SqlType::REFERENCE:
  {
    return fullname(static_cast<const definitions::Class*>(st));
  }
  case SqlType::ENUM:
  case SqlType::FLAG:
  case SqlType::CUSTOM:
  case SqlType::SQLQUERY:
    return st->cppType();
  }
  qFatal("cppReturnType unimplemented");

}

QString QtBaseCodeGenerator::cppReturnType(const definitions::Field* _field) const
{
  if(_field->typedefinition())
  {
    return cppReturnType(_field->typedefinition());
  }
  Q_ASSERT(_field->sqlType());
  return cppReturnType(_field->sqlType());
}

QString QtBaseCodeGenerator::cppReturnType(const definitions::Property* _field) const
{
  if(_field->typedefinition())
  {
    return cppReturnType(_field->typedefinition());
  }
  Q_ASSERT(_field->sqlType());
  return cppReturnType(_field->sqlType());
}

bool QtBaseCodeGenerator::hasSingleChild(const definitions::Class* _child, const definitions::Class* _parent)
{
  QList< const definitions::Field* > parentfields = _child->parentFields(_parent);
  Q_ASSERT(not parentfields.empty());
  return parentfields.size() == 1 and parentfields.first()->options().testFlag(definitions::Field::Key) and _child->keys().size() == 1;
}

QString QtBaseCodeGenerator::childfunctionname(const QString& name) const
{
  QString ret = name;
  ret[0] = ret[0].toLower();
  return ret;
}


QString QtBaseCodeGenerator::childrenfunctionname(const QString& name) const
{
  QString ret = name;
  ret[0] = ret[0].toLower();
  return ret + "s";
}

QString QtBaseCodeGenerator::functiongetallname(const QString& name) const
{
  QString ret = name;
  ret[0] = ret[0].toLower();
  return ret + "s";
}

QString QtBaseCodeGenerator::tableQueryArg(const definitions::Class* _klass, const QString& _value) const
{
  if(_klass->classOfContainerDefinition())
  {
    return ".arg(" + valueToMember(_klass->classOfContainerDefinition(), _value) + ")";
  }
  return QString();
}

QString QtBaseCodeGenerator::valueToMember(const SqlType* _sqlType, const QString& _value) const
{
  switch(_sqlType->type())
  {
    case SqlType::REFERENCE:
    {
      const definitions::Class* klass = static_cast<const definitions::Class*>(_sqlType);
      const definitions::Field* field = klass->keys().first();
      return valueToMember(field->sqlType(), (_value.isEmpty() ? "" : _value  + ".") + field->name() + "()");
    }
    case SqlType::BOOLEAN:
    case SqlType::ENUM:
    case SqlType::FLAG:
    case SqlType::SQLQUERY:
    case SqlType::CUSTOM:
    case SqlType::INTEGER:
    case SqlType::FLOAT32:
    case SqlType::FLOAT64:
    case SqlType::DATETIME:
    case SqlType::STRING:
    case SqlType::BLOB:
      return _value;
  }
  qFatal("unimplemented");
}

QString QtBaseCodeGenerator::sqlToVariant(const SqlType* _sqlType, const QString& _value) const
{
  switch(_sqlType->type())
  {
  default:
    return _value;
  case SqlType::ENUM:
  case SqlType::FLAG:
  case SqlType::CUSTOM:
  case SqlType::SQLQUERY:
    return "Sql::toVariant<" + _sqlType->cppType() + ">(" + _value + ")";
  case SqlType::REFERENCE:
    return _value + ".id()";
  }
}

QString QtBaseCodeGenerator::queryResultToMember(const SqlType* _sqlType, const QString& _value) const
{
  switch(_sqlType->type())
  {
  case SqlType::BOOLEAN:
    return _value + ".toBool()";
  case SqlType::INTEGER:
    return _value + ".toInt()";
  case SqlType::FLOAT32:
    return _value + ".toFloat()";
  case SqlType::FLOAT64:
    return _value + ".toDouble()";
  case SqlType::STRING:
    return _value + ".toString()";
  case SqlType::DATETIME:
    return _value + ".toDateTime()";
  case SqlType::REFERENCE:
    return _value + ".toInt()";
  case SqlType::BLOB:
    return _value + ".toByteArray()";
  case SqlType::ENUM:
  case SqlType::FLAG:
  case SqlType::CUSTOM:
  case SqlType::SQLQUERY:
    return _value + ".value<" + _sqlType->cppType() + ">()";
  }
  qFatal("variantToSql unimplemented");
}

QString QtBaseCodeGenerator::queryResultToMember(const definitions::Field* _field, const QString& _value) const
{
  if(_field->typedefinition() and (not _field->typedefinition()->sqltype()
    or (_field->typedefinition()->sqltype()->type() != SqlType::CUSTOM
    and _field->typedefinition()->sqltype()->type() != SqlType::SQLQUERY)))
  {
    return sqlTypeToTypeDef(_field->typedefinition()) + "(" + queryResultToMember(_field->typedefinition()->sqltype(), _value) + ")";
  }
  Q_ASSERT(_field->sqlType());
  const SqlType* st = _field->sqlType();
  return queryResultToMember(st, _value);
}

QString QtBaseCodeGenerator::queryResultToConstructor(const SqlType* _sqlType, const QString& _value) const
{
  switch(_sqlType->type())
  {
  case SqlType::BOOLEAN:
    return _value + ".toBool()";
  case SqlType::INTEGER:
    return _value + ".toInt()";
  case SqlType::FLOAT32:
    return _value + ".toDouble()";
  case SqlType::FLOAT64:
    return _value + ".toDouble()";
  case SqlType::STRING:
    return _value + ".toString()";
  case SqlType::DATETIME:
    return _value + ".toDateTime()";
  case SqlType::BLOB:
    return _value + ".toByteArray()";
  case SqlType::REFERENCE:
    {
      const definitions::Class* klass = static_cast<const definitions::Class*>(_sqlType);
      return fullname(klass) +"(d->database, " + _value + ".toInt())";
    }
  case SqlType::ENUM:
  case SqlType::FLAG:
  case SqlType::CUSTOM:
  case SqlType::SQLQUERY:
    return _value + ".value<" + _sqlType->cppType() + ">()";
  }
  qFatal("variantToSql unimplemented");
}

QString QtBaseCodeGenerator::queryResultToConstructor(const definitions::Field* _field, const QString& _value) const
{
  if(_field->typedefinition() and (not _field->typedefinition()->sqltype()
      or (_field->typedefinition()->sqltype()->type() != SqlType::CUSTOM and _field->typedefinition()->sqltype()->type() != SqlType::SQLQUERY)))
  {
    return sqlTypeToTypeDef(_field->typedefinition()) + "(" + queryResultToConstructor(_field->typedefinition()->sqltype(), _value) + ")";
  }
  Q_ASSERT(_field->sqlType());
  const SqlType* st = _field->sqlType();
  return queryResultToConstructor(st, _value);
}

QString QtBaseCodeGenerator::sqlTypeToTypeDef(const definitions::Typedef* typedefinition) const
{
  QString sqltypename;
  switch(typedefinition->sqltype()->type())
  {
  case SqlType::INTEGER:
    sqltypename = "int";
    break;
  case SqlType::FLOAT32:
    sqltypename = "float";
    break;
  case SqlType::FLOAT64:
    sqltypename = "double";
    break;
  case SqlType::STRING:
    sqltypename = "string";
    break;
  case SqlType::DATETIME:
    sqltypename = "datetime";
    break;
  case SqlType::CUSTOM:
  default:
    qFatal("sqlTypeToTypeDef unimplemented");
  }
  return sqltypename + "_to_" + typedefinition->cpptype().toLower();
}

QString QtBaseCodeGenerator::typeDefTosqlType(const definitions::Typedef* typedefinition) const
{
  QString sqltypename;
  switch(typedefinition->sqltype()->type())
  {
  case SqlType::INTEGER:
    sqltypename = "int";
    break;
  case SqlType::FLOAT32:
    sqltypename = "double";
    break;
  case SqlType::FLOAT64:
    sqltypename = "double";
    break;
  case SqlType::STRING:
    sqltypename = "string";
    break;
  case SqlType::DATETIME:
    sqltypename = "datetime";
    break;
  case SqlType::CUSTOM:
  default:
    qFatal("typeDefTosqlType unimplemented");
  }
  return typedefinition->cpptype().toLower() + "_to_" + sqltypename;
}

QString QtBaseCodeGenerator::argumentToBinding(const definitions::Field* _field, const QString& _value) const
{
  if(_field->sqlType()->type() == parc::SqlType::REFERENCE)
  {
    auto klass = dynamic_cast<const definitions::Class*>(_field->sqlType());
    return "Sql::toVariant<" + klass->keys().first()->sqlType()->cppType() + ">(" + _value + ".id())";
  } else {
    return "Sql::toVariant<" + cppType(_field) + ">(" + _value + ")";
  }
}

QString QtBaseCodeGenerator::memberToBinding(const definitions::Field* _field, const QString& _value) const
{
  return "Sql::toVariant<" + cppMemberType(_field) + ">(" + _value + ")";
}

QString QtBaseCodeGenerator::createArguments(const definitions::Class* _klass, bool _header) const
{
  QString args;
  if(_klass->isTree())
  {
    args += "const " + _klass->name() + "& _parent";
  }
  for(const definitions::Field* field : _klass->fields())
  {
    if(not field->options().testFlag(definitions::Field::Auto))
    {
      if(not args.isEmpty()) args += ", ";
      args += cppArgType(field) + " _" + field->name();
      if(_header and not field->options().testFlag(definitions::Field::Required) and not field->options().testFlag(definitions::Field::Key) )
      {
        args += " = " + defaultValue(field);
      }
    }
  }
  return args;
}

QString QtBaseCodeGenerator::defaultValue(const definitions::Field* _field) const
{
  QString dv = _field->defaultValue();
  if(dv.isEmpty())
  {
    if(_field->typedefinition() and (not _field->sqlType() or (_field->sqlType()->type() != SqlType::CUSTOM and _field->typedefinition()->sqltype()->type() != SqlType::SQLQUERY and _field->typedefinition()->sqltype()->type() != SqlType::ENUM)))
    {
      return _field->typedefinition()->cpptype().toLower() + "_default()";
    } else {
      Q_ASSERT(_field->sqlType());
      const SqlType* st = _field->sqlType();
      switch(st->type())
      {
      case SqlType::BOOLEAN:
        return "false";
      case SqlType::INTEGER:
        return "0";
      case SqlType::FLOAT32:
        return "0.0f";
      case SqlType::FLOAT64:
        return "0.0";
      case SqlType::STRING:
        return "QString()";
      case SqlType::DATETIME:
        return "QDateTime()";
      case SqlType::BLOB:
        return "QByteArray()";
      case SqlType::REFERENCE:
        return fullname(static_cast<const definitions::Class*>(st)) + "()";
      case SqlType::ENUM:
      {
        const definitions::Enum* e = static_cast<const definitions::Enum*>(st);
        return e->fullName() + "::" + e->values().first();
      }
      case SqlType::FLAG:
      {
        const definitions::Flags* e = static_cast<const definitions::Flags*>(st);
        return e->fullName() + "()";
      }
      case SqlType::CUSTOM:
      case SqlType::SQLQUERY:
        return st->cppType() + "()";
      }
    }
    qFatal("Unimplemented default value.");
  } else {
    return dv;
  }
}

bool parc::generators::QtBaseCodeGenerator::isConstantField(const definitions::Field* _field) const
{
  return _field->options().testFlag(definitions::Field::Constant)
      or _field->options().testFlag(definitions::Field::Auto)
      or _field->options().testFlag(definitions::Field::Key);
}

