/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "SqlType.h"

#include <QString>

using namespace parc;

struct SqlType::Private
{
  Type type;
  QString sqlType, cppType, sqlQuery;
};

const SqlType* SqlType::boolean   = new SqlType(SqlType::BOOLEAN);
const SqlType* SqlType::integer   = new SqlType(SqlType::INTEGER);
const SqlType* SqlType::float32   = new SqlType(SqlType::FLOAT32);
const SqlType* SqlType::float64   = new SqlType(SqlType::FLOAT64);
const SqlType* SqlType::string    = new SqlType(SqlType::STRING);
const SqlType* SqlType::datetime  = new SqlType(SqlType::DATETIME);
const SqlType* SqlType::blob      = new SqlType(SqlType::BLOB);


SqlType::SqlType(SqlType::Type _type) : d(new Private)
{
  d->type = _type;
}

SqlType::SqlType(Type _type, const QString& _cppType) : SqlType(_type)
{
  d->cppType = _cppType;
}

SqlType * SqlType::createCustom(const QString& _sqlType, const QString& _cppType)
{
  SqlType* type = new SqlType(SqlType::CUSTOM, _cppType);
  type->d->sqlType  = _sqlType;
  return type;
}

SqlType * SqlType::createSqlQuery(const QString& _sqlType, const QString& _cppType)
{
  SqlType* type = new SqlType(SqlType::SQLQUERY, _cppType);
  type->d->sqlQuery  = _sqlType;
  return type;
}

SqlType::~SqlType()
{
  delete d;
}

SqlType::Type SqlType::type() const
{
  return d->type;
}

QString SqlType::cppType() const
{
  return d->cppType;
}

QString SqlType::sqlType() const
{
  return d->sqlType;
}

QString parc::SqlType::sqlQuery() const
{
  return d->sqlQuery;
}
