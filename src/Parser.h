/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Token.h"
#include "../parc/definitions/Interfaces.h"

class QString;
namespace parc
{

  class SqlType;

  namespace definitions
  {
    class Database;
    class Class;
    class Typedef;
  }

  struct Error;
  class Lexer;
  
  class Parser
  {
  public:
    Parser(Lexer* _lexer, const QString& _filename);
    ~Parser();
    definitions::Database* parse();
    QList<Error> errors() const;
  private:
    void parseClass(bool _set_on_tree);
    void parseView();
    void parseClassBody(definitions::Class* _class);
    definitions::Typedef* parseTypeDef();
    void parseNamespaces();
    const SqlType* parseSqlType();
    void parseMapping();
    void parseObject();
    void parseForward();
    definitions::Interface interface(const Token& _token);
    void parseDatabase();
    void parseEnum(definitions::Class* _parent);
  private:
    void getNextToken();
  private:
    void reportError(const Token& _token, const QString& _errorMsg);
    void reportUnexpected(const Token& _token);
    void reportUnknownIndentifier(const Token& _token);
    bool isOfType(const Token& _token, Token::Type _type);
    bool isOfType(Token::Type _type);
    void reachNext(Token::Type _type);
    void checkSingleKeyInteger(const parc::Token& _token, const parc::definitions::Class* _klass);
  private:
    struct Private;
    Private* const d;
  };
}
