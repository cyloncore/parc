/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Token.h"

#include <QDebug>

using namespace parc;

const char* Token::typeToString(Token::Type _type)
{
  switch(_type)
  {
    case INVALID_IDENTIFIER:
      return "invalid identifier";
    case UNFINISHED_STRING:
      return "unfinished string";
    case UNFINISHED_COMMENT:
      return "unfinished comment";
    case END_OF_FILE:
      return "end of file";
    case UNKNOWN:
      return "unknown";
    case SEMI:
      return ";";
    case COLON:
      return ":";
    case COLONCOLON:
      return "::";
    case INFERIOR:
      return "<";
    case SUPPERIOR:
      return ">";
    case EQUAL:
      return "=";
    case STARTBRACE:
      return "{";
    case ENDBRACE:
      return "}";
    case STARTBRACKET:
      return "(";
    case ENDBRACKET:
      return ")";
    case COMMA:
      return ",";
    case CONSTANT_EXPRESSION:
      return "constant expression";
    case STRING_CONSTANT:
      return "string constant";
    case IDENTIFIER:
      return "identifier";
    case CLASS:
      return "class";
    case KEY:
      return "key";
    case BOOLEAN:
      return "boolean";
    case INT:
      return "int";
    case FLOAT:
      return "float";
    case DOUBLE:
      return "double";
    case BLOB:
      return "blob";
    case OF:
      return "of";
    case TREE:
      return "tree";
    case STRING:
      return "string";
    case DATETIME:
      return "datetime";
    case PARENT:
      return "parent";
    case REQUIRED:
      return "required";
    case MAP:
      return "map";
    case WITH:
      return "with";
    case AS:
      return "as";
    case NAMESPACE:
      return "namespace";
    case TYPEDEF:
      return "typedef";
    case MANY:
      return "many";
    case PUBLIC:
      return "public";
    case PRIVATE:
      return "private";
    case PROTECTED:
      return "protected";
    case CONST:
      return "const";
    case AUTO:
      return "auto";
    case UNIQUE:
      return "unique";
    case FORWARD:
      return "forward";
    case JOURNALED:
      return "journaled";
    case IMPLEMENTS:
      return "implements";
    case DATABASE:
      return "database";
    case INDEXED:
      return "indexed";
    case DEFINE:
      return "define";
    case ENUM:
      return "enum";
    case FLAGS:
      return "flags";
    case DEPENDENT:
      return "dependent";
    case AND:
      return "and";
    case VIEW:
      return "view";
    case INCLUDE:
      return "include";
  }
  qFatal("add the token");
}

Token::Token() : type(UNKNOWN)
{

}
Token::Token(parc::Token::Type _type, int _line, int _column) : type(_type), line(_line), column(_column)
{
}


Token::Token(Type _type, const QString& _string, int _line, int _column) : type(_type), line(_line), column(_column), string(_string)
{
  Q_ASSERT( _type == CONSTANT_EXPRESSION or _type == IDENTIFIER or _type == STRING_CONSTANT);
}

bool Token::isSqlType() const
{
  return type == INT or type == STRING or type == DATETIME or type == DOUBLE or type == FLOAT or type == BLOB or type == BOOLEAN;
}
