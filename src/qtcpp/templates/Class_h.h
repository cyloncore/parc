/%  QString guard = _database->namespaces().join("_").toUpper() + "_" + _klass->name().toUpper() + "_H_";
%/#ifndef _/%= guard %/
#define _/%= guard %/
#include <QList>
#include "Sql.h"
#include "TypesDefinitions.h"

#include "Forward.h"

namespace /%= _database->namespaces().join("::") %/
{
  
  class /%= _klass->name() %/SelectQuery
  {
    friend class /%= _klass->name() %/;
    struct Private;
    Private* const d;
  private:
    /%= _klass->name() %/SelectQuery(/%= databaseOrContainer(_klass) %/);
    /%= _klass->name() %/SelectQuery(/%= databaseOrContainer(_klass) %/, const QString& _fieldName, const QVariant& _value, Sql::Operand _op);
    /%= _klass->name() %/SelectQuery(Private* _d);
  public:
    /%= _klass->name() %/SelectQuery();
    /%= _klass->name() %/SelectQuery(const /%= _klass->name() %/SelectQuery& _rhs);
    /%= _klass->name() %/SelectQuery& operator=(const /%= _klass->name() %/SelectQuery& _rhs);
    ~/%= _klass->name() %/SelectQuery();
  public:
    std::size_t count(int _count = 0) const;
    QList</%= _klass->name() %/> exec(int _count = 0) const;
    QList</%= _klass->name() %/> exec(int _skip, int _count) const;
    QList</%= _klass->name() %/> exec(Sql::Transaction* _transaction, int _count = 0) const;
    QList</%= _klass->name() %/> exec(Sql::Transaction* _transaction, int _skip, int _count) const;
    /%= _klass->name() %/ first(Sql::Transaction* _transaction = nullptr) const;
  public:
    /%= _klass->name() %/SelectQuery operator||(const /%= _klass->name() %/SelectQuery& _rhs) const;
    /%= _klass->name() %/SelectQuery operator&&(const /%= _klass->name() %/SelectQuery& _rhs) const;
  public:
/%
  for(const definitions::Field* field : _klass->fields())
  { %/
    /%= _klass->name() %/SelectQuery orderBy/%= capitalizeFirstLetter(field->name()) %/(Sql::Order _order) const;
/%}%/
    /%= _klass->name() %/SelectQuery randomOrder() const;
  };

  class /%= _klass->name() %//%
    if(d->qtobject)
    {
      %/: public QObject/%
    }
  %/
  {/%
    if(d->qtobject)
    {
      %/
    Q_OBJECT
    Q_PROPERTY(bool valid READ isValid CONSTANT)/%
      for(const definitions::Field* field : _klass->fields())
      {
        QString type = cppReturnType(field);
        if(type.startsWith(_klass->name() + "::"))
        {
          type = type.right(type.size() - _klass->name().size() - 2);
        }
        %/
    Q_PROPERTY(/%= type %/ /%= field->name() %/ READ /%= field->name() %/ /%
        if(isConstantField(field))
        {
          %/CONSTANT)/%
        } else {
          %/WRITE set/%= capitalizeFirstLetter(field->name()) %/ NOTIFY /%= field->name() %/Changed)/%
        }
      }
      if(has_notifications)
      {%/
    Q_PROPERTY(bool notificationsEnabled READ notificationsEnabled WRITE setNotificationsEnabled NOTIFY notificationsEnabledChanged)/%
      }
      if(has_update_notifications)
      {%/
    Q_PROPERTY(bool updateNotificationsEnabled READ updateNotificationsEnabled WRITE setUpdateNotificationsEnabled NOTIFY updateNotificationsEnabledChanged)/%
      }
    }
    for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
    {
      %/
    friend class /%= sklass->name() %/;
    friend class /%= sklass->name() %/SelectQuery;/%
    }
    if(_klass->classOfContainerDefinition())
    {
      %/
    friend class /%= _klass->classOfContainerDefinition()->name() %/;/%
    }
    %/
    using SelectQuery = /%= _klass->name() %/SelectQuery;
    friend class Database;
    friend SelectQuery;
  public:
    static QString tableName();/%
    if(_klass->classOfContainerDefinition())
    {
    %/
    static bool lock(const /%= _klass->classOfContainerDefinition()->name() %/& _container, Sql::Transaction* _transaction);/%
    } else {%/
    static bool lock(Sql::Transaction* _transaction);/%
    }%/
  public:/%
    for(const definitions::Enum* e : _klass->enums())
    {
      %/
    enum class /%= e->name() %/ {/%= e->values().join(",") %/};/%
      if(d->qtobject)
      {
        %/
    Q_ENUM(/%= e->name() %/)/%
      }
    }
    for(const definitions::Flags* e : _klass->flags())
    {
      %/
    enum class /%= e->enumName() %/ {/%
        for(int i = 0; i < e->values().size(); ++i)
        {
          if(i > 0) { %/,
          /%}
          %/
      /%= e->values()[i] %/ = 1 << /%= i %//%
        }
      %/
    };
    Q_DECLARE_FLAGS(/%= e->name() %/, /%= e->enumName() %/)
    /%
      if(d->qtobject)
      {
        %/
    Q_ENUM(/%= e->enumName() %/)
    Q_FLAGS(/%= e->name() %/)/%
      }
    }
    %/
  private:
    void assign(const /%= _klass->name() %/& _other);
    /%= _klass->name() %/(/%= databaseOrContainer(_klass) %/ /%= constructorArguments(_klass) %/);
  public:
    /%= _klass->name() %/();
    /%= _klass->name() %/(const /%= _klass->name() %/& _rhs);
    /%= _klass->name() %/& operator=(const /%= _klass->name() %/& _rhs);
    /%= _klass->name() %/(/%= databaseOrContainer(_klass) %/ /%= constructorKeyArguments(_klass) %/, bool _refresh = true);
    ~/%= _klass->name() %/();
  public:
    bool sameAs(const /%= _klass->name() %/& _rhs) const;
    bool operator==(const /%= _klass->name() %/& _rhs) const;
    bool operator!=(const /%= _klass->name() %/& _rhs) const;
  public:
    /% if(d->qtobject) { %/Q_INVOKABLE/% } %/ void refresh(Sql::Transaction* _transaction = nullptr);
    /% if(d->qtobject) { %/Q_INVOKABLE/% } %/ void record(Sql::Transaction* _transaction = nullptr);
    /% if(d->qtobject) { %/Q_INVOKABLE/% } %/ void erase(Sql::Transaction* _transaction = nullptr);
    bool isValid(Sql::Transaction* _transaction = nullptr) const;
    bool isModified() const;
    /%
    for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
    {
      %/
  public:
    /%= sklass->name() %/ create/%= sklass->name() %/(/%= createArguments(sklass, true) %/);
    /%= sklass->name() %/ create/%= sklass->name() %/(Sql::Transaction* _transaction, /%= createArguments(sklass, true) %/);
      /%
    }
    %/
  public:
/%
  // Add function to navigate in the tree
  if(_klass->isTree())
  {%/
    QList</%= _klass->name() %/> children() const;
    QList</%= _klass->name() %/> parents() const;
    bool isChildOf(const /%= _klass->name() %/& _parent, int _max_distance) const;
    void addChild(const /%= _klass->name() %/& _child);
    void removeChild(const /%= _klass->name() %/& _child);
  public:
 /% } %/
/%
  // Add function to access children
  if(not _klass->children().empty())
  {
    foreach(const definitions::Class* child, _klass->children())
    {
      if(hasSingleChild(child, _klass))
      {%/
        /%= child->name() %/ /%= childfunctionname(child->name()) %/() const;
    /%} else {%/
        /%= child->name() %/SelectQuery /%= childrenfunctionname(child->name()) %/() const;
    /%}
    }
  }
  
  // Add field function: getter, setter and query
  for(const definitions::Field* field : _klass->fields())
  {%/
    /%= cppReturnType(field) %/ /%= field->name() %/() const;/%
    if(field->options().testFlag(definitions::Field::Dependent))
    {
      %/
    protected:/%
    }
    if(not isConstantField(field))
    {%/
      void set/%= capitalizeFirstLetter(field->name()) %/(/%= cppArgType(field) %/ _value);
    /%}
    if(field->options().testFlag(definitions::Field::Dependent))
    {
      %/
    public:/%
    }
    %/
    static SelectQuery by/%= capitalizeFirstLetter(field->name()) %/(/%= databaseOrContainer(_klass) %/, /%= cppArgType(field) %/, Sql::Operand _op = Sql::Operand::EQ);
/%}
  if(_klass->hasDependents())
  {%/
    public:
      void updateDependents();/%
  }
  if(d->qtobject)
  {
    %/
  signals:/%
    for(const definitions::Field* field : _klass->fields())
    {%/
    void /%= field->name() %/Changed();/%
    }
  }
  %/
  // Add all queries
  public:
    static SelectQuery all(/%= databaseOrContainer(_klass) %/);
    static void clear(/%= databaseOrContainer(_klass) %/, Sql::Transaction* _transaction = nullptr);
  public:/%
  for(const definitions::Mapping* mapping : _klass->mappings())
  {
    const definitions::Class* klass = mapping->other(_klass);%/
    QList</%= klass->name() %/> /%= functiongetallname(klass->name()) %/() const;
    void add/%= klass->name() %/(const /%= klass->name() %/& _other);
    bool has/%= klass->name() %/(const /%= klass->name() %/& _other) const;
    void remove/%= klass->name() %/(const /%= klass->name() %/& _other);
/%}%/
  private:
    /%
    if(_klass->doesImplement(definitions::INIT))
    {
      %/
      void init();/%
    }
    if(_klass->doesImplement(definitions::CLEANUP))
    {
      %/
      void cleanup();/%
    }
    if(_klass->doesImplement(definitions::EXTENDED))
    {
      %/
#include "/%= _klass->name() %/_.h"
      /%
    }
    if((d->type == Type::CyqlopsDBSQLite or d->type == Type::CyqlopsDBPostgreSQL) and d->qtobject and has_notifications)
    {
      %/
    public:
      bool notificationsEnabled() const;
      void setNotificationsEnabled(bool _v);
    signals:
      void notificationsEnabledChanged();/%
      if(_klass->doesImplement(definitions::NOTIFICATIONS))
      { 
        %/
    void updated();
    void removed();/%
      }
      for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
      {
        if(sklass->doesImplement(definitions::NOTIFICATIONS))
        {
          QString lc_name = uncapitalizeFirstLetter(sklass->name());
        %/
    void /%= lc_name %/Inserted(const /%= sklass->name() %/& _/%= lc_name %/);
    void /%= lc_name %/Changed();
    void /%= lc_name %/Removed();/%
        }
      }
    }
    if(has_update_notifications)
    {
      %/
    public:
      bool updateNotificationsEnabled() const;
      void setUpdateNotificationsEnabled(bool _v);
    signals:
      void updateNotificationsEnabledChanged();
      /%
      for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
      { 
        if(sklass->doesImplement(definitions::UPDATENOTIFICATION))
        {
          QString lc_name = uncapitalizeFirstLetter(sklass->name());
        %/
      void /%= lc_name %/Updated();/%
        }
      }
    }
    %/
  public:
    Database* database() const;
  private:
    struct Private;
    Private* const d;
  };

}

#include <QMetaType>
Q_DECLARE_METATYPE(/%= fullname(_klass) %/ *)
Q_DECLARE_METATYPE(/%= fullname(_klass) %/)
#endif
