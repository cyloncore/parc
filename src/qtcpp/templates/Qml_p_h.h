/%  QString guard = _database->namespaces().join("_").toUpper() + "_QML_P_H_";
%/ #ifndef _/%= guard %/
#define _/%= guard %/

#include <QAbstractListModel>
#include <QDateTime>
#include <QObject>
#include <QQmlListProperty>
#include "Sql.h"

/%
for(const definitions::AbstractClass* aklass : klasses)
{
  %/
#include "/%= aklass->name() %/.h"/%
}
%/

namespace /%= _database->namespaces().join("::") %/
{

  class Database;

namespace Qml {

  class Singleton : public QObject
  {
    Q_OBJECT
  public:
    Q_INVOKABLE QVariant ptr(const QVariant& _var);
    Q_INVOKABLE QVariant value(const QVariant& _var);
  };

  template<bool _refresh_result_>
  struct ResultRefresher;

  template<>
  struct ResultRefresher<true>
  {
    template<typename _T_>
    static void refresh(_T_* _t)
    {
      if(not _t->isModified())
      {
        _t->refresh();
      }
    }
  };
  template<>
  struct ResultRefresher<false>
  {
    template<typename _T_>
    static void refresh(_T_* _t)
    {
      
    }
  };

  template<typename _T_, bool _refresh_result_>
  class ResultModel : public QAbstractListModel
  {
  public:
    enum Roles {
        DataRole = Qt::UserRole + 1
    };
  public:
    ResultModel(QObject* _parent = nullptr) {}
    ~ResultModel() {}
    void setResults(const QList<_T_>& _results)
    {
#if 0
      beginResetModel();

      // Set the result object
      QList<_T_*> oldResults = m_results;
      m_results.clear();
      for(const _T_& r : _results)
      {
        m_results.append(new _T_(r));
      }
      qDeleteAll(oldResults);
      endResetModel();
#else
      
//       QList<_T_*> old_results = m_results;
//    
      // First:
      //   1) remove items from old results that are not in new
      //   2) check in which order the old results are in the new set
      QList<int> new_order;
      for(int i = 0; i < m_results.size();)
      {
        int index_new = -1;
        for(int j = 0; j < _results.size(); ++j)
        {
          if(m_results[i]->sameAs(_results[j]))
          {
            index_new = j;
            ResultRefresher<_refresh_result_>::refresh(m_results[i]);
            break;
          }
        }
        if(index_new == -1)
        { // Not in the new set, then remove
          beginRemoveRows(QModelIndex(), i, i);
          m_results[i]->deleteLater();
          m_results.removeAt(i);
          endRemoveRows();
        } else {
          // In the new set
          ++i;
          new_order.push_back(index_new);
        }
      }
      Q_ASSERT(new_order.size() == m_results.size());
      // Second: reorder the old set according to the new set
      quickSort(new_order, 0, new_order.size() - 1);

      // Third: Add the items from results that are in beteen
      int index = 0;

      while(index < m_results.size())
      {
        if(not m_results[index]->sameAs(_results[index]))
        {
          beginInsertRows(QModelIndex(), index, index);
          m_results.insert(index, new _T_(_results[index]));
          endInsertRows();
        }
        ++index;
        
      }
      
      // Fourth: add new items at the end
      if(index < _results.size())
      {
        beginInsertRows(QModelIndex(), index, _results.size() - 1);
        while(index < _results.size())
        {
          m_results.append(new _T_(_results[index]));
          ++index;
        }
        endInsertRows();
      }
      
      // Check the sets are identical
      Q_ASSERT(m_results.size() == _results.size());
      for(int i = 0; i < m_results.size(); ++i)
      {
        Q_ASSERT(m_results[i]->sameAs(_results[i]));
      }
#endif
    }
    QList<QObject*> results() const {
      #pragma GCC diagnostic ignored "-Wstrict-aliasing"
      return *reinterpret_cast<const QList<QObject*>*>(&m_results);
      #pragma GCC diagnostic error "-Wstrict-aliasing"      
    }
    int rowCount(const QModelIndex &parent = QModelIndex()) const override
    {
      return m_results.size();
    }
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override
    {
      switch(role)
      {
        case DataRole:
          return QVariant::fromValue(m_results[index.row()]);
        default:
          return QVariant();
      }
    }
    QHash<int, QByteArray> roleNames() const override {
      QHash<int, QByteArray> roles;
      roles[DataRole] = "data";
      return roles;
    }
  private:
    void swap(QList<int>& _order, int _i, int _j)
    {
      if(_i == _j) return;
      if(_i > _j) swap(_order, _j, _i);
      if(_i != _j - 1)
      {
        beginMoveRows(QModelIndex(), _i, _i, QModelIndex(), _j);
        m_results.move(_i, _j -1 );
        endMoveRows();
      }
      beginMoveRows(QModelIndex(), _j, _j, QModelIndex(), _i);
      m_results.move(_j, _i);
      endMoveRows();
#if QT_VERSION < QT_VERSION_CHECK(5,13,0)
      _order.swap(_i, _j);
#else
      _order.swapItemsAt(_i, _j);
#endif
    }
    int partition(QList<int>& _order, int _low, int _high)
    {
      int pivot = _order[_high];
      int i = _low;
      
      for(int j = _low; j <= _high - 1; ++j)
      {
        if(_order[j] < pivot)
        {
          swap(_order, i, j);
          ++i;
        }
      }
      swap(_order, i, _high);
      return i;
    }
    void quickSort(QList<int>& _order, int _low, int _high)
    {
      if(_low < _high)
      {
        int p = partition(_order, _low, _high);
        quickSort(_order, _low, p - 1);
        quickSort(_order, p + 1, _high);
      }
    }
  private:
    QList<_T_*> m_results;
  };
/%

  for(const definitions::AbstractClass* aklass : klasses)
  {
    auto klass = dynamic_cast<const definitions::Class*>(aklass);
    QString full_class_name = _database->namespaces().join("::") + "::" + aklass->name();
    %/
  namespace /%= aklass->name() %/
  {
    class SelectQueryFilterBase : public QObject
    {
      Q_OBJECT
    public:
      enum class Type
      {
        Generator, Modifier
      };
    public:
      SelectQueryFilterBase(Type _type, QObject* _parent = nullptr);
      ~SelectQueryFilterBase();
      virtual /%= aklass->name() %/SelectQuery generateQuery(/%= databaseOrContainer(aklass, _database->namespaces().join("::") + "::") %/) const = 0;
      virtual /%= aklass->name() %/SelectQuery generateQuery(const /%= aklass->name() %/SelectQuery& _query) const = 0;
    signals:
      void modified();
    public:
      Type type() const { return m_type; }
    private:
      Type m_type;
    };
    class SelectQueryFilterContainerBase : public SelectQueryFilterBase
    {
      Q_OBJECT
      Q_PROPERTY(QQmlListProperty</%= _database->namespaces().join("::") %/::Qml::/%= aklass->name() %/::SelectQueryFilterBase> filters READ filters)
      Q_CLASSINFO("DefaultProperty", "filters")
    public:
      SelectQueryFilterContainerBase(QObject* _parent = nullptr);
      ~SelectQueryFilterContainerBase();
      QQmlListProperty<SelectQueryFilterBase> filters();
    private:
      static void appendFilter(QQmlListProperty<SelectQueryFilterBase>*, SelectQueryFilterBase*);
      static qsizetype filterCount(QQmlListProperty<SelectQueryFilterBase>*);
      static SelectQueryFilterBase* filter(QQmlListProperty<SelectQueryFilterBase>*, qsizetype);
      static void clearFilters(QQmlListProperty<SelectQueryFilterBase>*);
    protected:
      virtual bool acceptFilter(const SelectQueryFilterBase* _filter) const = 0;
    protected:
      QList<SelectQueryFilterBase*> m_filters;
    };
    class SelectQuery : public QObject
    {
      Q_OBJECT
      Q_PROPERTY(QAbstractItemModel* model READ model CONSTANT)
      Q_PROPERTY(QList<QObject*> result READ result NOTIFY resultChanged)
      Q_PROPERTY(int skip READ skip WRITE setSkip NOTIFY skipChanged)
      Q_PROPERTY(int count READ count WRITE setCount NOTIFY countChanged)
      Q_PROPERTY(QQmlListProperty</%= _database->namespaces().join("::") %/::Qml::/%= aklass->name() %/::SelectQueryFilterBase> filters READ filters)
      Q_CLASSINFO("DefaultProperty", "filters")
    public:
      SelectQuery(QObject* _parent = nullptr);
      ~SelectQuery();
    public:
      QAbstractItemModel* model() const { return const_cast<QAbstractItemModel*>(static_cast<const QAbstractItemModel*>(&m_model)); }
      QList<QObject*> result() const { return m_model.results(); }
      QQmlListProperty<SelectQueryFilterBase> filters() { return m_filters->filters(); }
    public slots:
      void execute(/%= _database->namespaces().join("::") %/::Sql::Transaction* _transaction = nullptr);
      /%
      if(klass and klass->classOfContainerDefinition())
      {
        QString uFLcOCD = uncapitalizeFirstLetter(klass->classOfContainerDefinition()->name());
        %/
    private:
      Q_PROPERTY(/%= _database->namespaces().join("::") %/::/%= klass->classOfContainerDefinition()->name() %/ * /%= uFLcOCD %/ READ /%= uFLcOCD %/ WRITE set/%= klass->classOfContainerDefinition()->name() %/ NOTIFY /%= uFLcOCD %/Changed());
      /%= _database->namespaces().join("::") %/::/%= klass->classOfContainerDefinition()->name() %/ * m_container = nullptr;
    public:
      /%= _database->namespaces().join("::") %/::/%= klass->classOfContainerDefinition()->name() %/ * /%= uFLcOCD %/() { return m_container; }
      void set/%= klass->classOfContainerDefinition()->name() %/(/%= _database->namespaces().join("::") %/::/%= klass->classOfContainerDefinition()->name() %/ *_container)
      {
        if(m_container)
        {
          disconnect(m_container, SIGNAL(/%= uncapitalizeFirstLetter(aklass->name()) %/Changed()), this, SLOT(execute()));
        }
        m_container = _container;
        if(m_container)
        {
          m_database  = m_container->database();
        } else {
          m_database = nullptr;
        }
        emit(/%= uFLcOCD %/Changed());/%
        if(aklass->doesImplement(definitions::NOTIFICATIONS))
        { %/
        updateAutoExecute();/%
        }%/
      }
    signals:
      void /%= uFLcOCD %/Changed();
        /%
      } else {
        %/
    private:
      Q_PROPERTY(/%= _database->namespaces().join("::") %/::Database* database READ database WRITE setDatabase NOTIFY databaseChanged);
    public:
      Database* database() const { return m_database; }
      void setDatabase(Database* _database)
      {
        m_database = _database;
        emit(databaseChanged());/%
        if(aklass->doesImplement(definitions::NOTIFICATIONS))
        { %/
        updateAutoExecute();/%
        }%/
      }
    signals:
      void databaseChanged();
        /%
      }
      if(aklass->doesImplement(definitions::NOTIFICATIONS))
      {
        %/
    private:
      Q_PROPERTY(bool autoExecute READ autoExecute WRITE setAutoExecute NOTIFY autoExecuteChanged)
      bool m_autoExecute = false;
    public:
      bool autoExecute() const { return m_autoExecute; }
      void setAutoExecute(bool _v);
    private:
      void updateAutoExecute();
    signals:
      void autoExecuteChanged();/%
      } else {%/
    private:
      bool autoExecute() const { return false; }/%
      }
      %/
    public:
      int skip() const { return m_skip; }
      void setSkip(int _skip) { m_skip = _skip; emit(skipChanged()); if(autoExecute()) execute(); }
      int count() const { return m_count; }
      void setCount(int _count) { m_count = _count; emit(countChanged()); if(autoExecute()) execute(); }
    signals:
      void skipChanged();
      void countChanged();
      void resultChanged();
    private:
      int m_skip = 0, m_count = 0;
      Database* m_database = nullptr;
      ResultModel</%= full_class_name %/, /%= klass == nullptr ? "false" : "true" %/> m_model;
      SelectQueryFilterContainerBase* m_filters;
    };
    class Or : public SelectQueryFilterContainerBase
    {
      Q_OBJECT
    public:
      Or(QObject* _parent = nullptr);
      ~Or();
      /%= aklass->name() %/SelectQuery generateQuery(/%= databaseOrContainer(aklass, _database->namespaces().join("::") + "::") %/) const override;
      /%= aklass->name() %/SelectQuery generateQuery(const /%= aklass->name() %/SelectQuery& _query) const override { qFatal("NOT A MODIFIER"); }
    protected:
      virtual bool acceptFilter(const SelectQueryFilterBase* _filter) const override;
    };
    class And : public SelectQueryFilterContainerBase
    {
      Q_OBJECT
    public:
      And(QObject* _parent = nullptr);
      And(bool _acceptAll, QObject* _parent = nullptr);
      ~And();
      /%= aklass->name() %/SelectQuery generateQuery(/%= databaseOrContainer(aklass, _database->namespaces().join("::") + "::") %/) const override;
      /%= aklass->name() %/SelectQuery generateQuery(const /%= aklass->name() %/SelectQuery& _query) const override { qFatal("NOT A MODIFIER"); }
    protected:
      virtual bool acceptFilter(const SelectQueryFilterBase* _filter) const override;
    private:
      bool m_acceptAll = false;
    };
  /%
    for(const definitions::Field* field : aklass->fields())
    {
      QString typena = cppMemberType(field);
      %/
    class By/%= capitalizeFirstLetter(field->name()) %/ : public SelectQueryFilterBase
    {
      Q_OBJECT
    public:
      enum class Operand
      {
        EQ, GT, LT, LTE, GTE, NEQ, LIKE
      };
      Q_ENUM(Operand)
      Q_PROPERTY(Operand operand MEMBER m_operand NOTIFY modified);
      Q_PROPERTY(/%= typena %/ /%= uncapitalizeFirstLetter(field->name()) %/ MEMBER m_/%= uncapitalizeFirstLetter(field->name()) %/ NOTIFY modified);
    public:
      By/%= capitalizeFirstLetter(field->name()) %/(QObject* _parent = nullptr);
      ~By/%= capitalizeFirstLetter(field->name()) %/();
      /%= aklass->name() %/SelectQuery generateQuery(/%= databaseOrContainer(aklass, _database->namespaces().join("::") + "::") %/) const override;
      /%= aklass->name() %/SelectQuery generateQuery(const /%= aklass->name() %/SelectQuery& _query) const override { qFatal("NOT A MODIFIER"); }
    private:
      /%= typena %/ m_/%= uncapitalizeFirstLetter(field->name()) %/;
      Sql::Operand sqlOperand() const;
      Operand m_operand = Operand::EQ;
    };
    class OrderBy/%= capitalizeFirstLetter(field->name()) %/ : public SelectQueryFilterBase
    {
      Q_OBJECT
    public:
      enum class Order
      {
        Asc, Desc
      };
      Q_ENUM(Order)
      Q_PROPERTY(Order order MEMBER m_order NOTIFY modified);
    public:
      OrderBy/%= capitalizeFirstLetter(field->name()) %/(QObject* _parent = nullptr);
      ~OrderBy/%= capitalizeFirstLetter(field->name()) %/();
      /%= aklass->name() %/SelectQuery generateQuery(/%= databaseOrContainer(aklass, _database->namespaces().join("::") + "::") %/) const override { qFatal("NOT A MODIFIER"); }
      /%= aklass->name() %/SelectQuery generateQuery(const /%= aklass->name() %/SelectQuery& _query) const override;
    private:
      /%= typena %/ m_/%= uncapitalizeFirstLetter(field->name()) %/;
      Sql::Order sqlOrder() const;
      Order m_order = Order::Asc;
    };
    /%
    } %/
  } // namespace /%= aklass->name() %/ /%
  }
  %/
} // namespace qml  
} // namespace /%= _database->namespaces().join("::") %/

#endif

