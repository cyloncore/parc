#include "JournaledFields.h"

class QString;

/%
foreach(QString ns, _database->namespaces())
{
%/
  namespace /%= ns %/ {
/%
}
%/
  class JournalEntry
  {
  public:
    enum Type {
      DELETE, INSERT, UPDATE
    };
  protected:
    struct Private;
    Private* d;
  protected:
    JournalEntry(Private* _d);
  public:
    virtual ~JournalEntry();
  };
  /%
  foreach(const definitions::Class* klass, _database->classes())
  {
    if(klass->isJournaled())
    {
    %/
      class /%= klass->name() %/JournalEntry : public JournalEntry
      {
        friend class Journal;
        /%= klass->name() %/JournalEntry();
      public:
        /%= klass->name() %/JournalEntry(const /%= klass->name() %/JournalEntry& _rhs);
        /%= klass->name() %/JournalEntry& operator=(const /%= klass->name() %/JournalEntry& _rhs);
        virtual ~/%= klass->name() %/JournalEntry();
      };
    /%
    }
  }
  foreach(const definitions::Mapping* mapping, _database->mappings())
  {
    if(mapping->isJournaled())
    {
      const QString KLASS_NAME = mapping->a()->name() + mapping->b()->name() + "MappingJournalEntry";
      %/
      class /%= KLASS_NAME %/ : public JournalEntry
      {
        friend class Journal;
        /%= KLASS_NAME %/();
      public:
        /%= KLASS_NAME %/(const /%= KLASS_NAME %/& _rhs);
        /%= KLASS_NAME %/& operator=(const /%= KLASS_NAME %/& _rhs);
        virtual ~/%= KLASS_NAME %/();
      };
      /%
    }
  }
  %/
/%
foreach(QString ns, _database->namespaces())
{
%/
  }
/%
}
%/
