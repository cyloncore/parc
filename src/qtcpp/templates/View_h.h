/%  QString guard = _database->namespaces().join("_").toUpper() + "_" + _view->name().toUpper() + "_H_";
%/#ifndef _/%= guard %/
#define _/%= guard %/

#include <QList>
#include "Sql.h"
#include "TypesDefinitions.h"

#include "Forward.h"

namespace /%= _database->namespaces().join("::") %/
{ 
  class /%= _view->name() %/SelectQuery
  {
    friend class /%= _view->name() %/;
    struct Private;
    Private* const d;
  private:
    /%= _view->name() %/SelectQuery(Database* _database);
    /%= _view->name() %/SelectQuery(Database* _database, const QString& _fieldName, const QVariant& _value, Sql::Operand _op);
    /%= _view->name() %/SelectQuery(Private* _d);
  public:
    /%= _view->name() %/SelectQuery();
    /%= _view->name() %/SelectQuery(const /%= _view->name() %/SelectQuery& _rhs);
    /%= _view->name() %/SelectQuery& operator=(const /%= _view->name() %/SelectQuery& _rhs);
    ~/%= _view->name() %/SelectQuery();
  public:
    std::size_t count(int _count = 0) const;
    QList</%= _view->name() %/> exec(int _count = 0) const;
    QList</%= _view->name() %/> exec(int _skip, int _count) const;
    QList</%= _view->name() %/> exec(Sql::Transaction* _transaction, int _count = 0) const;
    QList</%= _view->name() %/> exec(Sql::Transaction* _transaction, int _skip, int _count) const;
    /%= _view->name() %/ first(Sql::Transaction* _transaction = nullptr) const;
  public:
    /%= _view->name() %/SelectQuery operator||(const /%= _view->name() %/SelectQuery& _rhs) const;
    /%= _view->name() %/SelectQuery operator&&(const /%= _view->name() %/SelectQuery& _rhs) const;
  public:
/%
  for(const definitions::Field* field : _view->fields())
  { %/
    /%= _view->name() %/SelectQuery orderBy/%= capitalizeFirstLetter(field->name()) %/(Sql::Order _order) const;
/%}%/
    /%= _view->name() %/SelectQuery randomOrder() const;
  };

  class /%= _view->name() %//%
    if(d->qtobject)
    {
      %/: public QObject/%
    }
  %/
  {
  public:
    enum class EntryClass
    {/%
    for(const definitions::View::Entry* e : _view->entries())
    {
      %/
      /%= e->klass->name() %/,/%
    }
    %/
    };
  private:
    /%
    if(d->qtobject)
    {
      %/
    Q_OBJECT
    Q_ENUM(EntryClass)
    Q_PROPERTY(EntryClass entryClass READ entryClass CONSTANT)
    Q_PROPERTY(QVariant nativeVariant READ nativeVariant CONSTANT)
    Q_PROPERTY(bool valid READ isValid CONSTANT)/%
      for(const definitions::Field* field : _view->fields())
      {
        QString type = cppReturnType(field);
        if(type.startsWith(_view->name() + "::"))
        {
          type = type.right(type.size() - _view->name().size() - 2);
        }
        %/
    Q_PROPERTY(/%= type %/ /%= field->name() %/ READ /%= field->name() %/ CONSTANT)/%
      }
    }%/
    using SelectQuery = /%= _view->name() %/SelectQuery;
    friend SelectQuery;
  public:
    static QString viewName();
  private:
    void assign(const /%= _view->name() %/& _other);
    /%= _view->name() %/(Database* _database, EntryClass _entryClass/%= constructorArguments(_view) %/);
  public:
    /%= _view->name() %/();
    /%= _view->name() %/(const /%= _view->name() %/& _rhs);
    /%= _view->name() %/& operator=(const /%= _view->name() %/& _rhs);
    ~/%= _view->name() %/();
  public:
    bool sameAs(const /%= _view->name() %/& _rhs) const;
    bool operator==(const /%= _view->name() %/& _rhs) const;
    bool operator!=(const /%= _view->name() %/& _rhs) const;
  public:
    /**
     * @return true if valid
     */
    bool isValid() const;
  public:
    EntryClass entryClass() const;
    /**
     * @return a variant with a value as the native class.
     */
    QVariant nativeVariant() const;
  // Add field function: getter query
  /%
  for(const definitions::Field* field : _view->fields())
  {%/
    /%= cppReturnType(field) %/ /%= field->name() %/() const;
    static SelectQuery by/%= capitalizeFirstLetter(field->name()) %/(Database* _database, /%= cppArgType(field) %/, Sql::Operand _op = Sql::Operand::EQ);/%
  }%/
  public:
    static SelectQuery all(Database* _database);
  public:
    Database* database() const;


  private:
    struct Private;
    Private* const d;
  };
}

#include <QMetaType>
Q_DECLARE_METATYPE(/%= fullname(_view) %/ *)
Q_DECLARE_METATYPE(/%= fullname(_view) %/)
#endif
