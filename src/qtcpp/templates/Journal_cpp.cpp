#include "Journal.h"

#include <QDebug>

#include "Database.h"
#include "JournalEntry.h"

/%
foreach(const definitions::Class* klass, _database->classes())
{
  if(klass->isJournaled())
  {
  %/
#include "/%= klass->name() %/.h"
  /%
  }
}
%/


using namespace /%= _database->namespaces().join("::") %/;

struct Journal::Private
{
  Database* database;
};

Journal::Journal(Database* _database) : d(new Private)
{
  d->database = _database;
}

Journal::~Journal()
{
  delete d;
}


/%
foreach(const definitions::Class* klass, _database->classes())
{
  if(klass->isJournaled())
  {
  %/
    QList</%= klass->name() %/JournalEntry> Journal::/%= uncapitalizeFirstLetter(klass->name()) %/Entry(unsigned int _startId, unsigned int _count)
    {
      Sql::Query query(d->database->sqlDatabase());
      query.prepare("/%= queryGenerator()->generateSelectJournalQuery(klass) %/");
      QList</%= klass->name() %/JournalEntry> result;
      if(query.exec())
      {
        while(query.next())
        {
//           const int id                        = query.value(0);
//           const int __journal_entry_type      = query.value(1);
//           const int __journal_modified_fields = query.value(2);
          /%= klass->name() %/JournalEntry entry;
          qFatal("Unimplemented");
          result.append(entry);
        }
      } else {
        qWarning() << query.lastQuery() << query.lastError();
      }
      return result;
    }
    void Journal::recordDelete(const /%= klass->name() %/& _object)
    {
      Sql::Query query(d->database->sqlDatabase());
      query.prepare(QString("/%= queryGenerator()->generateRecordJournalDeleteQuery(klass) %/"));
      query.bindValue(":__journal_entry_type", JournalEntry::DELETE);
      /%
      foreach(const definitions::Field* field, klass->fields())
      {
        if(field->options().testFlag(definitions::Field::Key))
        {
          %/
          query.bindValue(":/%= field->name() %/", /%= argumentToBinding(field, QString("_object.%1()").arg(field->name())) %/);
          /%
        }
      }
      %/
      if(not query.exec())
      {
        qWarning() << query.lastQuery() << query.lastError();
      }      
    }
    void Journal::recordUpdate(const /%= klass->name() %/& _object, const QFlags<JournaledFields::/%= klass->name() %/Fields>& _updatedFields)
    {
      Sql::Query query(d->database->sqlDatabase());
      QString fields, values;
      
      JournaledFields::Set<JournaledFields::/%= klass->name() %/Fields> __journal_modified_fields;
      /%
      QString KLASS_NAME = klass->name().toUpper();
      foreach(const definitions::Field* field, klass->fields())
      {
        if(not field->options().testFlag(definitions::Field::Constant) and not field->options().testFlag(definitions::Field::Key))
        {
          %/
          if(_updatedFields.testFlag(JournaledFields::/%= KLASS_NAME %/_/%= field->name().toUpper() %/))
          {
            fields += ", /%= field->name() %/";
            values += ", :/%= field->name() %/";
            __journal_modified_fields.set(JournaledFields::/%= KLASS_NAME %/_/%= field->name().toUpper() %/);
          }
          /%
        }
      }
      %/
      query.prepare(QString("/%= queryGenerator()->generateRecordJournalUpdateQuery(klass) %/").arg(fields).arg(values));
      query.bindValue(":__journal_entry_type", JournalEntry::UPDATE);
      query.bindValue(":__journal_modified_fields", __journal_modified_fields);
      /%
      foreach(const definitions::Field* field, klass->fields())
      {
        if(field->options().testFlag(definitions::Field::Key))
        {
          %/
          query.bindValue(":/%= field->name() %/", /%= argumentToBinding(field, QString("_object.%1()").arg(field->name())) %/);
          /%
        } else if(not field->options().testFlag(definitions::Field::Constant))
        {
          %/
          if(_updatedFields.testFlag(JournaledFields::/%= KLASS_NAME %/_/%= field->name().toUpper() %/))
          {
            query.bindValue(":/%= field->name() %/", /%= argumentToBinding(field, QString("_object.%1()").arg(field->name())) %/);
          }
          /%
        }
      }
      %/
      if(not query.exec())
      {
        qWarning() << query.lastQuery() << query.lastError();
      }
    }
    void Journal::recordInsert(const /%= klass->name() %/& _object)
    {
      Sql::Query query(d->database->sqlDatabase());
      query.prepare(QString("/%= queryGenerator()->generateRecordJournalInsertQuery(klass) %/"));
      query.bindValue(":__journal_entry_type", JournalEntry::INSERT);
      /%
      foreach(const definitions::Field* field, klass->fields())
      {
        if(field->options().testFlag(definitions::Field::Key))
        {
          %/
          query.bindValue(":/%= field->name() %/", /%= argumentToBinding(field, QString("_object.%1()").arg(field->name())) %/);
          /%
        }
      }
      %/
      if(not query.exec())
      {
        qWarning() << query.lastQuery() << query.lastError();
      }
    }
  /%
    if(klass->isTree() and klass->journaled() == parc::definitions::JOURNALED)
    {
      %/
      void Journal::recordChildAdd(const /%= klass->name() %/& _parent, const /%= klass->name() %/& _child)
      {
        Sql::Query query(d->database->sqlDatabase());
        query.prepare(QString("/%= queryGenerator()->generateTreeAddChildcJournalQuery(klass) %/"));
        query.bindValue(":__journal_entry_type", JournalEntry::INSERT);
        query.bindValue(":ancestor", _parent./%= klass->keys().first()->name() %/());
        query.bindValue(":descendant", _child./%= klass->keys().first()->name() %/());
        if(not query.exec())
        {
          qWarning() << query.lastQuery() << query.lastError();
        }
      }
//      void recordChildRemove(const /%= klass->name() %/& _parent, const /%= klass->name() %/& _child);
      /%
    }
  }
}

foreach(const definitions::Mapping* mapping, _database->mappings())
{
  if(mapping->isJournaled())
  {
    %/
    void Journal::recordMapping(const /%= mapping->a()->name() %/& _objectA, const /%= mapping->b()->name() %/& _objectB)
    {
      Sql::Query query(d->database->sqlDatabase());
      query.prepare(QString("/%= queryGenerator()->generateMappingUpdateJournalQuery(mapping) %/"));
      query.bindValue(":__journal_entry_type", JournalEntry::INSERT);
      query.bindValue(":id_a", _objectA./%= mapping->a()->keys().first()->name() %/());
      query.bindValue(":id_b", _objectB./%= mapping->b()->keys().first()->name() %/());
      if(not query.exec())
      {
        qWarning() << query.lastQuery() << query.lastError();
      }
    }
    void Journal::recordUnmapping(const /%= mapping->a()->name() %/& _objectA, const /%= mapping->b()->name() %/& _objectB)
    {
      Sql::Query query(d->database->sqlDatabase());
      query.prepare(QString("/%= queryGenerator()->generateMappingUpdateJournalQuery(mapping) %/"));
      query.bindValue(":__journal_entry_type", JournalEntry::DELETE);
      query.bindValue(":id_a", _objectA./%= mapping->a()->keys().first()->name() %/());
      query.bindValue(":id_b", _objectB./%= mapping->b()->keys().first()->name() %/());
      if(not query.exec())
      {
        qWarning() << query.lastQuery() << query.lastError();
      }
    }    
    /%
  }
}

%/
