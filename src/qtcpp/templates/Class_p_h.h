#include "/%= _klass->name() %/.h"

#include "Database.h"

/%
foreach(QString ns, _database->namespaces())
{
%/
namespace /%= ns %/ {
/%
}
%/
struct /%= _klass->name() %/::Private
{
  Private() : modified(false)
  /%
  for(const definitions::Field* field : _klass->fields())
  {
    if(field->sqlType()->type() == SqlType::INTEGER)
    {
      %/
      , /%= field->name() %/(0)
      /%
    }
    if(not field->options().testFlag(definitions::Field::Key)
        and not field->options().testFlag(definitions::Field::Constant))
    {
      %/
      , /%= field->name() %/_modified(false)
      /%
    }
  }
  %/
  {
  }
  ~Private()
  {/%
    if(has_notifications)
    {%/
    QObject::disconnect(notificationsHandle);/%
    }
    %/    
  }
  void resetOwned()
  {
  }
  Database* database;
  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
  /%= _klass->classOfContainerDefinition()->name() %/ container;
    /%
  }
  %/
  bool modified;
  /%
  for(const definitions::Field* field : _klass->fields())
  {
    %/
  /%= cppMemberType(field) %/ /%= field->name() %/;/%
    if(not field->options().testFlag(definitions::Field::Key)
        and not field->options().testFlag(definitions::Field::Constant))
    {
      %/
  bool /%= field->name() %/_modified;/%
    }
  }
  // Notifications support
  if(has_notifications)
  {%/
  bool notificationsEnabled = false;
  QMetaObject::Connection notificationsHandle;/%  
  }
  if(has_update_notifications)
  {%/
  bool updateNotificationsEnabled = false;/%
  
    for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
    { 
      if(sklass->doesImplement(definitions::UPDATENOTIFICATION))
      {%/
  QMetaObject::Connection /%= sklass->name() %/UpdateNotificationsHandle;/%
      }
    }
  }
  %/
};
/%
for(QString ns : _database->namespaces())
{
%/
}
/%
}
%/
