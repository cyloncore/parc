#include "/%= _view->name() %/.h"

#include <QDebug>
#include <QStringList>

#include "Sql.h"

#include "Database.h"

/%
for(const definitions::View::Entry* e : _view->entries())
{
%/
#include "/%= e->klass->name() %/.h"/%
}
%/

using namespace /%= _database->namespaces().join("::") %/;

//-----------------------------------//
//         Generate SelectQuery      //
//-----------------------------------//

struct /%= _view->name() %/SelectQuery::Private
{
  Private* clone() const;
  struct WhereDefinition
  {
    virtual ~WhereDefinition() {}
    virtual QString generateWhere(int& _counter) = 0;
    virtual void setBinding(Sql::Query& _query) = 0;
    virtual WhereDefinition* clone() = 0;
  };
  struct All : public WhereDefinition
  {
    virtual QString generateWhere(int& _counter);
    virtual void setBinding(Sql::Query& _query);
    virtual WhereDefinition* clone();
  };
  struct FieldComp : public WhereDefinition
  {
    QString fieldName;
    QString bindName;
    QVariant value;
    Sql::Operand op;
    virtual QString generateWhere(int& _counter);
    virtual void setBinding(Sql::Query& _query);
    virtual WhereDefinition* clone();
  };
  struct Operator : public WhereDefinition
  {
    virtual ~Operator();
    WhereDefinition* def_left;
    WhereDefinition* def_right;    
    virtual void setBinding(Sql::Query& _query);
  };
  struct AndOperator : public Operator
  {
    virtual QString generateWhere(int& _counter);
    virtual WhereDefinition* clone();
  };
  struct OrOperator : public Operator
  {
    virtual QString generateWhere(int& _counter);
    virtual WhereDefinition* clone();
  };

  Database* database;
  WhereDefinition* where;
  QStringList orderBy;
};

/%= _view->name() %/SelectQuery::Private* /%= _view->name() %/SelectQuery::Private::clone() const
{
  Private* nd = new Private(*this);
  if(where)
  {
    nd->where = where->clone();
  }
  return nd;
}


QString /%= _view->name() %/SelectQuery::Private::All::generateWhere(int& )
{
  return "/%= queryGenerator()->generateTrue() %/";
}

void /%= _view->name() %/SelectQuery::Private::All::setBinding(Sql::Query& /*_query*/)
{
}

/%= _view->name() %/SelectQuery::Private::WhereDefinition* /%= _view->name() %/SelectQuery::Private::All::clone()
{
  return new All;
}


QString /%= _view->name() %/SelectQuery::Private::FieldComp::generateWhere(int& _counter)
{
  bindName = QString(":bv%1").arg(++_counter);
  switch(op)
  {
    case Sql::Operand::EQ:
      return QString("/%= queryGenerator()->generateWhereEq() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::GT:
      return QString("/%= queryGenerator()->generateWhereGT() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::LT:
      return QString("/%= queryGenerator()->generateWhereLT() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::LTE:
      return QString("/%= queryGenerator()->generateWhereLTE() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::GTE:
      return QString("/%= queryGenerator()->generateWhereGTE() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::NEQ:
      return QString("/%= queryGenerator()->generateWhereNEq() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::LIKE:
      return QString("/%= queryGenerator()->generateWhereLike() %/").arg(fieldName).arg(bindName);
      break;
  }
  qFatal("Unknown query");
}

void /%= _view->name() %/SelectQuery::Private::FieldComp::setBinding(Sql::Query& _query)
{
  _query.bindValue(bindName, value);
}

/%= _view->name() %/SelectQuery::Private::WhereDefinition* /%= _view->name() %/SelectQuery::Private::FieldComp::clone()
{
  FieldComp* c = new FieldComp;
  *c = *this;
  return c;
}

/%= _view->name() %/SelectQuery::Private::Operator::~Operator()
{
  delete def_left;
  delete def_right;
}

void /%= _view->name() %/SelectQuery::Private::Operator::setBinding(Sql::Query& _query)
{
  def_left->setBinding(_query);
  def_right->setBinding(_query);
}

QString /%= _view->name() %/SelectQuery::Private::AndOperator::generateWhere(int& _counter)
{
  return "(" + def_left->generateWhere(_counter) + " AND " + def_right->generateWhere(_counter) + ")";     
}

/%= _view->name() %/SelectQuery::Private::WhereDefinition* /%= _view->name() %/SelectQuery::Private::AndOperator::clone()
{
  AndOperator* op = new AndOperator();
  op->def_left  = def_left->clone();
  op->def_right = def_right->clone();
  return op;
}

QString /%= _view->name() %/SelectQuery::Private::OrOperator::generateWhere(int& _counter)
{
  return "(" + def_left->generateWhere(_counter) + " OR " + def_right->generateWhere(_counter) + ")";     
}

/%= _view->name() %/SelectQuery::Private::WhereDefinition* /%= _view->name() %/SelectQuery::Private::OrOperator::clone()
{
  OrOperator* op = new OrOperator();
  op->def_left  = def_left->clone();
  op->def_right = def_right->clone();
  return op;
}
    
/%= _view->name() %/SelectQuery::/%= _view->name() %/SelectQuery(Database* _database) : d(new Private)
{
  d->database = _database;
  d->where = new Private::All;
}
    
/%= _view->name() %/SelectQuery::/%= _view->name() %/SelectQuery(Database* _database, const QString& _fieldName,
                                                                   const QVariant& _value, Sql::Operand _op) : d(new Private)
{
  d->database = _database;
  Private::FieldComp* fc = new Private::FieldComp;
  fc->fieldName = _fieldName;
  fc->value     = _value;
  fc->op        = _op;
  d->where      = fc;
}

/%= _view->name() %/SelectQuery::/%= _view->name() %/SelectQuery() : d(new Private)
{
  d->database = 0;
  d->where    = 0;
}

/%= _view->name() %/SelectQuery::/%= _view->name() %/SelectQuery(/%= _view->name() %/SelectQuery::Private* _d) : d(_d)
{
}

/%= _view->name() %/SelectQuery::/%= _view->name() %/SelectQuery(const /%= _view->name() %/SelectQuery& _rhs) : d(_rhs.d->clone())
{
}

/%= _view->name() %/SelectQuery& /%= _view->name() %/SelectQuery::operator=(const /%= _view->name() %/SelectQuery& _rhs)
{
  delete d->where;
  d->where = nullptr;
  *d = *_rhs.d;
  if(d->where)
  {
    d->where = d->where->clone();
  }
  return *this;
}

/%= _view->name() %/SelectQuery::~/%= _view->name() %/SelectQuery()
{
  delete d->where;
  delete d;
}

std::size_t /%= _view->name() %/SelectQuery::count(int _count) const
{
  QString limit;
  if(_count != 0)
  {
    limit = QString("/%= queryGenerator()->generateLimit() %/").arg(_count);
  } 
  Sql::Query query(d->database->sqlDatabase());
  int count = 0;
  query.prepare(QString("/%= queryGenerator()->generateSelectCount(_view) %/").arg(d->where->generateWhere(count)).arg(limit));
  d->where->setBinding(query);
  
  if(query.exec())
  {
    query.next();
    return Sql::fromVariant<int>(query.value(0));
  } else {
    qWarning() << query.lastQuery() << query.lastError();
    return -1;
  }
}

QList</%= _view->name() %/> /%= _view->name() %/SelectQuery::exec(int _count) const
{
  return exec(nullptr, 0, _count);
}

QList</%= _view->name() %/> /%= _view->name() %/SelectQuery::exec(int _skip, int _count) const
{
  return exec(nullptr, _skip, _count);
}

QList</%= _view->name() %/> /%= _view->name() %/SelectQuery::exec(Sql::Transaction* _transaction, int _count) const
{
  return exec(_transaction, 0, _count);
}

QList</%= _view->name() %/> /%= _view->name() %/SelectQuery::exec(Sql::Transaction* _transaction, int _skip, int _count) const
{
  QString limit;
  if(_count == 0)
  {
    if(_skip != 0) limit = QString("/%= queryGenerator()->generateOffset() %/").arg(_skip);
  } else if(_skip == 0)
  {
    if(_count != 0) limit = QString("/%= queryGenerator()->generateLimit() %/").arg(_count);
  } else
  {
    limit = QString("/%= queryGenerator()->generateOffsetLimit() %/").arg(_skip).arg(_count);
  }
  Sql::Query query = Sql::createQuery(d->database->sqlDatabase(), _transaction);
  if(d->orderBy.empty())
  {
    int count = 0;
    query.prepare(QString("/%= queryGenerator()->generateSelect(_view) %/").arg(d->where->generateWhere(count)).arg(limit));
  } else {
    int count = 0;
    query.prepare(QString("/%= queryGenerator()->generateSelectOrderBy(_view) %/").arg(d->where->generateWhere(count)).arg(d->orderBy.join(",")).arg(limit));
  }
  d->where->setBinding(query);
  /%
  makeResultList(stream, _view, false);
  %/
}

/%= _view->name() %/ /%= _view->name() %/SelectQuery::first(Sql::Transaction* _transaction) const
{
  QList</%= _view->name() %/> list = exec(_transaction, 1);
  if(list.isEmpty()) return /%= _view->name() %/();
  return list.first();
}

/%= _view->name() %/SelectQuery /%= _view->name() %/SelectQuery::operator||(const /%= _view->name() %/SelectQuery& _rhs) const
{
  Q_ASSERT(d->database == _rhs.d->database);
  Private* nd = new Private;
  nd->database = d->database;
  
  if(not d->where) nd->where = _rhs.d->where->clone();
  else if(not _rhs.d->where) nd->where = d->where->clone();
  else {
    Private::OrOperator* aop = new Private::OrOperator;
    aop->def_left = d->where->clone();
    aop->def_right = _rhs.d->where->clone();
    nd->where = aop;
  }
  nd->orderBy = d->orderBy + _rhs.d->orderBy;
  return /%= _view->name() %/SelectQuery(nd);
}

/%= _view->name() %/SelectQuery /%= _view->name() %/SelectQuery::operator&&(const /%= _view->name() %/SelectQuery& _rhs) const
{
  Q_ASSERT(d->database == _rhs.d->database);
  Private* nd = new Private;
  nd->database = d->database;
  if(not d->where) nd->where = _rhs.d->where->clone();
  else if(not _rhs.d->where) nd->where = d->where->clone();
  else {
    Private::AndOperator* aop = new Private::AndOperator;
    aop->def_left = d->where->clone();
    aop->def_right = _rhs.d->where->clone();
    nd->where = aop;
  }
  nd->orderBy = d->orderBy + _rhs.d->orderBy;
  return /%= _view->name() %/SelectQuery(nd);
}
/%
  for(const definitions::Field* field : _view->fields())
  {%/
    /%= _view->name() %/::SelectQuery /%= _view->name() %/SelectQuery::orderBy/%= capitalizeFirstLetter(field->name()) %/(Sql::Order _order) const
    {
      Private* nd = d->clone();
      switch(_order)
      {
      case Sql::Order::ASC:
        nd->orderBy.append("/%= queryGenerator()->generateOrderAsc(field) %/");
        break;
      case Sql::Order::DESC:
        nd->orderBy.append("/%= queryGenerator()->generateOrderDsc(field) %/");
        break;
      }
      return /%= _view->name() %/SelectQuery(nd);
    }
/%}%/
  /%= _view->name() %/::SelectQuery /%= _view->name() %/SelectQuery::randomOrder() const
  {
    Private* nd = d->clone();
    nd->orderBy.append("/%= queryGenerator()->generateRandomOrder() %/");
    return /%= _view->name() %/SelectQuery(nd);
  }

//-----------------------------------//
//           Generate class          //
//-----------------------------------//
  
struct /%= _view->name() %/::Private
{
  Database* database;
  bool valid = false;
  EntryClass entryClass;
  /%
  for(const definitions::Field* field : _view->fields())
  {
    %/
  /%= cppMemberType(field) %/ /%= field->name() %/;/%
  }
  %/
};

//-----------------------------------//
//        Generate static            //
//-----------------------------------//

QString /%= _view->name() %/::viewName()
{
  return QStringLiteral("/%= queryGenerator()->viewName(_view) %/");
}

//-----------------------------------//
//        Generate constructors      //
//-----------------------------------//

void /%= _view->name() %/::assign(const /%= _view->name() %/& _other)
{
  d->database = _other.d->database;
  d->entryClass = _other.d->entryClass;/%
  for(const definitions::Field* field : _view->fields())
  {
    %/
  d->/%= field->name() %/ = _other.d->/%= field->name() %/;/%
  }%/
}

/%= _view->name() %/::/%= _view->name() %/(Database* _database, EntryClass _entryClass/%= constructorArguments(_view) %/) : d(new Private())
{
  d->database = _database;
  d->entryClass = _entryClass;/%
  for(const definitions::Field* field : _view->fields())
  {
    %/
  d->/%= field->name() %/ = _/%= field->name()%/;/%
  }
  %/
}


/%= _view->name() %/::/%= _view->name() %/() : d(new Private)
{
  d->database = nullptr;
}

/%= _view->name() %/::/%= _view->name() %/(const /%= _view->name() %/& _rhs) : d(new Private)
{
  assign(_rhs);
}

/%= _view->name() %/& /%= _view->name() %/::operator=(const /%= _view->name() %/& _rhs)
{
  assign(_rhs);
  return *this;
}

/%= _view->name() %/::~/%= _view->name() %/()
{
  delete d;
}

//-----------------------------------//
//        Comparison operators       //
//-----------------------------------//
bool /%= _view->name() %/::sameAs(const /%= _view->name() %/& _rhs) const
{
  
  return *this == _rhs;
}

bool /%= _view->name() %/::operator==(const /%= _view->name() %/& _rhs) const
{
  return d->database == _rhs.d->database/%
  for(const definitions::Field* field : _view->fields())
  {
    %/ and d->/%= field->name() %/ == _rhs.d->/%= field->name()%//%
  }
  %/;
}

bool /%= _view->name() %/::operator!=(const /%= _view->name() %/& _rhs) const
{
  return not(*this == _rhs);
}

//-----------------------------------//
//                                   //
//-----------------------------------//

/%= _view->name() %/::EntryClass /%= _view->name() %/::entryClass() const
{
  return d->entryClass;
}

QVariant /%= _view->name() %/::nativeVariant() const
{
  switch(d->entryClass)
  {/%
  for(const definitions::View::Entry* e : _view->entries())
  {%/
  case EntryClass::/%= e->klass->name() %/:/% 
    bool has_all_keys = true;
    for(const definitions::Field* f : e->klass->keys())
    {
      if(not e->fields.contains(f->name()))
      {
        has_all_keys = false;
        break;
      }
    }
    if(has_all_keys)
    {%/
    return QVariant::fromValue(/%= e->klass->name() %/(d->database/%
    for(const definitions::Field* f : e->klass->keys())
    {
      %/, d->/%= _view->fields()[e->fields.indexOf(f->name())]->name() %//%
    }
    %/));
    /%
    } else {
      %/
    qWarning("/%= e->klass->name() %/ does not have all keys in /%= _view->name() %/ and cannot be retrived as a native variant.");/%
    }
  }
    %/
  }
  qWarning() << "Invalid entry type: " << d->entryClass;
  return QVariant();
}

bool /%= _view->name() %/::isValid() const
{
  return d->valid;
}

/%
// Add field function: getter, setter and query
for(const definitions::Field* field : _view->fields())
{%/
/%= cppReturnType(field) %/ /%= _view->name() %/::/%= field->name() %/() const
{
  return /%= memberToValue(field, QString("d->"), "d->database") %/;
}

/%= _view->name() %/::SelectQuery /%= _view->name() %/::by/%= capitalizeFirstLetter(field->name()) %/(Database* _database, /%= cppArgType(field) %/ _value, Sql::Operand _op)
{
  return SelectQuery(_database, "/%= queryGenerator()->fieldName(field) %/", /%= argumentToBinding(field, "_value") %/, _op);
}

/%
}
%/

//-----------------------------------//
//           Generate all()          //
//-----------------------------------//
/%= _view->name() %/::SelectQuery /%= _view->name() %/::all(Database* _database)
{
  return SelectQuery(_database);
}

/%
if(d->qtobject)
{%/
#include "moc_/%= _view->name() %/.cpp"/%
}
%/

