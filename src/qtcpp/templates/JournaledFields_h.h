#ifndef _/%= _database->namespaces().join("_").toUpper() %/_FIELDS_H_
#define _/%= _database->namespaces().join("_").toUpper() %/_FIELDS_H_

#include <QVariant>

/%
foreach(QString ns, _database->namespaces())
{
%/
  namespace /%= ns %/ {
/%
}
%/
  namespace JournaledFields
  {
    template<typename _T_>
    class Set
    {
    public:
      Set() : m_data(0)
      {
      }
      Set(quint64 _value) : m_data(_value)
      {}
      void set(_T_ _t) { m_data |= 1 << _t; }
      operator quint64() const { return m_data; }
      operator QVariant() const { return m_data; }
    private:
      quint64 m_data;
    };
    /%
    foreach(const definitions::Class* klass, _database->classes())
    {
      if(klass->isJournaled())
      {
      %/
        enum /%= klass->name() %/Fields
        {
          /%
          QString KLASS_NAME = klass->name().toUpper();
          foreach(const definitions::Field* field, klass->fields())
          {
            if(field->isJournaled())
            {
              %/
              /%= KLASS_NAME %/_/%= field->name().toUpper() %/,
              /%
            }
          }
          %/
        };
      /%
      }
    }
    %/
  }
/%
foreach(QString ns, _database->namespaces())
{
%/
  }
/%
}
%/

#endif
