#include "Qml.h"
#include "Qml_p.h"

#include <QDebug>
#include <QQmlEngine>

#include "Database.h"

/%
for(const definitions::AbstractClass* aklass : klasses)
{
  %/
#include "/%= aklass->name() %/.h"/%
}
%/

/%
for(QString ns : _database->namespaces())
{
%/
namespace /%= ns %/ {
/%
}
%/

namespace Qml {

  void initialise()
  {/%
  QString top_uri = _database->namespaces().join(".");    
    %/
    Database::registerMetaTypes();
    qmlRegisterType<Database>("/%= top_uri %/", 1, 0, "Database");
    qmlRegisterSingletonType<Singleton>("/%= top_uri %/", 1, 0, "/%= _database->namespaces().last() %/", [](QQmlEngine *, QJSEngine *) -> QObject* { return new Singleton; });

/%
  for(const definitions::AbstractClass* aklass : klasses)
  {
    QString uri = top_uri + "." + aklass->name();
    %/
    qmlRegisterUncreatableType</%= _database->namespaces().join("::") %/::
    /%= aklass->name() %/>("/%= top_uri %/", 1, 0, "/%= aklass->name() %/", "Create query");
    qmlRegisterUncreatableType<Qml::/%= aklass->name() %/::SelectQueryFilterBase>("/%= uri %/", 1, 0, "SelectQueryFilterBase", "Abstract");
    qmlRegisterType<Qml::/%= aklass->name() %/::SelectQuery>("/%= uri %/", 1, 0, "SelectQuery");
    qmlRegisterType<Qml::/%= aklass->name() %/::And>("/%= uri %/", 1, 0, "And");
    qmlRegisterType<Qml::/%= aklass->name() %/::Or>("/%= uri %/", 1, 0, "Or");
  /%
    for(const definitions::Field* field : aklass->fields())
    {
      QString classname = "By" + capitalizeFirstLetter(field->name());
      %/
    qmlRegisterType<Qml::/%= aklass->name() %/::/%= classname %/>("/%= uri %/", 1, 0, "/%= classname %/");
    qmlRegisterType<Qml::/%= aklass->name() %/::Order/%= classname %/>("/%= uri %/", 1, 0, "Order/%= classname %/");/%
    }
  }
  %/
    // Register enum in Qt's metatypes/%
    for(const definitions::Enum* e : _database->enums())
    {%/
    qRegisterMetaType</%= e->cppType() %/>("/%= e->cppType() %/");/%
    }
    for(const definitions::AbstractClass* aklass : klasses)
    {
      auto klass = dynamic_cast<const definitions::Class*>(aklass);
      if(klass)
      {
        for(const definitions::Enum* e : klass->enums())
        {%/
    qRegisterMetaType</%= e->cppType() %/>("/%= e->cppType() %/");/%
        }
        for(const definitions::Flags* f : klass->flags())
        {%/
    qRegisterMetaType</%= f->cppType() %/>("/%= f->cppType() %/");/%
        }
      }
    }
    %/
  }
QVariant Singleton::ptr(const QVariant& _var)
{
/%
  for(const definitions::AbstractClass* aklass : klasses)
  {%/
  using /%= aklass->name()%/ = ::/%= _database->namespaces().join("::") %/::/%= aklass->name()%/;
  if(_var.canConvert</%= aklass->name()%/>())
  {
    return QVariant::fromValue(new /%= aklass->name()%/(_var.value</%= aklass->name()%/>()));
  }/%
  }
%/
  qWarning() << _var << " is not a DB object";
  return QVariant();
}

QVariant Singleton::value(const QVariant& _var)
{
/%
  for(const definitions::AbstractClass* aklass : klasses)
  {%/
  using /%= aklass->name()%/ = ::/%= _database->namespaces().join("::") %/::/%= aklass->name()%/;
  if(_var.canConvert<::/%= _database->namespaces().join("::") %/::/%= aklass->name()%/ *>())
  {
    return QVariant::fromValue(*_var.value</%= aklass->name()%/ *>());
  }/%
  }
%/
  qWarning() << _var << " is not a DB object";
  return QVariant();
}

/%
  for(const definitions::AbstractClass* aklass : klasses)
  {
    auto klass = dynamic_cast<const definitions::Class*>(aklass);
    QString full_class_name = _database->namespaces().join("::") + "::" + aklass->name();
    %/
  namespace /%= aklass->name() %/
  {
    SelectQueryFilterBase::SelectQueryFilterBase(Type _type, QObject* _parent) : QObject(_parent), m_type(_type)
    {
    }
    SelectQueryFilterBase::~SelectQueryFilterBase()
    {
    }
    
    SelectQueryFilterContainerBase::SelectQueryFilterContainerBase(QObject* _parent) : SelectQueryFilterBase(Type::Generator, _parent)
    {
    }
    SelectQueryFilterContainerBase::~SelectQueryFilterContainerBase()
    {
    }
    QQmlListProperty<SelectQueryFilterBase> SelectQueryFilterContainerBase::filters()
    {
      return QQmlListProperty<SelectQueryFilterBase>(this, this,
             &SelectQueryFilterContainerBase::appendFilter,
             &SelectQueryFilterContainerBase::filterCount,
             &SelectQueryFilterContainerBase::filter,
             &SelectQueryFilterContainerBase::clearFilters);
    }
    void SelectQueryFilterContainerBase::appendFilter(QQmlListProperty<SelectQueryFilterBase>* _property, SelectQueryFilterBase* _filter)
    {
      SelectQueryFilterContainerBase* self = qobject_cast<SelectQueryFilterContainerBase*>(_property->object);
      if(self->acceptFilter(_filter))
      {
        self->m_filters.append(_filter);
        connect(_filter, SIGNAL(modified()), self, SIGNAL(modified()));
        self->modified();
      } else {
        qWarning() << "Filter rejected:" << _filter;
      }
    }
    qsizetype SelectQueryFilterContainerBase::filterCount(QQmlListProperty<SelectQueryFilterBase>* _property)
    {
      SelectQueryFilterContainerBase* self = qobject_cast<SelectQueryFilterContainerBase*>(_property->object);
      return self->m_filters.size();
    }
    SelectQueryFilterBase* SelectQueryFilterContainerBase::filter(QQmlListProperty<SelectQueryFilterBase>* _property, qsizetype _index)
    {
      SelectQueryFilterContainerBase* self = qobject_cast<SelectQueryFilterContainerBase*>(_property->object);
      return self->m_filters[_index];
    }
    void SelectQueryFilterContainerBase::clearFilters(QQmlListProperty<SelectQueryFilterBase>* _property)
    {
      SelectQueryFilterContainerBase* self = qobject_cast<SelectQueryFilterContainerBase*>(_property->object);
      for(SelectQueryFilterBase* filter : self->m_filters)
      {
        disconnect(filter, SIGNAL(modified()), self, SIGNAL(modified()));
      }
      self->m_filters.clear();
    }
  
    SelectQuery::SelectQuery(QObject* _parent) : QObject(_parent), m_filters(new And(true, this))
    {
      connect(m_filters, &SelectQueryFilterBase::modified, [this]() { if(autoExecute()) execute(); });
    }
    SelectQuery::~SelectQuery()
    {
    }
    void SelectQuery::execute(Sql::Transaction* _transaction)
    {
      QList</%= full_class_name %/> res;
      if(m_database)
      {
        // Execute query
        /%= aklass->name() %/SelectQuery query = m_filters->generateQuery(/%= (klass and klass->classOfContainerDefinition()) ? "*m_container" : "m_database" %/);
        res = query.exec(_transaction, m_skip, m_count);
      }
      m_model.setResults(res);
      emit(resultChanged());
    }
    /%
    if(aklass->doesImplement(definitions::NOTIFICATIONS))
    {
      %/
    void SelectQuery::setAutoExecute(bool _v)
    {
      if(_v != m_autoExecute)
      {
        m_autoExecute = _v;
        emit(autoExecuteChanged());
        updateAutoExecute();
      }
    }
    void SelectQuery::updateAutoExecute()
    {
      if(not m_database) return;
      if(m_autoExecute)
      {
        connect(m_/%= databaseOrContainerMemberName(aklass) %/, SIGNAL(/%= uncapitalizeFirstLetter(aklass->name()) %/Changed()), this, SLOT(execute()));
        execute();
      } else {
        disconnect(m_/%= databaseOrContainerMemberName(aklass) %/, SIGNAL(/%= uncapitalizeFirstLetter(aklass->name()) %/Changed()), this, SLOT(execute()));
      }
    }
    /%
    }
    %/
    Or::Or(QObject* _parent) : SelectQueryFilterContainerBase(_parent)
    {
    }
    Or::~Or()
    {
    }
    /%= aklass->name() %/SelectQuery Or::generateQuery(/%= databaseOrContainer(aklass, _database->namespaces().join("::") + "::") %/) const
    {
      switch(m_filters.size())
      {
        case 0:
          return /%= aklass->name() %/SelectQuery();
        case 1:
          return m_filters.first()->generateQuery(/%= databaseOrContainerParameterName(aklass) %/);
        default:
        {
          /%= aklass->name() %/SelectQuery query = m_filters.first()->generateQuery(/%= databaseOrContainerParameterName(aklass) %/);
          for(int i = 1; i < m_filters.size(); ++i)
          {
            query = query or m_filters[i]->generateQuery(/%= databaseOrContainerParameterName(aklass) %/);
          }
          return query;
        }
      }
    }
    bool Or::acceptFilter(const SelectQueryFilterBase* _filter) const
    {
      return _filter->type() == Type::Generator;
    }
    And::And(QObject* _parent) : SelectQueryFilterContainerBase(_parent), m_acceptAll(false)
    {
    }
    And::And(bool _acceptAll, QObject* _parent) : SelectQueryFilterContainerBase(_parent), m_acceptAll(_acceptAll)
    {
    }
    And::~And()
    {
    }
    /%= aklass->name() %/SelectQuery And::generateQuery(/%= databaseOrContainer(aklass, _database->namespaces().join("::") + "::") %/) const
    {
      QList<SelectQueryFilterBase*> generators, modifiers;
      
      for(SelectQueryFilterBase* filter : m_filters)
      {
        switch(filter->type())
        {
          case Type::Generator:
            generators.append(filter);
            break;
          case Type::Modifier:
            modifiers.append(filter);
            break;
        }
      }
      
      /%= aklass->name() %/SelectQuery query;
      
      switch(generators.size())
      {
        case 0:
          query = /%= _database->namespaces().join("::") %/::/%= aklass->name() %/::all(/%= databaseOrContainerParameterName(aklass) %/);
          break;
        case 1:
          query = generators.first()->generateQuery(/%= databaseOrContainerParameterName(aklass) %/);
          break;
        default:
        {
          query = generators.first()->generateQuery(/%= databaseOrContainerParameterName(aklass) %/);
          for(int i = 1; i < generators.size(); ++i)
          {
            query = query and generators[i]->generateQuery(/%= databaseOrContainerParameterName(aklass) %/);
          }
          break;
        }
      }
      for(SelectQueryFilterBase* modifier : modifiers)
      {
        query = modifier->generateQuery(query);
      }
      return query;
    }
    bool And::acceptFilter(const SelectQueryFilterBase* _filter) const
    {
      return m_acceptAll or _filter->type() == Type::Generator;
    }
  /%
    for(const definitions::Field* field : aklass->fields())
    {
      QString classname = "By" + capitalizeFirstLetter(field->name());
      %/
      /%= classname %/::/%= classname %/(QObject* _parent) : SelectQueryFilterBase(Type::Generator, _parent)
      {
      }
      /%= classname %/::~/%= classname %/()
      {
      }
      Sql::Operand /%= classname %/::sqlOperand() const
      {
        switch(m_operand)
        {
          case Operand::EQ: return Sql::Operand::EQ;
          case Operand::GT: return Sql::Operand::GT;
          case Operand::LT: return Sql::Operand::LT;
          case Operand::LTE: return Sql::Operand::LTE;
          case Operand::GTE: return Sql::Operand::GTE;
          case Operand::NEQ: return Sql::Operand::NEQ;
          case Operand::LIKE: return Sql::Operand::LIKE;
          
        }
        qFatal("ERROR");
      }
      /%= aklass->name() %/SelectQuery /%= classname %/::generateQuery(/%= databaseOrContainer(aklass, _database->namespaces().join("::") + "::") %/) const
      {
        return /%= _database->namespaces().join("::") %/::/%= aklass->name() %/::by/%= capitalizeFirstLetter(field->name()) %/(/%= databaseOrContainerParameterName(aklass) %/, /%= memberToValue(field, "m_", databaseOrContainerParameterName(aklass) ) %/, sqlOperand());
      }/%
      classname = "Order" + classname;
      %/
      /%= classname %/::/%= classname %/(QObject* _parent) : SelectQueryFilterBase(Type::Modifier, _parent)
      {
      }
      /%= classname %/::~/%= classname %/()
      {
      }
      Sql::Order /%= classname %/::sqlOrder() const
      {
        switch(m_order)
        {
          case Order::Asc: return Sql::Order::ASC;
          case Order::Desc: return Sql::Order::DESC;
        }
        qFatal("ERROR");
      }
      /%= aklass->name() %/SelectQuery /%= classname %/::generateQuery(const /%= aklass->name() %/SelectQuery& _query) const
      {
        return _query.orderBy/%= capitalizeFirstLetter(field->name()) %/(sqlOrder());
      }/%
    } %/
  } // namespace /%= aklass->name() %/ /%
  }
  %/
} // namespace qml  
  
/%
for(QString ns : _database->namespaces())
{
%/
} // namespace /%= ns %/
/%
}
%/


#include "moc_Qml_p.cpp"
