/%  QString guard = _database->namespaces().join("_").toUpper() + "_QML_H_";
%/#ifndef _/%= guard %/
#define _/%= guard %/

namespace /%= _database->namespaces().join("::") %/::Qml
{
  void initialise();
}

#endif
