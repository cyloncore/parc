/%  QString guard = _database->namespaces().join("_").toUpper() + "_FORWARD_H_";
%/#ifndef _/%= guard %/
#define _/%= guard %/


class QDateTime;
class QString;

/%
for(const definitions::Forward* fw : _database->forwards())
{
%/
namespace /%= fw->namespaces().join("::") %/
{
  class /%= fw->name() %/;
}/%
}%/

namespace /%= _database->namespaces().join("::") %/
{
  class Database;
/%for(const definitions::Class* klass : _database->classes())
  {
    %/
  class /%= klass->name() %/;
  class /%= klass->name() %/SelectQuery;/%

   for(const definitions::Class* sklass : klass->classOfSubDefinitions())
    {
    %/
  class /%= sklass->name() %/;
  class /%= sklass->name() %/SelectQuery;/%
    }
  }
  for(const definitions::View* view : _database->views())
  {
    %/
  class /%= view->name() %/;
  class /%= view->name() %/SelectQuery;/%
  }
  %/
}

#endif
