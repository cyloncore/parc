/%  QString guard = _database->namespaces().join("_").toUpper() + "_DATABASE_H_";
%/ #ifndef _/%= guard %/
#define _/%= guard %/
#include <QDateTime>
#include <QString>

#include "Sql.h"

#include "TypesDefinitions.h"/%
for(const definitions::Class* klass : _database->classes())
{
  %/
#include "/%= klass->name() %/.h"/%
}
%/

namespace /%= _database->namespaces().join("::") %/ {
/%
  if(_database->hasJournal())
  {
    %/
    class Journal;
    /%
  }
  %/
  class Database/%
    if(d->qtobject)
    {
      %/: public QObject/%
    }
  %/
  {/%
    if(d->qtobject)
    {
      %/
    Q_OBJECT/%
      if(has_notifications)
      {%/
    Q_PROPERTY(bool notificationsEnabled READ notificationsEnabled WRITE setNotificationsEnabled NOTIFY notificationsEnabledChanged)/%
      }
      if(has_update_notifications)
      {%/
    Q_PROPERTY(bool updateNotificationsEnabled READ updateNotificationsEnabled WRITE setUpdateNotificationsEnabled NOTIFY updateNotificationsEnabledChanged)/%
      }%/
    Q_PROPERTY(/%= databae_property_type %/ sqlDatabase READ sqlDatabase WRITE setSqlDatabase NOTIFY sqlDatabaseChanged);/%
    }%/
  public:
    static void registerMetaTypes();
  public:
    Database();
    Database(const Sql::Database& _database/%= generateDatabaseArguments(_database, true) %/);
    ~Database();
  public:
    void setSqlDatabase(const Sql::Database& _database);
    Sql::Database sqlDatabase() const;
    /%
    if(_database->hasJournal())
    {
      %/
      Journal* journal() const;
      /%
    }
    if(d->qtobject)
    {
      %/
  signals:
    void sqlDatabaseChanged();/%
    }
    %/
  public:
  /%
  for(const definitions::Class* klass : _database->classes())
  { 
    %/
    /% if(d->qtobject) { %/Q_INVOKABLE/% } %/ /%= fullname(klass) %/ create/%= klass->name() %/(/%= createArguments(klass, true) %/);
    /% if(d->qtobject) { %/Q_INVOKABLE/% } %/ /%= fullname(klass) %/ create/%= klass->name() %/(Sql::Transaction* _transaction, /%= createArguments(klass, true) %/);
    /%
  }
  %/
  public:
/% 
  for(const definitions::Object* object : _database->objects())
  { %/
    /%= object->klass()->name() %/ /%= object->name() %/() const;
/% } %/
public:
/% 
  for(const definitions::Property* property : _database->properties())
  { %/
    /%= cppReturnType(property) %/ /%= property->name() %/() const;
    /% if(not property->isConstant())
    { %/
    void set/%= capitalizeFirstLetter(property->name()) %/ ( /%= cppArgType(property) %/);
    /% }
  } %/
  private:
    /%
    if(_database->doesImplement(definitions::INIT))
    {
      %/
      void init();
      /%
    }
    if(_database->doesImplement(definitions::CLEANUP))
    {
      %/
      void cleanup();
      /%
    }
    if(_database->doesImplement(definitions::EXTENDED))
    {
      %/
#include "Database_.h"
      /%
    }
    if(has_notifications)
    {
      %/
    public:
      bool notificationsEnabled() const;
      void setNotificationsEnabled(bool _v);
    signals:
      void notificationsEnabledChanged();
      /%
      for(const definitions::Class* klass : _database->classes())
      { 
        if(klass->doesImplement(definitions::NOTIFICATIONS))
        {
          QString lc_name = uncapitalizeFirstLetter(klass->name());
        %/
    void /%= lc_name %/Inserted(const /%= _database->namespaces().join("::") %/::/%= klass->name() %/& _/%= lc_name %/);
    void /%= lc_name %/Removed();
    void /%= lc_name %/Changed();/%
        }
      }
      for(const definitions::View* view : _database->views())
      { 
        if(view->doesImplement(definitions::NOTIFICATIONS))
        {
          QString lc_name = uncapitalizeFirstLetter(view->name());
        %/
    void /%= lc_name %/Changed();/%
        }
      }
    }
    if(has_update_notifications)
    {
      %/
    public:
      bool updateNotificationsEnabled() const;
      void setUpdateNotificationsEnabled(bool _v);
    signals:
      void updateNotificationsEnabledChanged();
      /%
      for(const definitions::Class* klass : _database->classes())
      { 
        if(klass->doesImplement(definitions::UPDATENOTIFICATION))
        {
          QString lc_name = uncapitalizeFirstLetter(klass->name());
        %/
      void /%= lc_name %/Updated();/%
        }
      }
    }
    %/
  private:
    struct Private;
    Private* const d;
  };  
}

#include <QMetaType>
Q_DECLARE_METATYPE(::/%= _database->namespaces().join("::") %/::Database*)
#endif
