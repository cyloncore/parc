#include "JournalEntry.h"

using namespace /%= _database->namespaces().join("::") %/;

struct JournalEntry::Private
{
};

JournalEntry::JournalEntry(Private* _d) : d(_d)
{
}

JournalEntry::~JournalEntry()
{
  delete d;
}

/%
foreach(const definitions::Class* klass, _database->classes())
{
  if(klass->isJournaled())
  {
  %/
/%= klass->name() %/JournalEntry::/%= klass->name() %/JournalEntry() : JournalEntry(new Private)
{
}
/%= klass->name() %/JournalEntry::/%= klass->name() %/JournalEntry(const /%= klass->name() %/JournalEntry& _rhs) : JournalEntry(new Private)
{
}
/%= klass->name() %/JournalEntry& /%= klass->name() %/JournalEntry::operator=(const /%= klass->name() %/JournalEntry& _rhs)
{
  return *this;
}
/%= klass->name() %/JournalEntry::~/%= klass->name() %/JournalEntry()
{
}
  /%
  }
}
foreach(const definitions::Mapping* mapping, _database->mappings())
{
  if(mapping->isJournaled())
  {
    const QString KLASS_NAME = mapping->a()->name() + mapping->b()->name() + "MappingJournalEntry";
    %/
/%= KLASS_NAME %/::/%= KLASS_NAME %/() : JournalEntry(new Private)
{
}
/%= KLASS_NAME %/::/%= KLASS_NAME %/(const /%= KLASS_NAME %/& _rhs) : JournalEntry(new Private)
{
}
/%= KLASS_NAME %/& /%= KLASS_NAME %/::operator=(const /%= KLASS_NAME %/& _rhs)
{
  return *this;
}
/%= KLASS_NAME %/::~/%= KLASS_NAME %/()
{
}
    /%
  }
}
%/
