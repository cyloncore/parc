#include "JournaledFields.h"
#include <QFlags>

/%
foreach(QString ns, _database->namespaces())
{
%/
  namespace /%= ns %/ {
/%
}
%/
  class Database;
  /%
  foreach(const definitions::Class* klass, _database->classes())
  {
    if(klass->isJournaled())
    {
    %/
      class /%= klass->name() %/;
      class /%= klass->name() %/JournalEntry;
    /%
    }
  }
  %/
  class Journal
  {
    friend class Database;
    /%
    foreach(const definitions::Class* klass, _database->classes())
    {
      if(klass->isJournaled())
      {
      %/
        friend class /%= klass->name() %/;
      /%
      }
    }
    %/
  private:
    Journal(Database* _database);
    ~Journal();
  /%
  foreach(const definitions::Class* klass, _database->classes())
  {
    if(klass->isJournaled())
    {
    %/
    public:
      QList</%= klass->name() %/JournalEntry> /%= uncapitalizeFirstLetter(klass->name()) %/Entry(unsigned int _startId = 0, unsigned int _count = -1);
    private:
      void recordDelete(const /%= klass->name() %/& _object);
      void recordUpdate(const /%= klass->name() %/& _object, const QFlags<JournaledFields::/%= klass->name() %/Fields>& _updatedFields);
      void recordInsert(const /%= klass->name() %/& _object);
      /%
      if(klass->isTree() and klass->journaled() == parc::definitions::JOURNALED)
      {
        %/
        void recordChildAdd(const /%= klass->name() %/& _parent, const /%= klass->name() %/& _child);
        void recordChildRemove(const /%= klass->name() %/& _parent, const /%= klass->name() %/& _child);
        /%
      }
    }
  }
  foreach(const definitions::Mapping* mapping, _database->mappings())
  {
    if(mapping->isJournaled())
    {
      %/
    private:
      void recordMapping(const /%= mapping->a()->name() %/& _objectA, const /%= mapping->b()->name() %/& _objectB);
      void recordUnmapping(const /%= mapping->a()->name() %/& _objectA, const /%= mapping->b()->name() %/& _objectB);
      /%
    }
  }
  %/
  private:
    struct Private;
    Private* const d;
  };
/%
foreach(QString ns, _database->namespaces())
{
%/
  }
/%
}
%/
