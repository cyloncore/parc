#include "/%= _klass->name() %/_p.h"
#include <QDebug>
#include <QStringList>

#include "Sql.h"

/%
  if(_klass->isJournaled())
  {
    %/
#include "Journal.h"
    /%
  }
for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
{
  %/
#include "/%= sklass->name() %/_p.h"/%
}
%/

using namespace /%= _database->namespaces().join("::") %/;

//-----------------------------------//
//         Generate SelectQuery      //
//-----------------------------------//

struct /%= _klass->name() %/SelectQuery::Private
{
  Private* clone() const;
  struct WhereDefinition
  {
    virtual ~WhereDefinition() {}
    virtual QString generateWhere(int& _counter) = 0;
    virtual void setBinding(Sql::Query& _query) = 0;
    virtual WhereDefinition* clone() = 0;
  };
  struct All : public WhereDefinition
  {
    virtual QString generateWhere(int& _counter);
    virtual void setBinding(Sql::Query& _query);
    virtual WhereDefinition* clone();
  };
  struct FieldComp : public WhereDefinition
  {
    QString fieldName;
    QString bindName;
    QVariant value;
    Sql::Operand op;
    virtual QString generateWhere(int& _counter);
    virtual void setBinding(Sql::Query& _query);
    virtual WhereDefinition* clone();
  };
  struct Operator : public WhereDefinition
  {
    virtual ~Operator();
    WhereDefinition* def_left;
    WhereDefinition* def_right;    
    virtual void setBinding(Sql::Query& _query);
  };
  struct AndOperator : public Operator
  {
    virtual QString generateWhere(int& _counter);
    virtual WhereDefinition* clone();
  };
  struct OrOperator : public Operator
  {
    virtual QString generateWhere(int& _counter);
    virtual WhereDefinition* clone();
  };

  Database* database;
  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
  /%= _klass->classOfContainerDefinition()->name() %/ container;/%
  }
  %/
  WhereDefinition* where;
  QStringList orderBy;
};

/%= _klass->name() %/SelectQuery::Private* /%= _klass->name() %/SelectQuery::Private::clone() const
{
  Private* nd = new Private(*this);
  if(where)
  {
    nd->where = where->clone();
  }
  return nd;
}


QString /%= _klass->name() %/SelectQuery::Private::All::generateWhere(int& )
{
  return "/%= queryGenerator()->generateTrue() %/";
}

void /%= _klass->name() %/SelectQuery::Private::All::setBinding(Sql::Query& /*_query*/)
{
}

/%= _klass->name() %/SelectQuery::Private::WhereDefinition* /%= _klass->name() %/SelectQuery::Private::All::clone()
{
  return new All;
}


QString /%= _klass->name() %/SelectQuery::Private::FieldComp::generateWhere(int& _counter)
{
  bindName = QString(":bv%1").arg(++_counter);
  switch(op)
  {
    case Sql::Operand::EQ:
      return QString("/%= queryGenerator()->generateWhereEq() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::GT:
      return QString("/%= queryGenerator()->generateWhereGT() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::LT:
      return QString("/%= queryGenerator()->generateWhereLT() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::LTE:
      return QString("/%= queryGenerator()->generateWhereLTE() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::GTE:
      return QString("/%= queryGenerator()->generateWhereGTE() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::NEQ:
      return QString("/%= queryGenerator()->generateWhereNEq() %/").arg(fieldName).arg(bindName);
      break;
    case Sql::Operand::LIKE:
      return QString("/%= queryGenerator()->generateWhereLike() %/").arg(fieldName).arg(bindName);
      break;
  }
  qFatal("Unknown query");
}

void /%= _klass->name() %/SelectQuery::Private::FieldComp::setBinding(Sql::Query& _query)
{
  _query.bindValue(bindName, value);
}

/%= _klass->name() %/SelectQuery::Private::WhereDefinition* /%= _klass->name() %/SelectQuery::Private::FieldComp::clone()
{
  FieldComp* c = new FieldComp;
  *c = *this;
  return c;
}

/%= _klass->name() %/SelectQuery::Private::Operator::~Operator()
{
  delete def_left;
  delete def_right;
}

void /%= _klass->name() %/SelectQuery::Private::Operator::setBinding(Sql::Query& _query)
{
  def_left->setBinding(_query);
  def_right->setBinding(_query);
}

QString /%= _klass->name() %/SelectQuery::Private::AndOperator::generateWhere(int& _counter)
{
  return "(" + def_left->generateWhere(_counter) + " AND " + def_right->generateWhere(_counter) + ")";     
}

/%= _klass->name() %/SelectQuery::Private::WhereDefinition* /%= _klass->name() %/SelectQuery::Private::AndOperator::clone()
{
  AndOperator* op = new AndOperator();
  op->def_left  = def_left->clone();
  op->def_right = def_right->clone();
  return op;
}

QString /%= _klass->name() %/SelectQuery::Private::OrOperator::generateWhere(int& _counter)
{
  return "(" + def_left->generateWhere(_counter) + " OR " + def_right->generateWhere(_counter) + ")";     
}

/%= _klass->name() %/SelectQuery::Private::WhereDefinition* /%= _klass->name() %/SelectQuery::Private::OrOperator::clone()
{
  OrOperator* op = new OrOperator();
  op->def_left  = def_left->clone();
  op->def_right = def_right->clone();
  return op;
}
    
/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery(/%= databaseOrContainer(_klass) %/) : d(new Private)
{
  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
  d->database = _container.database();
  d->container = _container;/%
  } else {
    %/
  d->database = _database;/%
  }
  %/
  d->where = new Private::All;
}
    
/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery(/%= databaseOrContainer(_klass) %/, const QString& _fieldName,
                                                                   const QVariant& _value, Sql::Operand _op) : d(new Private)
{/%
  if(_klass->classOfContainerDefinition())
  {
    %/
  d->database = _container.database();
  d->container = _container;/%
  } else {
    %/
  d->database = _database;/%
  }
  %/
  Private::FieldComp* fc = new Private::FieldComp;
  fc->fieldName = _fieldName;
  fc->value     = _value;
  fc->op        = _op;
  d->where      = fc;
}

/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery() : d(new Private)
{
  d->database = 0;
  d->where    = 0;
}

/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery(/%= _klass->name() %/SelectQuery::Private* _d) : d(_d)
{
}

/%= _klass->name() %/SelectQuery::/%= _klass->name() %/SelectQuery(const /%= _klass->name() %/SelectQuery& _rhs) : d(_rhs.d->clone())
{
}

/%= _klass->name() %/SelectQuery& /%= _klass->name() %/SelectQuery::operator=(const /%= _klass->name() %/SelectQuery& _rhs)
{
  delete d->where;
  d->where = nullptr;
  *d = *_rhs.d;
  if(d->where)
  {
    d->where = d->where->clone();
  }
  return *this;
}

/%= _klass->name() %/SelectQuery::~/%= _klass->name() %/SelectQuery()
{
  delete d->where;
  delete d;
}

std::size_t /%= _klass->name() %/SelectQuery::count(int _count) const
{
  QString limit;
  if(_count != 0)
  {
    limit = QString("/%= queryGenerator()->generateLimit() %/").arg(_count);
  } 
  Sql::Query query(d->database->sqlDatabase());
  int count = 0;
  query.prepare(QString("/%= queryGenerator()->generateSelectCount(_klass) %/")/%= tableQueryArg(_klass, "d->container") %/.arg(d->where->generateWhere(count)).arg(limit));
  d->where->setBinding(query);
  
  if(query.exec())
  {
    query.next();
    return Sql::fromVariant<int>(query.value(0));
  } else {
    qWarning() << query.lastQuery() << query.lastError();
    return -1;
  }
}

QList</%= _klass->name() %/> /%= _klass->name() %/SelectQuery::exec(int _count) const
{
  return exec(nullptr, 0, _count);
}

QList</%= _klass->name() %/> /%= _klass->name() %/SelectQuery::exec(int _skip, int _count) const
{
  return exec(nullptr, _skip, _count);
}

QList</%= _klass->name() %/> /%= _klass->name() %/SelectQuery::exec(Sql::Transaction* _transaction, int _count) const
{
  return exec(_transaction, 0, _count);
}

QList</%= _klass->name() %/> /%= _klass->name() %/SelectQuery::exec(Sql::Transaction* _transaction, int _skip, int _count) const
{
  QString limit;
  if(_count == 0)
  {
    if(_skip != 0) limit = QString("/%= queryGenerator()->generateOffset() %/").arg(_skip);
  } else if(_skip == 0)
  {
    if(_count != 0) limit = QString("/%= queryGenerator()->generateLimit() %/").arg(_count);
  } else
  {
    limit = QString("/%= queryGenerator()->generateOffsetLimit() %/").arg(_skip).arg(_count);
  }
  Sql::Query query = Sql::createQuery(d->database->sqlDatabase(), _transaction);
  if(d->orderBy.empty())
  {
    int count = 0;
    query.prepare(QString("/%= queryGenerator()->generateSelect(_klass) %/")/%= tableQueryArg(_klass, "d->container") %/.arg(d->where->generateWhere(count)).arg(limit));
  } else {
    int count = 0;
    query.prepare(QString("/%= queryGenerator()->generateSelectOrderBy(_klass) %/")/%= tableQueryArg(_klass, "d->container") %/.arg(d->where->generateWhere(count)).arg(d->orderBy.join(",")).arg(limit));
  }
  d->where->setBinding(query);
  /%
  makeResultList(stream, _klass, false);
  %/
}

/%= _klass->name() %/ /%= _klass->name() %/SelectQuery::first(Sql::Transaction* _transaction) const
{
  QList</%= _klass->name() %/> list = exec(_transaction, 1);
  if(list.isEmpty()) return /%= _klass->name() %/();
  return list.first();
}

/%= _klass->name() %/SelectQuery /%= _klass->name() %/SelectQuery::operator||(const /%= _klass->name() %/SelectQuery& _rhs) const
{
  Q_ASSERT(d->database == _rhs.d->database);
  Private* nd = new Private;
  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
  nd->database = d->container.database();
  nd->container = d->container;/%
  } else {
    %/
  nd->database = d->database;/%
  }
  %/
  if(not d->where) nd->where = _rhs.d->where->clone();
  else if(not _rhs.d->where) nd->where = d->where->clone();
  else {
    Private::OrOperator* aop = new Private::OrOperator;
    aop->def_left = d->where->clone();
    aop->def_right = _rhs.d->where->clone();
    nd->where = aop;
  }
  nd->orderBy = d->orderBy + _rhs.d->orderBy;
  return /%= _klass->name() %/SelectQuery(nd);
}

/%= _klass->name() %/SelectQuery /%= _klass->name() %/SelectQuery::operator&&(const /%= _klass->name() %/SelectQuery& _rhs) const
{
  Q_ASSERT(d->database == _rhs.d->database);
  Private* nd = new Private;
  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
  nd->database = d->container.database();
  nd->container = d->container;/%
  } else {
    %/
  nd->database = d->database;/%
  }
  %/
  if(not d->where) nd->where = _rhs.d->where->clone();
  else if(not _rhs.d->where) nd->where = d->where->clone();
  else {
    Private::AndOperator* aop = new Private::AndOperator;
    aop->def_left = d->where->clone();
    aop->def_right = _rhs.d->where->clone();
    nd->where = aop;
  }
  nd->orderBy = d->orderBy + _rhs.d->orderBy;
  return /%= _klass->name() %/SelectQuery(nd);
}
/%
  for(const definitions::Field* field : _klass->fields())
  {%/
    /%= _klass->name() %/::SelectQuery /%= _klass->name() %/SelectQuery::orderBy/%= capitalizeFirstLetter(field->name()) %/(Sql::Order _order) const
    {
      Private* nd = d->clone();
      switch(_order)
      {
      case Sql::Order::ASC:
        nd->orderBy.append("/%= queryGenerator()->generateOrderAsc(field) %/");
        break;
      case Sql::Order::DESC:
        nd->orderBy.append("/%= queryGenerator()->generateOrderDsc(field) %/");
        break;
      }
      return /%= _klass->name() %/SelectQuery(nd);
    }
/%}%/
  /%= _klass->name() %/::SelectQuery /%= _klass->name() %/SelectQuery::randomOrder() const
  {
    Private* nd = d->clone();
    nd->orderBy.append("/%= queryGenerator()->generateRandomOrder() %/");
    return /%= _klass->name() %/SelectQuery(nd);
  }

//-----------------------------------//
//           Generate class          //
//-----------------------------------//
  

//-----------------------------------//
//        Generate static            //
//-----------------------------------//

QString /%= _klass->name() %/::tableName()
{
  return QStringLiteral("/%= queryGenerator()->tableName(_klass) %/");
}

/%
if(_klass->classOfContainerDefinition())
{
%/
bool /%= _klass->name() %/::lock(const /%= _klass->classOfContainerDefinition()->name() %/& _container, Sql::Transaction* _transaction)
{
  QString table_name = tableName().arg(/%= valueToMember(_klass->classOfContainerDefinition(), "_container") %/);
/%
} else {%/
bool /%= _klass->name() %/::lock(Sql::Transaction* _transaction)
{
  QString table_name = tableName();
/%
}%/
  Sql::Query query = Sql::createQuery(_transaction);
  query.prepare(QString("/%= queryGenerator()->generateLockQuery() %/").arg(table_name));
  return query.exec();
}

//-----------------------------------//
//        Generate constructors      //
//-----------------------------------//

void /%= _klass->name() %/::assign(const /%= _klass->name() %/& _other)
{
  d->database = _other.d->database;
  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
  d->container = _other.d->container;
    /%
  }
  %/
  d->modified = _other.d->modified;
  /%
  for(const definitions::Field* field : _klass->fields())
  {
    %/
  d->/%= field->name() %/ = _other.d->/%= field->name() %/;/%
    if(not field->options().testFlag(definitions::Field::Key)
        and not field->options().testFlag(definitions::Field::Constant))
    {
      %/
  d->/%= field->name() %/_modified = _other.d->/%= field->name() %/_modified;/%
    }
  }%/
}

/%= _klass->name() %/::/%= _klass->name() %/() : d(new Private)
{
  d->database = nullptr;
  /%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
}

/%= _klass->name() %/::/%= _klass->name() %/(const /%= _klass->name() %/& _rhs) : QObject(), d(new Private())
{
  assign(_rhs);
  d->resetOwned();
  /%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
}

/%= _klass->name() %/& /%= _klass->name() %/::operator=(const /%= _klass->name() %/& _rhs)
{
  assign(_rhs);
  d->resetOwned();
  /%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
  }
  %/
  return *this;
}

/%= _klass->name() %/::/%= _klass->name() %/(/%= databaseOrContainer(_klass) %/ /%= constructorArguments(_klass) %/) : d(new Private())
{  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
  d->database = _container.database();
  d->container = _container;/%
  } else {
    %/
  d->database = _database;/%
  }
  for(const definitions::Field* field : _klass->fields())
  {
      %/
  d->/%= field->name() %/ = _/%= field->name()%/;/%
  }
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
  init();/%
    if(d->qtobject)
    {
      for(const definitions::Field* field : _klass->fields())
      {
        if(not isConstantField(field))
        {
        %/
  emit(/%= field->name()%/Changed());/%
        }
      }
    }
  }
  %/
}

/%= _klass->name() %/::/%= _klass->name() %/(/%= databaseOrContainer(_klass) %/ /%= constructorKeyArguments(_klass) %/, bool _refresh) : d(new Private())
{/%
  if(_klass->classOfContainerDefinition())
  {
    %/
  d->database = _container.database();
  d->container = _container;/%
  } else {
    %/
  d->database = _database;/%
  }
  for(const definitions::Field* field : _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      %/
  d->/%= field->name() %/ =  _/%= field->name()%/;/%
    }
  }
  %/
  if(_refresh)
  {
    refresh();
  }
  /%
  if(_klass->doesImplement(definitions::INIT))
  {
    %/
    init();
    /%
    if(d->qtobject)
    {
      for(const definitions::Field* field : _klass->fields())
      {
        if(not isConstantField(field))
        {
        %/
    emit(/%= field->name()%/Changed());/%
        }
      }
    }
  }
  %/
}

/%= _klass->name() %/::~/%= _klass->name() %/()
{
  Q_ASSERT(not d->modified);
  /%
  if(_klass->doesImplement(definitions::CLEANUP))
  {
    %/
  cleanup();/%
  }
  %/
  delete d;
}

  //-----------------------------------//
  //   Generate comparison operators   //
  //-----------------------------------//
bool /%= _klass->name() %/::sameAs(const /%= _klass->name() %/& _rhs) const
{
  
  return d->database == _rhs.d->database/%
  if(_klass->classOfContainerDefinition())
  {
    %/ and d->container.sameAs(_rhs.d->container)/%
  }
  foreach(const definitions::Field* field, _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
       %/ and d->/%= field->name() %/ == _rhs.d->/%= field->name()%//%
    }
  }
  %/;
}

bool /%= _klass->name() %/::operator==(const /%= _klass->name() %/& _rhs) const
{
  return sameAs(_rhs)/%
  foreach(const definitions::Field* field, _klass->fields())
  {
    if(not field->options().testFlag(definitions::Field::Key))
    {
       %/ and d->/%= field->name() %/ == _rhs.d->/%= field->name()%//%
    }
  }
  %/;
}

bool /%= _klass->name() %/::operator!=(const /%= _klass->name() %/& _rhs) const
{
  return not(*this == _rhs);
}

//-----------------------------------//
//          Generate refresh         //
//-----------------------------------//
void /%= _klass->name() %/::refresh(Sql::Transaction* _transaction)
{
  d->modified = false;
  Sql::Query query = Sql::createQuery(d->database->sqlDatabase(), _transaction);
  query.prepare(QString("/%= queryGenerator()->generateRefreshQuery(_klass) %/")/%= tableQueryArg(_klass, "d->container") %/);
/%  setKeysOnQuery(stream, "query", _klass); %/
    if(query.exec())
    {
      if(query.first())
      {
/% {
    int k = 0;
    foreach(const definitions::Field* field, _klass->fields())
    {
      %/
  {
    /%= cppMemberType(field) %/ new_value = Sql::fromVariant</%= cppMemberType(field) %/>(query.value(/%= k %/));
    if(new_value != d->/%= field->name() %/)
    {
      d->/%= field->name() %/ = new_value;/%
      if(d->qtobject and not isConstantField(field))
      {
        %/
      emit(/%= field->name()%/Changed());/%
      }%/
    }
  }
      /%
      if(not field->options().testFlag(definitions::Field::Key)
        and not field->options().testFlag(definitions::Field::Constant))
      {
        %/
        d->/%= field->name() %/_modified = false;
        /%
      }
      ++k;
    }
  } %/
    }
  } else {
    qWarning() << query.lastQuery() << query.lastError();
  }
}

  //-----------------------------------//
  //          Generate record         //
  //-----------------------------------//
void /%= _klass->name() %/::record(Sql::Transaction* _transaction)
{/%
  if(_klass->hasDependents())
  {
    %/
  updateDependents();/%
  }
  %/
  if(not d->modified) return;
  d->modified = false;
  Sql::Query query = Sql::createQuery(d->database->sqlDatabase(), _transaction);
  QString fields;
/%
  if(_klass->isJournaled())
  {
    %/
    QFlags<JournaledFields::/%= _klass->name() %/Fields> journaled_fields;
    /%
  }
  QString KLASS_NAME = _klass->name().toUpper();
  foreach(const definitions::Field* field, _klass->fields())
  {
    if(not field->options().testFlag(definitions::Field::Constant) and not field->options().testFlag(definitions::Field::Key))
    {
      %/
      if(d->/%= field->name() %/_modified)
      {
        if(not fields.isEmpty()) fields += ",";
        fields += "\\"/%= queryGenerator()->fieldName(field) %/\\"=:/%= field->name() %/";
        /%
        if(field->isJournaled())
        {
          %/
          journaled_fields |= JournaledFields::/%= KLASS_NAME %/_/%= field->name().toUpper() %/;
          /%
        }
        %/
      }
      /%
    }
  }
  if(_klass->isJournaled())
  {
    %/
    d->database->journal()->recordUpdate(/%= _klass->name() %/(d->database
      /%
      foreach(const definitions::Field* field, _klass->fields())
      {
        if(field->options().testFlag(definitions::Field::Key))
        {
          %/
          , /%= memberToValue(field, QString("d->"), "d->database") %/
          /%
        }
      }
      %/
    ), journaled_fields);
    /%
  }
%/
  query.prepare(QString("/%= queryGenerator()->generateRecordQuery(_klass) %/")/%= tableQueryArg(_klass, "d->container") %/.arg(fields));
/%
  foreach(const definitions::Field* field, _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      %/
      query.bindValue(":/%= field->name() %/", Sql::toVariant(d->/%= field->name() %/));
      /%
    } else if(not field->options().testFlag(definitions::Field::Constant))
    {
      %/
      if(d->/%= field->name() %/_modified)
      {
        query.bindValue(":/%= field->name() %/", Sql::toVariant(d->/%= field->name() %/));
        d->/%= field->name() %/_modified = false;
      }
      /%
    }
  }
%/
  if(not query.exec())
  {
      qWarning() << query.lastQuery() << query.lastError();
  }
}
 
  //-----------------------------------//
  //          Generate erase           //
  //-----------------------------------//
void /%= _klass->name() %/::erase(Sql::Transaction* _transaction)
{
  Sql::Query query = Sql::createQuery(d->database->sqlDatabase(), _transaction);
  query.prepare(QString("/%= queryGenerator()->generateDeleteQuery(_klass) %/")/%= tableQueryArg(_klass, "d->container") %/);

  // Set the keys
/%
  foreach(const definitions::Field* field, _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      %/
      query.bindValue(":/%= field->name() %/", Sql::toVariant(d->/%= field->name() %/));
      /%
    }
  }
  if(_klass->isJournaled() == parc::definitions::JOURNALED)
  {
    %/
    d->database->journal()->recordDelete(*this);
    /%
  }
  %/
  
  if(not query.exec())
  {
      qWarning() << query.lastQuery() << query.lastError();
  }
  
}

bool /%= _klass->name() %/::isValid(Sql::Transaction* _transaction) const
{
  if(d->database)
  {
    Sql::Query query = Sql::createQuery(d->database->sqlDatabase(), _transaction);
    query.prepare(QString("/%= queryGenerator()->generateExistQuery(_klass) %/")/%= tableQueryArg(_klass, "d->container") %/);
/% setKeysOnQuery(stream, "query", _klass); %/
    if(query.exec() and query.next()) return true;
  }
  return false;
}

bool /%= _klass->name() %/::isModified() const
{
  return d->modified;
}

/%
//----------------------------------------//
// Generate subdefinition object creation //
//----------------------------------------//
for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
{
  %/
  /%= sklass->name() %/ /%= _klass->name() %/::create/%= sklass->name() %/(/%= createArguments(sklass, false) %/)
  {
    return create/%= sklass->name() %/(nullptr, /%
  bool first = true;
  if(sklass->isTree())
  { %/_parent/%
    first = false;
  }
  for(const definitions::Field* field : sklass->fields())
  {
    if(not field->options().testFlag(definitions::Field::Auto))
    {
      if(not first) { %/, /% }
      first = false;
      %/_/%= field->name() %//%
    }
  } %/);
  }
  /%= sklass->name() %/ /%= _klass->name() %/::create/%= sklass->name() %/(Sql::Transaction* _transaction, /%= createArguments(sklass, false) %/)
  {
    /%
    const definitions::Class* klass = sklass;
    %/
    Sql::Query query = Sql::createQuery(d->database->sqlDatabase(), _transaction);
    /%= klass->name() %/ object;
    /%= _klass->name() %/ database_ptr = *this;
    /%
    const bool include_auto_field_in_query = false;
#include "Database_createObject_cpp.h"
    if(klass->journaled() == parc::definitions::JOURNALED)
    {
      %/
      d->journal->recordInsert(object);
      /%
    }
    %/
    return object;
  }
  /%
}
%/


/%
//-----------------------------------//
//       Generate tree access        //
//-----------------------------------//
  
if(_klass->isTree())
{ %/
  QList</%= _klass->name() %/> /%= _klass->name() %/::children() const
  {
    Sql::Query query(d->database->sqlDatabase());
    query.prepare("/%= queryGenerator()->generateTreeChildrenQuery(_klass) %/");
    query.bindValue(":id", Sql::toVariant(/%= _klass->keys().first()->name() %/()));
    /% makeResultList(stream, _klass, true); %/
  }
  QList</%= _klass->name() %/> /%= _klass->name() %/::parents() const
  {
    Sql::Query query(d->database->sqlDatabase());
    query.prepare("/%= queryGenerator()->generateTreeParentsQuery(_klass) %/");
    query.bindValue(":id", /%= _klass->keys().first()->name() %/());
    /% makeResultList(stream, _klass, true); %/
  }
  bool /%= _klass->name() %/::isChildOf(const /%= _klass->name() %/& _parent, int _max_distance) const
  {
    Sql::Query query(d->database->sqlDatabase());
    query.prepare("/%= queryGenerator()->generateTreeIsChildQuery(_klass) %/");
    query.bindValue(":parent", Sql::toVariant(_parent./%= _klass->keys().first()->name() %/()));
    query.bindValue(":child", Sql::toVariant(/%= _klass->keys().first()->name() %/()));
    query.bindValue(":distance", Sql::toVariant(_max_distance));
    return query.exec() and query.first();
  }
  void /%= _klass->name() %/::addChild(const /%= _klass->name() %/& _child)
  {
    if(not _child.isChildOf(*this, 1))
    {
      Sql::Query query(d->database->sqlDatabase());
      query.prepare("/%= queryGenerator()->generateTreeAddChildQuery(_klass) %/");
      query.bindValue(":parent", Sql::toVariant(/%= _klass->keys().first()->name() %/()));
      query.bindValue(":child", Sql::toVariant(_child./%= _klass->keys().first()->name() %/()));
      if(not query.exec())
      {
        qWarning() << "Failed to create ancestor between" << id() << "and" << _child.id() << ":" << query.lastError() << query.lastQuery();
      }
      /%
      if(_klass->journaled() == parc::definitions::JOURNALED)
      {
        %/
        d->database->journal()->recordChildAdd(*this, _child);
        /%
      }
      %/
    }
  }

/%}
  
//-----------------------------------//
//     Generate children access      //
//-----------------------------------//
// Add function to access children
if(not _klass->children().empty())
{
  foreach(const definitions::Class* child, _klass->children())
  {
    if(hasSingleChild(child, _klass))
    {
      const definitions::Field* field = child->parentFields(_klass).first();
      %/
      /%= child->name() %/ /%= _klass->name() %/::/%= childfunctionname(child->name()) %/() const
      {
        QList</%= child->name() %/> child = /%= child->name() %/::by/%= capitalizeFirstLetter(field->name()) %/(d->database, *this).exec();
        Q_ASSERT(child.size() <= 1);
        if(child.isEmpty()) return /%= child->name() %/();
        return child.first();
      }
      /%
    } else {
      %/
      /%= child->name() %/SelectQuery /%= _klass->name() %/::/%= childrenfunctionname(child->name()) %/() const
      {
        return /%
      QList<const definitions::Field*> parentfields = child->parentFields(_klass);
      bool isfirst = true;
      foreach(const definitions::Field* parentfield, child->parentFields(_klass))
      {
        if(isfirst)
        {
          isfirst = false;
        } else {
            %/ || /%
        } %/
        /%= child->name() %/::by/%= capitalizeFirstLetter(parentfield->name()) %/(d->database, *this)/%
      }
      %/;
      }/%
    }
  }
}

// Add field function: getter, setter and query
for(const definitions::Field* field : _klass->fields())
{%/
  /%= cppReturnType(field) %/ /%= _klass->name() %/::/%= field->name() %/() const
  {
    return /%= memberToValue(field, QString("d->"), "d->database") %/;
  }
  /%
  if(not field->options().testFlag(definitions::Field::Constant) and not field->options().testFlag(definitions::Field::Auto) and not field->options().testFlag(definitions::Field::Key))
  {%/
    void /%= _klass->name() %/::set/%= capitalizeFirstLetter(field->name()) %/(/%= cppArgType(field) %/ _value)
    {
      d->modified = true;
      d->/%= field->name() %/_modified = true;
      d->/%= field->name() %/ = /%= valueToMember(field->sqlType(), "_value") %/;/%
    if(d->qtobject and not isConstantField(field))
    {
      %/
      emit(/%= field->name()%/Changed());/%
    }%/
    }
    /%
  }
  %/
  /%= _klass->name() %/::SelectQuery /%= _klass->name() %/::by/%= capitalizeFirstLetter(field->name()) %/(/%= databaseOrContainer(_klass) %/, /%= cppArgType(field) %/ _value, Sql::Operand _op)
  {
    return SelectQuery(/%= databaseOrContainerParameterName(_klass) %/, "/%= queryGenerator()->fieldName(field) %/", /%= argumentToBinding(field, "_value") %/, _op);
  }
  /%
}
%/
//-----------------------------------//
//           Generate all()          //
//-----------------------------------//
/%= _klass->name() %/::SelectQuery /%= _klass->name() %/::all(/%= databaseOrContainer(_klass) %/)
{
  return SelectQuery(/%= databaseOrContainerParameterName(_klass) %/);
}

//-----------------------------------//
//           Generate clear()        //
//-----------------------------------//

void /%= _klass->name() %/::clear(/%= databaseOrContainer(_klass) %/, Sql::Transaction* _transaction)
{
  /%
  if(_klass->classOfContainerDefinition())
  {
    %/
  Sql::Query query = Sql::createQuery(_container.database()->sqlDatabase(), _transaction);/%
  } else {
    %/
  Sql::Query query = Sql::createQuery(_database->sqlDatabase(), _transaction);/%
  }
  %/
  query.prepare(QString("/%= queryGenerator()->generateRemoveAll(_klass) %/")/%= tableQueryArg(_klass, "_container") %/);
  if(not query.exec())
  {
    qWarning() << "Failed to clear: " << tableName();
  }
}

//-----------------------------------//
//        Generate mapping( )        //
//-----------------------------------//

/%
foreach(const definitions::Mapping* mapping, _klass->mappings())
{
  const definitions::Class* klass = mapping->other(_klass);
  %/
//     header_stream %/    QList</%= klass->name() %/> /%= functiongetallname(klass->name()) %/() const;
//                      "    void add/%= klass->name() %/(const /%= klass->name() %/& _other);
//                      "    void remove/%= klass->name() %/(const /%= klass->name() %/& _other);
  QList</%= klass->name() %/> /%= _klass->name() %/::/%= functiongetallname(klass->name()) %/() const
  {
    Sql::Query query(d->database->sqlDatabase());
    query.prepare("/%= queryGenerator()->generateGetMappingsQuery(mapping, _klass) %/");
    query.bindValue(":id_self", Sql::toVariant(/%= _klass->keys().first()->name() %/()));
    QList</%= klass->name() %/> result;
    if(not query.exec())
    {
      qWarning() << query.lastQuery() << query.lastError();
      return result;
    }
    while(query.next())
    {
      result.append(/%= klass->name() %/(d->database, query.value(0).toInt()));
    }
    return result;
  }
  void /%= _klass->name() %/::remove/%= klass->name() %/(const /%= klass->name() %/& _other)
  {
    Sql::Query query(d->database->sqlDatabase());
    query.prepare("/%= queryGenerator()->generateRemoveMappingQuery(mapping, _klass) %/");
    query.bindValue(":id_self", Sql::toVariant(/%= _klass->keys().first()->name() %/()));
    query.bindValue(":id_other", Sql::toVariant(_other./%= mapping->other(_klass)->keys().first()->name() %/()));
    if(not query.exec())
    {
      qWarning() << query.lastQuery() << query.lastError();
      return;
    }    
    /%
    if(mapping->isJournaled())
    {
      if(mapping->a() == _klass)
      {
        %/
        d->database->journal()->recordUnmapping(*this, _other);
        /%
      } else {
        Q_ASSERT(mapping->b() == _klass);
        %/
        d->database->journal()->recordUnmapping(_other, *this);
        /%
      }
    }
    %/
  }
  bool /%= _klass->name() %/::has/%= klass->name() %/(const /%= klass->name() %/& _other) const
  {
    Sql::Query query(d->database->sqlDatabase());
    query.prepare("/%= queryGenerator()->generateHasMappingQuery(mapping, _klass) %/");
    query.bindValue(":id_self", Sql::toVariant(/%= _klass->keys().first()->name() %/()));
    query.bindValue(":id_other", Sql::toVariant(_other./%= mapping->other(_klass)->keys().first()->name() %/()));
    if(not query.exec())
    {
      qWarning() << query.lastQuery() << query.lastError();
      return false;
    }
    query.next();
    return query.value(0).toInt() == 1;
  }
  void /%= _klass->name() %/::add/%= klass->name() %/(const /%= klass->name() %/& _other)
  {
    Sql::Query query(d->database->sqlDatabase());
    query.prepare("/%= queryGenerator()->generateAddMappingQuery(mapping, _klass) %/");
    query.bindValue(":id_self", Sql::toVariant(/%= _klass->keys().first()->name() %/()));
    query.bindValue(":id_other", Sql::toVariant(_other./%= mapping->other(_klass)->keys().first()->name() %/()));
    if(not query.exec())
    {
      qWarning() << query.lastQuery() << query.lastError();
      return;
    }
    /%
    if(mapping->isJournaled())
    {
      if(mapping->a() == _klass)
      {
        %/
        d->database->journal()->recordMapping(*this, _other);
        /%
      } else {
        Q_ASSERT(mapping->b() == _klass);
        %/
        d->database->journal()->recordMapping(_other, *this);
        /%
      }
    }
    %/
  }
  /%
}

if((d->type == Type::CyqlopsDBSQLite or d->type == Type::CyqlopsDBPostgreSQL) and d->qtobject and has_notifications)
{
  Q_ASSERT(d->type != Type::CyqlopsDBPostgreSQL); // Not quiet implemented yet
  %/
//-----------------------------------//
//      Generate notifications()     //
//-----------------------------------//

bool /%= _klass->name() %/::notificationsEnabled() const
{
  return d->notificationsEnabled;
}

void /%= _klass->name() %/::setNotificationsEnabled(bool _v)
{
  if(d->notificationsEnabled == _v) return;
  d->notificationsEnabled = _v;
  emit(notificationsEnabledChanged());
  
  if(d->notificationsEnabled)
  {
    Sql::Query query(d->database->sqlDatabase());
    query.prepare("/%= queryGenerator()->generateGetRowID(_klass) %/");/%
    for(const parc::definitions::Field* key : _klass->keys())
    {
      %/
    query.bindValue(":/%= key->name() %/", Sql::toVariant(d->/%= key->name() %/));/%
    }%/
    if(not query.exec())
    {
      qWarning() << query.lastQuery() << query.lastError();
      return;
    }
    if(not query.first())
    {
      qWarning() << "No ROWID found in database";
      return;
    }
    quint64 row_id = query.value(0).toInt();
    d->notificationsHandle = QObject::connect(d->database->sqlDatabase(), &Cyqlops::DB::SQLite::Database::changed, [this, row_id](Cyqlops::DB::SQLite::Database::ChangeType _type, const QString& _tablename, qint64 _rowid) {
      // Check for modification to current
      if(_tablename == QStringLiteral("/%= queryGenerator()->tableName(_klass) %/"))
      {
        if(_rowid == row_id)
        {
          switch(_type)
          {
            case Cyqlops::DB::SQLite::Database::ChangeType::Insert:
              break;
            case Cyqlops::DB::SQLite::Database::ChangeType::Delete:
              emit(removed());
              break;
            case Cyqlops::DB::SQLite::Database::ChangeType::Update:
              emit(updated());
              break;
          }
        }
        return;
      }

      // Check for modification to sub-objects
      /%
      for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
      { 
        if(sklass->doesImplement(definitions::NOTIFICATIONS))
        {
          QString lc_name = uncapitalizeFirstLetter(sklass->name());
        %/
      if(_tablename == QString("/%= queryGenerator()->tableName(sklass) %/").arg(d->/%= _klass->keys().first()->name() %/))
      {
        switch(_type)
        {
          case Cyqlops::DB::SQLite::Database::ChangeType::Insert:
            emit(/%= lc_name %/Inserted(/%= sklass->name() %/(*this, _rowid, false)));
            break;
          case Cyqlops::DB::SQLite::Database::ChangeType::Delete:
            emit(/%= lc_name %/Removed());
            break;
          case Cyqlops::DB::SQLite::Database::ChangeType::Update:
            break;
        }
        emit(/%= lc_name %/Changed());
        return;
      }/%
        }
      }%/
    });
  } else {
    QObject::disconnect(d->notificationsHandle);
  }
  
  emit(notificationsEnabled());
}


/%
}

if(d->type == Type::CyqlopsDBPostgreSQL and d->qtobject and has_update_notifications)
{
  %/

bool /%= _klass->name() %/::updateNotificationsEnabled() const
{
  return d->updateNotificationsEnabled;
}

void /%= _klass->name() %/::setUpdateNotificationsEnabled(bool _v)
{
  if(d->updateNotificationsEnabled == _v) return;
  d->updateNotificationsEnabled = _v;
  emit(updateNotificationsEnabledChanged());
  Sql::Query query(d->database->sqlDatabase());
  QString tablename;
  if(d->updateNotificationsEnabled)
  {/%
  for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
  {
    if(sklass->doesImplement(definitions::UPDATENOTIFICATION))
    {
      QString lc_name = uncapitalizeFirstLetter(sklass->name());
      %/
    tablename = QString("/%= queryGenerator()->tableName(sklass) %/").arg(d->/%= _klass->keys().first()->name() %/);
    query.prepare(QString("/%= queryGenerator()->createUpdatedTrigger(sklass)%/").arg(d->/%= _klass->keys().first()->name() %/));
    if(not query.exec())
    {
      qWarning() << query.lastQuery() << query.lastError();
    }
    d->/%= sklass->name() %/UpdateNotificationsHandle = d->database->sqlDatabase()->notificationsManager()->listen(qPrintable(tablename + "updated"), [this](const QByteArray&)
      {
        emit(/%= lc_name %/Updated());
      }
      );/%
    }
  }%/
  } else {/%
  for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
  {
    if(sklass->doesImplement(definitions::UPDATENOTIFICATION))
    {
      %/
    query.prepare(QString("/%= queryGenerator()->deleteUpdatedTrigger(sklass) %/").arg(d->/%= _klass->keys().first()->name() %/));
    if(not query.exec())
    {
      qWarning() << query.lastQuery() << query.lastError();
    }
    QObject::disconnect(d->/%= sklass->name() %/UpdateNotificationsHandle);/%
    }
  }%/
  }
}
  /%
}

%/

//-----------------------------------//
//        Generate database()        //
//-----------------------------------//
Database* /%= _klass->name() %/::database() const
{
  return d->database;
}
/%
if(d->qtobject)
{%/
#include "moc_/%= _klass->name() %/.cpp"/%
}
%/
