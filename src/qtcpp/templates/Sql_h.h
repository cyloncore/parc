/%  QString guard = _database->namespaces().join("_").toUpper() + "_SQL_H_";
%/#ifndef _/%= guard %/
#define _/%= guard %/

#include <QVariant>

/%
switch(d->type)
{
  case Type::CyqlopsDBSQLite:
  %/
#include <Cyqlops/DB/SQLite/Database.h>
#include <Cyqlops/DB/SQLite/Query.h>
/%
    break;
  case Type::CyqlopsDBPostgreSQL:
  %/
#include <Cyqlops/DB/PostgreSQL/Database.h>
#include <Cyqlops/DB/PostgreSQL/Query.h>
#include <Cyqlops/DB/PostgreSQL/Result.h>
#include <Cyqlops/DB/PostgreSQL/NotificationsManager.h>
/%
    break;
  case Type::QtSQL:
  %/
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
/%
    break;
}

foreach(QString ns, _database->namespaces())
{
%/
namespace /%= ns %/ {
/%
}
%/

namespace Sql
{
  enum class Order
  {
    ASC,
    DESC
  };
  enum class Operand
  {
    EQ, GT, LT, LTE, GTE, NEQ, LIKE
  };
/%
switch(d->type)
{
  case Type::CyqlopsDBSQLite:
  %/
  typedef Cyqlops::DB::SQLite::Query Query;
  typedef Cyqlops::DB::SQLite::Database* Database;
  typedef Cyqlops::DB::SQLite::Transaction Transaction;
  inline Query createQuery(Database _database, Transaction* _transaction)
  {
    if(_transaction)
    {
      return Query(_transaction);
    } else {
      return Query(_database);
    }
  }
  inline Query createQuery(Transaction* _transaction)
  {
    return Query(_transaction);
  }
  /%
    break;
  case Type::CyqlopsDBPostgreSQL:
  %/
  namespace __CyqlopsDBPostgreSQLCompatibility__
  {
    class Query
    {
    public:
      Query() {}
      Query(Cyqlops::DB::PostgreSQL::Database* _database, Cyqlops::DB::PostgreSQL::Transaction* _transaction = nullptr) : m_database(_database), m_transaction(_transaction) {}
      ~Query() {}
      bool exec(const QString& _query)
      {
        return prepare(_query) and exec();
      }
      bool exec()
      {
        Cyqlops::DB::PostgreSQL::Query query = m_transaction ? Cyqlops::DB::PostgreSQL::Query(m_transaction) : Cyqlops::DB::PostgreSQL::Query(m_database);
        query.prepare(m_query);
        for(QHash<QString, QVariant>::const_iterator cit = m_bindings.cbegin(); cit != m_bindings.cend(); ++cit)
        {
          query.bindValue(cit.key(), cit.value());
        }
        m_result = query.execute();
        m_currentIndex = -1;
        return m_result;
      }
      bool prepare(const QString& _query)
      {
        m_query = _query;
        return true;
      }
      void bindValue(const QString &placeholder, const QVariant &val)
      {
        m_bindings[placeholder] = val;
      }
      bool isSelect() const
      {
        return m_result.tuples() > 0;
      }
      bool first()
      {
        m_currentIndex = 0;
        return m_currentIndex < m_result.tuples();
      }
      bool next()
      {
        ++m_currentIndex;
        return m_currentIndex < m_result.tuples();
      }
      QVariant value(int _index) const
      {
        return m_result.value(m_currentIndex, _index);
      }
      QVariant value(QString _name) const
      {
        return m_result.value(m_currentIndex, _name.toLatin1());
      }
      QString lastQuery() const
      {
        return m_query;
      }
      QString lastError() const
      {
        return m_result.error();
      }
    private:
      QString m_query;
      QHash<QString, QVariant> m_bindings;
      Cyqlops::DB::PostgreSQL::Result m_result;
      Cyqlops::DB::PostgreSQL::Database* m_database = nullptr;
      Cyqlops::DB::PostgreSQL::Transaction* m_transaction = nullptr;
      int m_currentIndex = 0;
    };
  }
  typedef __CyqlopsDBPostgreSQLCompatibility__::Query Query;
  typedef Cyqlops::DB::PostgreSQL::Database* Database;
  typedef Cyqlops::DB::PostgreSQL::Transaction Transaction;
  inline Query createQuery(Database _database, Transaction* _transaction)
  {
    return Query(_database, _transaction);
  }
  inline Query createQuery(Transaction* _transaction)
  {
    return Query(nullptr, _transaction);
  }
  /%
    break;
  case Type::QtSQL:
  %/
  typedef QSqlQuery Query;
  typedef QSqlDatabase Database;
  typedef void Transaction;
  inline Query createQuery(Database _database, Transaction* /*_transaction*/)
  {
    return Query(_database);
  }
  inline Query createQuery(Transaction* _transaction)
  {
    qFatal("Transactions are not supported with QtSQL");
  }
/%
    break;
}
%/

  namespace details
  {

    template<typename T>
    struct is_qflags : std::false_type {};

    template<typename T>
    struct is_qflags<QFlags<T>> : std::true_type {};

    template<typename _T_>
    struct variant_conversion_helper;
    
    template<typename _T_>
    requires (not std::is_enum_v<_T_> and not is_qflags<_T_>::value)
    struct variant_conversion_helper<_T_>
    {
      static QVariant toVariant(const _T_& _t)
      {
        return QVariant::fromValue(_t);
      }
      static _T_ fromVariant(const QVariant& _variant)
      {
        return _variant.value<_T_>();
      }
    };
    template<typename _T_>
    requires (std::is_enum<_T_>::value)
    struct variant_conversion_helper<_T_>
    {
      static QVariant toVariant(const _T_& _t)
      {
        return QVariant::fromValue(int(_t));
      }
      static _T_ fromVariant(const QVariant& _variant)
      {
        return _T_(_variant.value<int>());
      }
    };
    template<typename _T_>
    struct variant_conversion_helper<QFlags<_T_>>
    {
      static QVariant toVariant(const QFlags<_T_>& _t)
      {
        return QVariant::fromValue(int(_t));
      }
      static QFlags<_T_> fromVariant(const QVariant& _variant)
      {
        return QFlags<_T_>(_variant.value<int>());
      }
    };
  }
  
  template<typename _T_>
  QVariant toVariant(const _T_& _t)
  {
    return details::variant_conversion_helper<_T_>::toVariant(_t);
  }
  template<typename _T_>
  _T_ fromVariant(const QVariant& _variant)
  {
    return details::variant_conversion_helper<_T_>::fromVariant(_variant);
  }
  
}

/%
// Close namespace
foreach(QString ns, _database->namespaces())
{
%/
  }
/%
}
%/

#endif
