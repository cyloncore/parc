#include "Database.h"
#include <QDebug>
#include <QVariant>

/%
if(_database->hasJournal())
{
  %/
#include "Journal.h"/%
}
foreach(const definitions::Class* klass, _database->classes())
{
  %/
#include "/%= klass->name() %/_p.h"/%
}
%/
using namespace /%= _database->namespaces().join("::") %/;

struct Database::Private
{
  Sql::Database database;
  /%
  if(_database->hasJournal())
  {
    %/
    Journal* journal;
    /%
  }
  foreach(const definitions::Object* object, _database->objects())
  { %/
    /%= object->klass()->name() %/ /%= object->name() %/;
/% }
  foreach(const definitions::Property* property, _database->properties())
  { %/
    /%= cppMemberType(property) %/ /%= property->name() %/;
/% } %/
  int version();
  void setVersion(int _version);
  void initOrUpdate();
  Database* self;/%
  if(has_notifications)
  {%/
  bool notificationsEnabled = false;
  QMetaObject::Connection notificationsHandle;/%  
  }
  if(has_update_notifications)
  {%/
  bool updateNotificationsEnabled = false;/%
  
    for(const definitions::Class* klass : _database->classes())
    { 
      if(klass->doesImplement(definitions::UPDATENOTIFICATION))
      {%/
  QMetaObject::Connection /%= klass->name() %/UpdateNotificationsHandle;/%
      }
    }
  }
  %/
};

int Database::Private::version()
{
  Sql::Query query(database);
  if(query.exec("/%= queryGenerator()->versionQuery() %/") and query.isSelect() and query.first())
  {
    return query.value(0).toInt();
  } else {
    return -1;
  }
}

void Database::Private::setVersion(int _version)
{
  Sql::Query query(database);
  query.prepare("/%= queryGenerator()->setVersionQuery() %/ ");
  query.bindValue(":version", _version);
  if(not query.exec())
  {
    qWarning() << "In setting version: " << query.lastQuery() << query.lastError();
  }
}

void Database::Private::initOrUpdate()
{
  switch(version())
  {
  case -1:
    {
      Sql::Query query(database);
      if(not query.exec("/%= queryGenerator()->createInfoTable() %/"))
      {
        qWarning() << "During execution of " << query.lastQuery() << " got " << query.lastError();
        return;
      };
/%
  for(const definitions::Class* klass : _database->classes())
  {
    %/
    if(not query.exec("/%= queryGenerator()->createTable(klass) %/"))
    {
      qWarning() << "During execution of " << query.lastQuery() << " got " << query.lastError();
      return;
    }
    /%
    for(const definitions::Field* field : klass->fields())
    {
      if(field->sqlType()->type() == SqlType::SQLQUERY)
      {
        %/
    if(not query.prepare("/%= field->sqlType()->sqlQuery() %/"))
    {
      qWarning() << "During execution of " << query.lastQuery() << " got " << query.lastError();
      return;
    }
    query.bindValue(":table", "/%= queryGenerator()->tableName(klass) %/");
    query.bindValue(":column_name", "/%= queryGenerator()->fieldName(field) %/");
    if(not query.exec())
    {
      qWarning() << "During execution of " << query.lastQuery() << " got " <<query.lastError();
      return;
    }
    /%
      }
    }
    for(const definitions::Field* field : klass->fields())
    {
      if(field->options().testFlag(definitions::Field::Indexed))
      {
        QList<const definitions::Field*> fields;
        fields.append(field);
        %/
        if(not query.exec("/%= queryGenerator()->createIndex(klass, fields) %/"))
        {
          qWarning() << "During execution of " << query.lastQuery() << " got " << query.lastError();
          return;
        }
        /%
      }
    }
    for(const definitions::Index* index : klass->indexes())
    {
      %/
      if(not query.exec("/%= queryGenerator()->createIndex(klass, index->fields()) %/"))
      {
        qWarning() << "During execution of " << query.lastQuery() << " got " << query.lastError();
        return;
      }
      /%
    }
    if(klass->isJournaled())
    {
      %/
      if(not query.exec("/%= queryGenerator()->createJournalTable(klass) %/"))
      {
        qWarning() << "During execution of " << query.lastQuery() << " got " << query.lastError();
        return;
      }
      /%
    }
    if(klass->isTree())
    {
      %/
      if(not query.exec("/%= queryGenerator()->createTreeTable(klass) %/"))
      {
        qWarning() << "During execution of " << query.lastQuery() << " got " << query.lastError();
        return;
      }
      /%
      if(klass->journaled() == definitions::JOURNALED)
      {
        %/
        if(not query.exec("/%= queryGenerator()->createJournalTreeTable(klass) %/"))
        {
          qWarning() << "During execution of " << query.lastQuery() << " got " << query.lastError();
          return;
        }        
        /%
      }
    }
  }

  for(const definitions::View* view : _database->views())
  {
    %/
    if(not query.exec("/%= queryGenerator()->createView(view) %/"))
    {
      qWarning() << "During execution of " << query.lastQuery() << " got " << query.lastError();
      return;
    }/%
  }
  for(const definitions::Mapping* map : _database->mappings())
  {
    %/
    if(not query.exec("/%= queryGenerator()->createMappingTable(map) %/"))
    {
      qWarning() << query.lastError();
      return;
    }
    /%
    if(map->isJournaled())
    {
      %/
      if(not query.exec("/%= queryGenerator()->createMappingJournalTable(map) %/"))
      {
        qWarning() << query.lastError();
        return;
      }
      /%
    }
  }
      for(const definitions::Object* object : _database->objects())
      {
        const definitions::Class* klass = object->klass();
        %/
        {
          /%
          int index = 0;
          if(klass->isTree())
          {
            %/
            /%= klass->name() %/ _parent = /%= object->arguments().at(index) %/;
            /%
            ++index;
          }
          foreach(const definitions::Field* field, klass->fields())
          {
            %/
            /%= cppReturnType(field) %/ _/%= field->name() %/ =
            /% if(index < object->arguments().size())
            {
              %/
              /%= object->arguments().at(index) %/
              /%
              ++index;
            } else {
              %/
              /%= defaultValue(field) %/
              /%
            }
            %/;/%
          }
          %/
          Sql::Query query(database);
          /%= klass->name() %/ object;
          Database* database_ptr = self;
          /%
          const bool include_auto_field_in_query = true;
#include "Database_createObject_cpp.h"
          %/
        }
        /%
      }
      %/
      setVersion(1);
    }
  }
}

void Database::registerMetaTypes()
{
  qRegisterMetaType<Database*>();
  /%
  for(const definitions::Class* klass : _database->classes())
  {%/
  qRegisterMetaType</%= _database->namespaces().join("::") %/::/%= klass->name() %/>();
  qRegisterMetaType</%= _database->namespaces().join("::") %/::/%= klass->name() %/ *>();/%
  }
  %/
}

Database::Database() : d(new Private)
{
  registerMetaTypes();
  /%
  if(_database->hasJournal())
  {
    %/
    d->journal = new Journal(this);
    /%
  }
  %/
  d->self     = this;
}

Database::Database(const Sql::Database& _database/%= generateDatabaseArguments(_database, false) %/) : Database()
{
  setSqlDatabase(_database);/%
  foreach(const definitions::Property* property, _database->properties())
  {%/
    d->/%= property->name() %/ = _/%= property->name() %/;
  /%
  }%/
}

Database::~Database()
{
  /%
  if(_database->hasJournal())
  {
    %/
    delete d->journal;
    /%
  }
  if(_database->doesImplement(definitions::CLEANUP))
  {
    %/
    cleanup();
    /%
  }
  if(d->type == Type::CyqlopsDBSQLite or d->type == Type::CyqlopsDBPostgreSQL)
  {
    %/
  delete d->database;
    /%
  }
  %/
  delete d;
}  

  
void Database::setSqlDatabase(const Sql::Database& _database)
{
  d->database = _database;
  /% if(d->type == Type::CyqlopsDBSQLite or d->type == Type::CyqlopsDBPostgreSQL) { %/
  if(not d->database->isOpen())
  {
    d->database->open();
  }
  /%
  } else { %/
  if(not d->database.isOpen())
  {
    d->database.open();
  }
  /%
  }%/
  Sql::Query query(d->database);/%
  for(QString query : queryGenerator()->initialisationQueries())
  {%/
  query.exec("/%= query.replace('\n', "\\n\"\n\"") %/");/%
  }%/
  d->initOrUpdate();
/%
  foreach(const definitions::Object* object, _database->objects())
  {%/
    d->/%= object->name() %/ = /%= object->klass()->name() %/(this, /%= object->arguments().at(object->klass()->isTree() ? 1 : 0) %/);
/%}
  if(_database->doesImplement(definitions::INIT))
  {
    %/
  init();
    /%
  }
  %/
  emit(sqlDatabaseChanged());
}

Sql::Database Database::sqlDatabase() const
{
  return d->database;
}


/%
if(_database->hasJournal())
{
  %/
  Journal* Database::journal() const
  {
    return d->journal;
  }
  /%
}
%/

/%
  for(const definitions::Class* klass : _database->classes())
  { %/
    /%= klass->name() %/ Database::create/%= klass->name() %/(/%= createArguments(klass, false) %/)
    {
      return create/%= klass->name() %/(nullptr, /%
  bool first = true;
  if(klass->isTree())
  { %/_parent/%
    first = false;
  }
  for(const definitions::Field* field : klass->fields())
  {
    if(not field->options().testFlag(definitions::Field::Auto))
    {
      if(not first) { %/, /% }
      first = false;
      %/_/%= field->name() %//%
    }
  } %/);
    }
    /%= klass->name() %/ Database::create/%= klass->name() %/(Sql::Transaction* _transaction, /%= createArguments(klass, false) %/)
    {
      Sql::Query query = Sql::createQuery(d->database, _transaction);
      /%= klass->name() %/ object;
      Database* database_ptr = this;
      /%
      const bool include_auto_field_in_query = false;
#include "Database_createObject_cpp.h"
      if(klass->journaled() == parc::definitions::JOURNALED)
      {
        %/
        d->journal->recordInsert(object);
        /%
      }
      %/
      return object;
    }
/%}
  foreach(const definitions::Property* property, _database->properties())
  {%/
    /%= cppReturnType(property) %/ Database::/%= property->name() %/() const
  {
   return d->/%= property->name() %/;
  }
 /% if(not property->isConstant())
    { %/
    void Database::set/%= capitalizeFirstLetter(property->name()) %/(/%= cppArgType(property) %/ _value)
    {
      d->/%= property->name() %/ = _value;
    }
   /% }
  }
  foreach(const definitions::Object* object, _database->objects())
  {%/
    /%= object->klass()->name() %/ Database::/%= object->name() %/() const
  {
    return d->/%= object->name() %/;
  };
/% }

// Notfications implementation
if((d->type == Type::CyqlopsDBPostgreSQL or d->type == Type::CyqlopsDBSQLite) and d->qtobject and has_notifications)
{
  Q_ASSERT(d->type != Type::CyqlopsDBPostgreSQL); // Not quiet implemented yet
  %/

bool Database::notificationsEnabled() const
{
  return d->notificationsEnabled;
}
 
void Database::setNotificationsEnabled(bool _v)
{
  if(d->notificationsEnabled == _v) return;
  d->notificationsEnabled = _v;
  emit(notificationsEnabledChanged());
  
  if(d->notificationsEnabled)
  {
    d->notificationsHandle = QObject::connect(d->database, &Cyqlops::DB::SQLite::Database::changed, [this](Cyqlops::DB::SQLite::Database::ChangeType _type, const QString& _tablename, qint64 _rowid) {/%
      for(const definitions::View* view : _database->views())
      {
        if(view->doesImplement(definitions::NOTIFICATIONS))
        {
          QString lc_name = uncapitalizeFirstLetter(view->name());
        %/
      if(false /%
        for(const definitions::View::Entry* entry : view->entries())
        {
          %/
          or _tablename == QStringLiteral("/%= queryGenerator()->tableName(entry->klass) %/")/%
        }
      %/)
      {
        emit(/%= lc_name %/Changed());
      }/%
        }
      }
      for(const definitions::Class* klass : _database->classes())
      {
        if(klass->doesImplement(definitions::NOTIFICATIONS))
        {
          QString lc_name = uncapitalizeFirstLetter(klass->name());
        %/
      if(_tablename == QStringLiteral("/%= queryGenerator()->tableName(klass) %/"))
      {
        switch(_type)
        {
          case Cyqlops::DB::SQLite::Database::ChangeType::Insert:
          {
            Sql::Query query(d->database);
            query.prepare("/%= queryGenerator()->generateSelectFromRowID(klass) %/");
            query.bindValue(":rowid", Sql::toVariant(_rowid));
            if(not query.exec())
            {
              qWarning() << query.lastQuery() << query.lastError();
              return;
            }
            if(not query.first())
            {
              qWarning() << "No ROWID found in database";
              return;
            }
            emit(/%= lc_name %/Inserted(/%= klass->name() %/(this, /%
              int key_index = 0;
              for(const definitions::Field* field : klass->fields())
              {
                if(field->options().testFlag(definitions::Field::Key))
                {
                  %/Sql::fromVariant</%= cppMemberType(field) %/>(query.value(/%= key_index++ %/)) ,/%
                }
              }
            %/ false)));
          }
            break;
          case Cyqlops::DB::SQLite::Database::ChangeType::Delete:
            emit(/%= lc_name %/Removed());
            break;
          case Cyqlops::DB::SQLite::Database::ChangeType::Update:
            break;
        }
        emit(/%= lc_name %/Changed());
        return;
      }/%
        }
      }
      %/
    });
  } else {
    QObject::disconnect(d->notificationsHandle);
  }
  
  emit(notificationsEnabled());
}

/%
}

if(d->type == Type::CyqlopsDBPostgreSQL and d->qtobject and has_update_notifications)
{
  %/

bool Database::updateNotificationsEnabled() const
{
  return d->updateNotificationsEnabled;
}

void Database::setUpdateNotificationsEnabled(bool _v)
{
  if(d->updateNotificationsEnabled == _v) return;
  d->updateNotificationsEnabled = _v;
  emit(updateNotificationsEnabledChanged());
  
  Sql::Query query(d->database);
  QString tablename;
  
  if(d->updateNotificationsEnabled)
  {/%
  for(const definitions::Class* klass : _database->classes())
  {
    if(klass->doesImplement(definitions::UPDATENOTIFICATION))
    {%/
      tablename = QString("/%= queryGenerator()->tableName(klass) %/");
      query.prepare("/%= queryGenerator()->createUpdatedTrigger(klass) %/");
      if(not query.exec())
      {
        qWarning() << query.lastQuery() << query.lastError();
      }
      d->/%= klass->name() %/UpdateNotificationsHandle = d->database->notificationsManager()->listen("/%= queryGenerator()->tableName(klass) %/updated", [this](const QByteArray&)
      {
        emit(/%= uncapitalizeFirstLetter(klass->name()) %/Updated());
      }
      );/%
    }
  }%/
  } else {/%
  for(const definitions::Class* klass : _database->classes())
  {
    if(klass->doesImplement(definitions::UPDATENOTIFICATION))
    {%/
    query.prepare("/%= queryGenerator()->deleteUpdatedTrigger(klass) %/");    
    if(not query.exec())
    {
      qWarning() << query.lastQuery() << query.lastError();
    }
    QObject::disconnect(d->/%= klass->name() %/UpdateNotificationsHandle);/%
    }
  }%/
  }
}
  /%
}

if(d->qtobject)
{%/
#include "moc_Database.cpp"/%
}
%/
