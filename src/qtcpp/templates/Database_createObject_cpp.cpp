query.prepare(QString("/%= queryGenerator()->generateCreateQuery(klass, include_auto_field_in_query) %/")/%= tableQueryArg(klass, QString()) %/);
/% foreach(const definitions::Field* field, klass->fields())
{
  if( (include_auto_field_in_query or not field->options().testFlag(definitions::Field::Auto))
    and not field->options().testFlag(definitions::Field::Dependent))
  {%/
    query.bindValue(":/%= field->name() %/", /%= argumentToBinding(field, "_" + field->name()) %/);
/%}
}%/
if(query.exec())
{/%
  if(queryGenerator()->createReturnValues())
  {%/
  query.first();
    /%
  }
  if(klass->classOfContainerDefinition())
  {%/
  object.d->database = d->database;
  object.d->container = *this;/%
  }
  else
  {%/
  object.d->database = database_ptr;/%
  }
  int auto_index = 0;
  foreach(const definitions::Field* field, klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      if(field->options().testFlag(definitions::Field::Auto))
      {
        if(queryGenerator()->createReturnValues())
        {%/
  object.d->/%= field->name() %/ = Sql::fromVariant</%= cppMemberType(field) %/>(query.value(/%= auto_index %/));/%
          ++auto_index;
        } else {
      %/
  object.d->/%= field->name() %/ = query.lastInsertId().toInt();/%
        }
      } else {
      %/
  object.d->/%= field->name() %/ = /%= valueToMember(field->sqlType(), "_" + field->name()) %/;/%
      }
    } else if(not field->options().testFlag(definitions::Field::Dependent)) {
      %/
  object.d->/%= field->name() %/ = /%= valueToMember(field->sqlType(), "_" + field->name()) %/;/%
    }
  }
  if(klass->isTree())
  { 
    %/
    query.prepare("/%= queryGenerator()->generateTreeCreateQuery(klass) %/");
    query.bindValue(":id1", object./%= klass->keys().first()->name() %/());
    query.bindValue(":id2", object./%= klass->keys().first()->name() %/());
    if(not query.exec())
    {
      qWarning() << "Failed to self insert in a tree: " << query.lastError() << query.lastQuery();
    }
    if(_parent.isValid())
    {
      /%= klass->name() %/(_parent).addChild(object);
    }
    /%
  }
  for(const definitions::Class* sklass : klass->classOfSubDefinitions())
  {
    // Need to create a table
    const definitions::Field* keyfield = klass->keys().first();
    %/
  {
    /%= cppReturnType(keyfield) %/ object_key = object./%= keyfield->name() %/();
    if(not query.exec(QString("/%= queryGenerator()->createTable(sklass) %/").arg(/%= valueToMember(keyfield->sqlType(), "object_key") %/)))
    {
      qWarning() << query.lastError();
    }
  }
    /%
  }
  %/
} else {
  qWarning() << query.lastQuery() << query.lastError();
  object = /%= klass->name() %/();
}
/%
  if(klass->hasDependents())
  {
  %/
object.record();/%
  }
%/
