/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "../../parc/generators/QtBaseCodeGenerator.h"

#include <QString>

class QTextStream;

namespace parc
{
  class AbstractQueryGenerator;
  namespace qtcpp
  {
    class CodeGenerator : public generators::QtBaseCodeGenerator
    {
    public:
      enum class Type
      {
        QtSQL, CyqlopsDBSQLite, CyqlopsDBPostgreSQL
      };
    public:
      CodeGenerator(AbstractQueryGenerator* _aqg, Type _type, bool _qtobject, bool _qml);
      virtual ~CodeGenerator();
      void generate(const definitions::Database* _database, const QString& _destination) override;
      bool canGenerate(const definitions::Database* _database, QString* _reason) override;
    protected:
      void generateDatabase(const definitions::Database* _database, const QString& _destination) override;
      void generateClass(const definitions::Database* _database, const definitions::Class* _klass, const QString& _destination) override;
      void generateView(const definitions::Database* _database, const definitions::View* _view, const QString& _destination) override;
      void generateJournal(const definitions::Database* _database, const QString& _destination) override;
    private:
      QString databaseOrContainer(const definitions::Class* _klass, const QString& _prefix = QString()) const;
      QString databaseOrContainer(const definitions::AbstractClass* _klass, const QString& _prefix = QString()) const;
      QString databaseOrContainerParameterName(const definitions::AbstractClass* _klass) const;
      QString databaseOrContainerParameterName(const definitions::Class* _klass) const;
      QString databaseOrContainerMemberName(const definitions::AbstractClass* _klass) const;
      QString databaseOrContainerMemberName(const definitions::Class* _klass) const;
      QString memberToValue(const definitions::Field* _field, const QString& _member_prefix, const QString& _database_variable) const;
      QString fieldToStringArg(const definitions::Field* _field, const QString& _value) const;
      QString sqlToStringArg(const SqlType* _sqlType, const QString& _value) const;
      QString escapeIfNeeded(const definitions::Field* field, const QString& fieldToStringArg) const;
      QString generateDatabaseArguments(const definitions::Database* _database, bool _header) const;
      void setKeysOnQuery(QTextStream& _stream, const QString& _query, const definitions::Class* _klass) const;
      void makeResultList(QTextStream& body_stream, const parc::definitions::AbstractClass* _klass, bool _only_keys) const;
    private:
      struct Private;
      Private* const d;
    };
  }
}
