/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "CodeGenerator.h"

#include <QDebug>
#include <QFile>

#include "../../parc/definitions/Class.h"
#include "../../parc/definitions/Database.h"
#include "../../parc/definitions/Enum.h"
#include "../../parc/definitions/Field.h"
#include "../../parc/definitions/Flags.h"
#include "../../parc/definitions/Forward.h"
#include "../../parc/definitions/Index.h"
#include "../../parc/definitions/Mapping.h"
#include "../../parc/definitions/Property.h"
#include "../../parc/definitions/Object.h"
#include "../../parc/definitions/Typedef.h"
#include "../../parc/definitions/View.h"

#include "../../parc/AbstractQueryGenerator.h"

using namespace parc;
using namespace parc::qtcpp;

struct CodeGenerator::Private
{
  Type type;
  bool qtobject;
  bool qml;
};

CodeGenerator::CodeGenerator(parc::AbstractQueryGenerator* _aqg, Type _type, bool _qtobject, bool _qml) : generators::QtBaseCodeGenerator(_aqg), d(new Private)
{
  d->type       = _type;
  d->qtobject   = _qtobject;
  d->qml        = _qml;
  Q_ASSERT(not d->qml or (d->qml and d->qtobject));
}

CodeGenerator::~CodeGenerator()
{
  delete d;
}

void CodeGenerator::generate(const definitions::Database* _database, const QString& _destination)
{
  generators::QtBaseCodeGenerator::generate(_database, _destination);

  {
    QFile forward_file(_destination + "/Forward.h");
    forward_file.open(QIODevice::WriteOnly);
    QTextStream stream(&forward_file);
#include "Forward_h.h"
  }
  if(d->qml)
  {
    QList<const definitions::AbstractClass*> klasses;
    for(const definitions::Class* klass : _database->classes())
    {
      klasses.append(klass);
      for(const definitions::Class* sklass : klass->classOfSubDefinitions())
      {
        klasses.append(sklass);
      }
    }
    for(const definitions::View* view : _database->views())
    {
      klasses.append(view);
    }
    {
      QFile header_file(_destination + "/Qml.h");
      header_file.open(QIODevice::WriteOnly);
      QTextStream stream(&header_file);

#include "Qml_h.h"
    }
    {
      QFile header_file(_destination + "/Qml_p.h");
      header_file.open(QIODevice::WriteOnly);
      QTextStream stream(&header_file);

#include "Qml_p_h.h"
    }
    {
      QFile header_file(_destination + "/Qml.cpp");
      header_file.open(QIODevice::WriteOnly);
      QTextStream stream(&header_file);

#include "Qml_cpp.h"
    }
  }
}

void CodeGenerator::generateDatabase(const definitions::Database* _database, const QString& _destination)
{
  // Generate sql header
  {
    QFile header_file(_destination + "/Sql.h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);

#include "Sql_h.h"
  }

  bool has_notifications = false;
  bool has_update_notifications = false;
  for(const definitions::Class* klass : _database->classes())
  {
    if(klass->doesImplement(definitions::NOTIFICATIONS))
    {
      has_notifications = true;
    }
    if(klass->doesImplement(definitions::UPDATENOTIFICATION))
    {
      has_update_notifications = true;
    }
  }

  // Generate header
  {
    QString databae_property_type;
    switch(d->type)
    {
      case Type::CyqlopsDBSQLite:
        databae_property_type = "Cyqlops::DB::SQLite::Database*";
        break;
      case Type::CyqlopsDBPostgreSQL:
        databae_property_type = "Cyqlops::DB::PostgreSQL::Database*";
        break;
      case Type::QtSQL:
        databae_property_type = "QSqlDatabase";
        break;
    }
    QFile header_file(_destination + "/Database.h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);

#include "Database_h.h"
  }

  // Generate source
  {
    QFile body_file(_destination + "/Database.cpp");
    body_file.open(QIODevice::WriteOnly);
    QTextStream stream(&body_file);
    
#include "Database_cpp.h"
  }
}

bool CodeGenerator::canGenerate(const definitions::Database* _database, QString* _reason)
{
  if(d->qtobject and (d->type == Type::CyqlopsDBSQLite or d->type == Type::CyqlopsDBPostgreSQL))
  {
    for(const definitions::Class* klass : _database->classes())
    {
      switch(d->type)
      {
        case Type::CyqlopsDBPostgreSQL:
        {
          if(klass->doesImplement(parc::definitions::NOTIFICATIONS))
          {
            if(_reason) *_reason = "Notifications are not supported by this combination of generator";
            return false;
          }
          break;
        }
        case Type::CyqlopsDBSQLite:
        {
          if(klass->doesImplement(parc::definitions::NOTIFICATIONS))
          {
            if(klass->keys().size() != 1)
            {
              if(_reason) *_reason = "Notifications are only supported for table with single keys";
              return false;
            }
          }
          if(klass->doesImplement(parc::definitions::UPDATENOTIFICATION))
          {
            if(_reason) *_reason = "Update notifications are not supported by this combination of generator";
            return false;
          }
        }
        break;
        case Type::QtSQL:
          qFatal("Internal error!");
      }
    }
  } else {
    for(const definitions::Class* klass : _database->classes())
    {
      if(klass->doesImplement(parc::definitions::NOTIFICATIONS))
      {
        if(_reason) *_reason = "Notifications are not supported by this combination of generator";
        return false;
      }
      if(klass->doesImplement(parc::definitions::UPDATENOTIFICATION))
      {
        if(_reason) *_reason = "Update notifications are not supported by this combination of generator";
        return false;
      }
    }
  }
  return true;
}

void CodeGenerator::generateView(const definitions::Database* _database, const definitions::View* _view, const QString& _destination)
{
  // Generate header
  {
    QFile header_file(_destination + "/" + _view->name() + ".h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);
#include "View_h.h"
  }
  {
    QFile body_file(_destination + "/" + _view->name() + ".cpp");
    body_file.open(QIODevice::WriteOnly);
    QTextStream stream(&body_file);
#include "View_cpp.h"
  }
}

void CodeGenerator::generateClass(const definitions::Database* _database, const definitions::Class* _klass, const QString& _destination)
{
  bool has_notifications = _klass->doesImplement(definitions::NOTIFICATIONS);
  bool has_update_notifications = false;
  for(const definitions::Class* sklass : _klass->classOfSubDefinitions())
  {
    if(sklass->doesImplement(definitions::NOTIFICATIONS))
    {
      has_notifications = true;
    }
    if(sklass->doesImplement(definitions::UPDATENOTIFICATION))
    {
      has_update_notifications = true;
    }
  }
  // Generate header
  {
    QFile header_file(_destination + "/" + _klass->name() + ".h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);
#include "Class_h.h"
  }
  // Generate private header
  {
    QFile header_file(_destination + "/" + _klass->name() + "_p.h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);
#include "Class_p_h.h"
  }
    
  {
    QFile body_file(_destination + "/" + _klass->name() + ".cpp");
    body_file.open(QIODevice::WriteOnly);
    QTextStream stream(&body_file);
    
#include "Class_cpp.h"
    
  }

}


void CodeGenerator::generateJournal(const definitions::Database* _database, const QString& _destination)
{
  // Generate header
  {
    QFile header_file(_destination + "/Journal.h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);
#include "Journal_h.h"
  }
    
  {
    QFile body_file(_destination + "/Journal.cpp");
    body_file.open(QIODevice::WriteOnly);
    QTextStream stream(&body_file);
#include "Journal_cpp.h"
  }

  {
    QFile header_file(_destination + "/JournalEntry.h");
    header_file.open(QIODevice::WriteOnly);
    QTextStream stream(&header_file);
#include "JournalEntry_h.h"
  }
    
  {
    QFile body_file(_destination + "/JournalEntry.cpp");
    body_file.open(QIODevice::WriteOnly);
    QTextStream stream(&body_file);
#include "JournalEntry_cpp.h"
  }

  {
    QFile body_file(_destination + "/JournaledFields.h");
    body_file.open(QIODevice::WriteOnly);
    QTextStream stream(&body_file);
#include "JournaledFields_h.h"
  }

}

QString CodeGenerator::databaseOrContainer(const definitions::Class* _klass, const QString& _prefix) const
{
  if(_klass->classOfContainerDefinition())
  {
    return "const " + _prefix + _klass->classOfContainerDefinition()->name() + "& _container";
  } else {
    return "Database* _database";
  }
}

QString CodeGenerator::databaseOrContainer(const definitions::AbstractClass* _klass, const QString& _prefix) const
{
  auto klass = dynamic_cast<const definitions::Class*>(_klass);
  if(klass)
  {
    return databaseOrContainer(klass, _prefix);
  } else {
    return "Database* _database";
  }
}

QString CodeGenerator::databaseOrContainerParameterName(const definitions::AbstractClass* _klass) const
{
  const definitions::Class* klass = dynamic_cast<const definitions::Class*>(_klass);
  if(klass)
  {
    return databaseOrContainerParameterName(klass);
  } else {
    return "_database";
  }
}

QString CodeGenerator::databaseOrContainerParameterName(const definitions::Class* _klass) const
{
  if(_klass->classOfContainerDefinition())
  {
    return "_container";
  } else {
    return "_database";
  }
}

QString CodeGenerator::databaseOrContainerMemberName(const definitions::AbstractClass* _klass) const
{
  const definitions::Class* klass = dynamic_cast<const definitions::Class*>(_klass);
  if(klass)
  {
    return databaseOrContainerMemberName(klass);
  } else {
    return "database";
  }
}

QString CodeGenerator::databaseOrContainerMemberName(const definitions::Class* _klass) const
{
  if(_klass->classOfContainerDefinition())
  {
    return "container";
  } else {
    return "database";
  }
}

QString CodeGenerator::memberToValue(const definitions::Field* _field, const QString& _member_prefix, const QString& _database_variable) const
{
  if(_field->sqlType()->type() == SqlType::REFERENCE)
  {
    const definitions::Class* klass = static_cast<const definitions::Class*>(_field->sqlType());
    return QString("%1::%2(%3, %4%5)").arg(klass->database()->namespaces().join("::")).arg(klass->name()).arg(_database_variable).arg(_member_prefix).arg(_field->name());
  } else {
    return QString("%1%2").arg(_member_prefix).arg(_field->name());
  }
}

QString CodeGenerator::sqlToStringArg(const SqlType* _sqlType, const QString& _value) const
{
  switch(_sqlType->type())
  {
  case SqlType::DATETIME:
    return "QString(\"" + queryGenerator()->generateDateTimeToQuery() + "\").arg(_value.toTime_t())";
  case SqlType::REFERENCE:
    return _value + ".id()";
  default:
    return _value;
  }
}

QString CodeGenerator::fieldToStringArg(const definitions::Field* _field, const QString& _value) const
{
  if(_field->typedefinition())
  {
    return typeDefTosqlType(_field->typedefinition()) + "(" + sqlToStringArg(_field->typedefinition()->sqltype(), _value) + ")";
  }
  return sqlToStringArg(_field->sqlType(), _value);
}

QString CodeGenerator::escapeIfNeeded(const definitions::Field* field, const QString& fieldToStringArg) const
{
  switch(field->sqlType()->type())
  {
  case parc::SqlType::STRING:
    return "Sql::escapeString(" + fieldToStringArg + ")";
  case parc::SqlType::DATETIME:
  case parc::SqlType::INTEGER:
  case parc::SqlType::REFERENCE:
  default:
    return fieldToStringArg;
  }
}

QString CodeGenerator::generateDatabaseArguments(const definitions::Database* _database, bool _header) const
{
  QString ret;
  foreach(const parc::definitions::Property* property, _database->properties())
  {
    if(property->isConstant())
    {
      ret += ", " + cppArgType(property) + " _" + property->name();
      if(_header and property->value().size() > 0)
      {
        ret += " = " + cppMemberType(property) + "(";
        for(int i = 0; i < property->value().size(); ++i)
        {
          if(i!= 0) ret += ", ";
          ret += property->value().at(i);
        }
        ret += ")";
      }
    }
  }
  return ret;  
}

void CodeGenerator::setKeysOnQuery(QTextStream& _stream, const QString& _query, const definitions::Class* _klass) const
{
  foreach(const definitions::Field* field, _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      _stream << _query << ".bindValue(\":" << field->name() << "\", " << memberToBinding(field, "d->" + field->name()) << ");\n";
    }
  }
}

void CodeGenerator::makeResultList(QTextStream& body_stream, const definitions::AbstractClass* _klass, bool _only_keys) const
{
  const definitions::View* view = dynamic_cast<const definitions::View*>(_klass);
  body_stream << "  QList< "<< _klass->name() << "> results;\n"
                 "  if(query.exec())\n"
                 "  {\n"
                 "    while (query.next())\n"
                 "    {\n"
                 "      results.push_back(" << _klass->name() << "( d->" << databaseOrContainerMemberName(_klass);
  {
    int idx = 0;
    if(view)
    {
      body_stream << ", " << _klass->name() << "::EntryClass(query.value(0).toInt())";
      ++idx;
    }
    for(const definitions::Field* field : _klass->fields())
    {
      if(_only_keys == false or field->options().testFlag(definitions::Field::Key))
      {
        if(field->sqlType()->type() == SqlType::REFERENCE and _only_keys == true)
        {
          const definitions::Class* klass = static_cast<const definitions::Class*>(field->sqlType());
          body_stream << ", " << klass->name() << "(d->database, Sql::fromVariant<" << cppMemberType(field) << ">(query.value(" << idx << ")))";
        } else {
          body_stream << ", " << "Sql::fromVariant<" << cppMemberType(field) << ">(query.value(" << idx <<"))";
        }
        ++idx;
      }
    }
  }
  body_stream << " ) );\n"
                 "    }\n"
                 "  } else {\n"
                 "    qWarning() << query.lastQuery() << query.lastError();\n"
                 "  }\n"
                 "  return results;\n";
}
