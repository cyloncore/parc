/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QString>

class QVariant;
namespace parc
{
  struct Token
  {
    enum Type
    {
    // Not really token
      INVALID_IDENTIFIER = -5,
      UNFINISHED_STRING = -4,
      UNFINISHED_COMMENT = -3,
      END_OF_FILE = -2,
      UNKNOWN = - 1,
    // Special characters
      SEMI = 0, ///< ;
      COLON, ///< :
      COLONCOLON, ///< ::
      INFERIOR, ///< <
      SUPPERIOR, ///< >
      EQUAL, ///< =
      STARTBRACE, ///< {
      ENDBRACE, ///< }
      STARTBRACKET, ///< (
      ENDBRACKET, ///< )
      COMMA, ///< ,
    // Constants
      CONSTANT_EXPRESSION,
      STRING_CONSTANT,
      IDENTIFIER,
    // Keywords
      CLASS,
      KEY,
      BOOLEAN,
      INT,
      FLOAT,
      DOUBLE,
      BLOB,
      OF,
      TREE,
      STRING,
      DATETIME,
      PARENT,
      REQUIRED,
      MAP,
      WITH,
      AS,
      NAMESPACE,
      TYPEDEF,
      MANY,
      CONST,
      AUTO,
      UNIQUE,
      PUBLIC,
      PRIVATE,
      PROTECTED,
      FORWARD,
      JOURNALED,
      IMPLEMENTS,
      DATABASE,
      INDEXED,
      DEFINE,
      ENUM,
      DEPENDENT,
      AND,
      FLAGS,
      VIEW,
      INCLUDE
    };
    /// type of the token
    Type type;
    /// line of the token
    int line;
    /// Column of the token
    int column;
    /// String or identifier name
    QString string;
    
    Token();
    /**
      * Creates a token of the given type
      */
    Token(Type _type, int _line, int _column);
    /**
      * Creates an identifier or a string constant
      */
    Token(Type _type, const QString& _string, int _line, int _column);
    static const char* typeToString(parc::Token::Type _type );
    bool isSqlType() const;
  };
}
