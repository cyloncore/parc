
CONFIG += object_parallel_to_source c++11 console
QT -= gui

macx {
  CONFIG -= app_bundle
}

SOURCES += main.cpp \
  Lexer.cpp \
  Parser.cpp \
  Token.cpp

HEADERS += \
  Lexer.h \
  Parser.h \
  Token.h

# Generators

SOURCES += \
  qtcpp/CodeGenerator.cpp \
  sql/QueryGenerator.cpp \
  postgres/QueryGenerator.cpp \
  sqlite/QueryGenerator.cpp


HEADERS += \
  qtcpp/CodeGenerator.h \
  sql/QueryGenerator.h \
  postgres/QueryGenerator.h \
  sqlite/QueryGenerator.h

macx {
LIBS += $${top_parc_builddir}/parc/libparc.dylib
}
else {
  unix {
    LIBS += $${top_parc_builddir}/parc/libparc.so
  }
}
# QMAKE_LFLAGS_RPATH +=$${top_parc_builddir}/parc
# QMAKE_LFLAGS    += "-W1,-rpath,\'$${top_parc_builddir}/parc\'"

# Call to ptc

PTC    = $$system(which ptc)

isEmpty(PTC)
{
  PTC    = $${top_parc_builddir}/../ptc/src/ptc
}

PTC_SOURCES = $${PWD}/qtcpp/templates/Class_cpp.cpp $${PWD}/qtcpp/templates/Class_h.h $${PWD}/qtcpp/templates/Class_p_h.h \
              $${PWD}/qtcpp/templates/Database_cpp.cpp $${PWD}/qtcpp/templates/Database_createObject_cpp.cpp \
              $${PWD}/qtcpp/templates/Database_h.h \
              $${PWD}/qtcpp/templates/Sql_h.h \
              $${PWD}/qtcpp/templates/Qml_h.h $${PWD}/qtcpp/templates/Qml_p_h.h $${PWD}/qtcpp/templates/Qml_cpp.h \
              $${PWD}/qtcpp/templates/Journal_cpp.cpp $${PWD}/qtcpp/templates/Journal_h.h \
              $${PWD}/qtcpp/templates/JournalEntry_cpp.cpp $${PWD}/qtcpp/templates/JournalEntry_h.h \
              $${PWD}/qtcpp/templates/JournaledFields_h.h

genqtcpptemplates.input     = PTC_SOURCES
genqtcpptemplates.output    = ${QMAKE_FILE_BASE}.h
genqtcpptemplates.commands  = $${PTC} qtcpp ${QMAKE_FILE_IN} ${QMAKE_FILE_BASE}.h
genqtcpptemplates.depends   = $${PTC}
genqtcpptemplates.variable_out = HEADERS

QMAKE_EXTRA_COMPILERS += genqtcpptemplates
PRE_TARGETDEPS += Database_createObject_cpp.h

OTHER_FILES += $${PTC_SOURCES}

unix:!mac{
  QMAKE_LFLAGS += -Wl,--rpath=$${target.path}/lib,--rpath=$${top_parc_builddir}/parc
}
mac{
QMAKE_RPATHDIR+=$${target.path}/lib
QMAKE_RPATHDIR+=$${top_parc_builddir}/parc
QMAKE_POST_LINK += install_name_tool -change libparc.1.dylib @rpath/libparc.1.dylib $$TARGET
}

target.path = "$${target.path}/bin"
INSTALLS += target
