#include "QueryGenerator.h"

#include <QString>
#include <QStringList>

#include "../utils_p.h"

#include "../../parc/definitions/Mapping.h"
#include "../../parc/definitions/Class.h"
#include "../../parc/definitions/Field.h"
#include "../../parc/definitions/Unique.h"

using namespace parc::postgres;

QueryGenerator::QueryGenerator() : sql::QueryGenerator(Feature::Copy)
{
  
}

QueryGenerator::~QueryGenerator()
{
  
}

QStringList QueryGenerator::initialisationQueries() const
{
  return QStringList() << "CREATE FUNCTION parc_notify_change() RETURNS trigger AS $$\n"
                          "DECLARE\n"
                          "  channel text;\n"
                          "BEGIN\n"
                          "  channel   := TG_ARGV[0];\n"
                          "  PERFORM pg_notify(channel, '');\n"
                          "  RETURN NULL;\n"
                          "END;\n"
                          "$$ LANGUAGE 'plpgsql';";

}

QString QueryGenerator::checkTableExistence(const parc::definitions::Class* _class) const
{
  return "SELECT EXISTS( SELECT * FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '" + tableName(_class) + "');";
}


QString QueryGenerator::sqltype(const parc::SqlType* sqlType) const
{
  switch(sqlType->type())
  {
    case SqlType::BOOLEAN:
      return "BOOLEAN";
    case parc::SqlType::INTEGER:
    case parc::SqlType::ENUM:
    case parc::SqlType::FLAG:
      return "INTEGER";
    case parc::SqlType::FLOAT32:
      return "FLOAT4";
    case parc::SqlType::FLOAT64:
      return "FLOAT8";
    case parc::SqlType::DATETIME:
      return "TIMESTAMP";
    case parc::SqlType::STRING:
      return "TEXT";
    case parc::SqlType::REFERENCE:
      return "INTEGER";
    case parc::SqlType::BLOB:
      return "BYTEA";
    case parc::SqlType::CUSTOM:
      return sqlType->sqlType();
    case parc::SqlType::SQLQUERY:
      qFatal("sqltype: SQLQUERY have no type!");
  }
  qFatal("sqltype: unimplemented");
}

QString QueryGenerator::tableName(const definitions::Class* _class) const
{
  return sql::QueryGenerator::tableName(_class).toLower();
}

QString QueryGenerator::fieldName(const parc::definitions::Field* _field) const
{
  return sql::QueryGenerator::fieldName(_field).toLower();
}

QString QueryGenerator::mappingFieldName(const definitions::Class* _class) const
{
  return sql::QueryGenerator::mappingFieldName(_class).toLower();
}

QString QueryGenerator::createInfoTable() const
{
    return "CREATE TABLE info (id SERIAL, name TEXT UNIQUE, value TEXT)";
}

QString QueryGenerator::createTable(const parc::definitions::Class* _class) const
{
  QString q("CREATE TABLE " + quote(tableName(_class)) + "(");
  bool first = true;
  
  QStringList primary_keys;
  
  QString constraints;
  
  foreach(const parc::definitions::Field* field, _class->fields())
  {
    if(field->sqlType()->type() != parc::SqlType::SQLQUERY)
    {
      if(not first)
      {
        q += ",";
      } else {
        first = false;
      }
      QString sql_field_name = quote(fieldName(field));
      if(field->options().testFlag(parc::definitions::Field::Auto))
      {
        q += sql_field_name + " serial";
      } else {
        q += sql_field_name + " " + sqltype(field->sqlType());
      }
      if(field->options().testFlag(parc::definitions::Field::Key))
      {
        primary_keys << sql_field_name;
      }
      if(field->options().testFlag(parc::definitions::Field::Unique))
      {
        q += " UNIQUE";
      }
      if(field->options().testFlag(parc::definitions::Field::Parent))
      {
        const parc::definitions::Class* pkl = static_cast<const parc::definitions::Class*>(field->sqlType());
        constraints += ", FOREIGN KEY (" + sql_field_name + ") REFERENCES " + quote(tableName(pkl)) + "(" + quote(fieldName(pkl->keys().first())) + ")";
      }
    }
  }
  
  for(const parc::definitions::Unique* unique : _class->uniques())
  {
    q += ", UNIQUE (";
    bool first = true;
    for(const parc::definitions::Field* field : unique->fields())
    {
      if(first)
      {
        first = false;
      } else {
        q += ",";
      }
      q += quote(fieldName(field));
    }
    
    q += ")";
  }

  if(not primary_keys.isEmpty())
  {
    q += ", PRIMARY KEY (" + primary_keys.join(",") + ")";
  }
  
  return q + constraints + ")";
//   qFatal("fails");
}

QString QueryGenerator::createUpdatedTrigger(const definitions::Class* _class) const
{
  QString tablename = tableName(_class);
  return "CREATE TRIGGER " + tablename + "_notify_update AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON " + tablename + " FOR EACH STATEMENT EXECUTE PROCEDURE parc_notify_change('" + tablename + "updated');";
}

QString QueryGenerator::deleteUpdatedTrigger(const definitions::Class* _class) const
{
  QString tablename = tableName(_class);
  return "DROP TRIGGER " + tablename + "_notify_update;";
}

QString QueryGenerator::generateCreateQuery(const definitions::Class* _klass, bool _include_auto_field_in_query) const
{
  QString q = sql::QueryGenerator::generateCreateQuery(_klass, _include_auto_field_in_query);
  
  bool first = true;
  for(const definitions::Field* field : _klass->fields())
  {
    if(field->options().testFlag(definitions::Field::Auto))
    {
      if(first)
      {
        q += " RETURNING ";
        first = false;
      }
      else
      {
        q += ", ";
      }
      q += fieldName(field);
    }
  }
  
  return q;
}

bool QueryGenerator::createReturnValues() const
{
  return true;
}

QString QueryGenerator::generateCopyQuery(const parc::definitions::Class* _klass, bool _include_auto_field_in_query) const
{
  QString q = "COPY " + quote(tableName(_klass)) + "(";
  bool first = true;
  for(const parc::definitions::Field* field : _klass->fields())
  {
    if( (_include_auto_field_in_query or not field->options().testFlag(definitions::Field::Auto))
        and not field->options().testFlag(definitions::Field::Dependent) )
    {
      if(not first)
      {
        q += ",";
      } else {
        first = false;
      }
      q += quote(fieldName(field));
    }
  }
  
  return q + ") FROM STDIN WITH (FORMAT binary)";
}

QString QueryGenerator::generateTrue() const
{
  return "true";
}

QString QueryGenerator::generateLockQuery() const
{
  return "LOCK %1 IN SHARE ROW EXCLUSIVE MODE";
}

QString QueryGenerator::upsert(const QString& _body, UpsertType _upsertType) const
{
  switch(_upsertType)
  {
    case UpsertType::Mapping:
      return "INSERT INTO " + _body + " ON CONFLICT DO NOTHING";
    case UpsertType::Tree:
      qFatal("unimplemented");
      return "INSERT INTO " + _body + " ON CONFLICT DO UPDATE";
    case UpsertType::Version:
      return "INSERT INTO " + _body + " ON CONFLICT (name) DO UPDATE SET value=:version";
  }
  qFatal("unsupported upsert");
}
