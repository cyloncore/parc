#include "../sql/QueryGenerator.h"

namespace parc
{
  namespace postgres
  {
    class QueryGenerator : public sql::QueryGenerator
    {
    public:
      QueryGenerator();
      virtual ~QueryGenerator();
    public:
      QStringList initialisationQueries() const override;
      QString checkTableExistence(const definitions::Class* _class) const override;
      QString createInfoTable() const override;
      QString createTable(const definitions::Class* _class) const override;
      QString createUpdatedTrigger(const definitions::Class * _class) const override;
      QString deleteUpdatedTrigger(const definitions::Class * _class) const override;
      QString generateCreateQuery(const definitions::Class * _klass, bool _include_auto_field_in_query) const override;
      bool createReturnValues() const override;
      QString generateCopyQuery(const definitions::Class* _klass, bool _include_auto_field_in_query) const override;
      QString generateTrue() const override;
      QString generateLockQuery() const override;
    protected:
      QString sqltype(const parc::SqlType* sqlType) const override;
      QString tableName(const definitions::Class* _class) const override;
      QString fieldName(const definitions::Field* _field) const override;
      QString mappingFieldName(const definitions::Class* _class) const override;
      QString upsert(const QString& _body, UpsertType _upsertType) const override;
      
    };
  }
}
