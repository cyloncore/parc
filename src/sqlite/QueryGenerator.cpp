#include "QueryGenerator.h"


#include <QString>

#include "../utils_p.h"

#include "../../parc/definitions/Mapping.h"
#include "../../parc/definitions/Class.h"
#include "../../parc/definitions/Field.h"

using namespace parc::sqlite;

QueryGenerator::QueryGenerator() : sql::QueryGenerator(Features())
{
  
}

QueryGenerator::~QueryGenerator()
{
  
}

QStringList QueryGenerator::initialisationQueries() const
{
  return QStringList() << "PRAGMA foreign_keys=on";
}

QString QueryGenerator::checkTableExistence(const parc::definitions::Class* _class) const
{
  return "SELECT COUNT(name) FROM sqlite_master WHERE type='table' AND name='" + tableName(_class) + "'";
}

QString QueryGenerator::createInfoTable() const
{
    return "CREATE TABLE info (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE, value TEXT)";
}


QString QueryGenerator::sqltype(const parc::SqlType* sqlType) const
{
  switch(sqlType->type())
  {
    case parc::SqlType::ENUM:
    case parc::SqlType::INTEGER:
    case parc::SqlType::FLAG:
      return "INTEGER";
    case parc::SqlType::FLOAT32:
      return "REAL";
    case parc::SqlType::FLOAT64:
      return "REAL";
    case parc::SqlType::DATETIME:
      return "INTEGER";
    case parc::SqlType::STRING:
      return "TEXT";
    case parc::SqlType::REFERENCE:
      return "INTEGER";
    case parc::SqlType::BLOB:
      return "BLOB";
    case parc::SqlType::BOOLEAN:
      return "BOOLEAN";
    case parc::SqlType::CUSTOM:
      return sqlType->sqlType();
    case parc::SqlType::SQLQUERY:
      qFatal("sqltype: SQLQUERY have no type!");
  }
  qFatal("sqltype: unimplemented");
}


QString QueryGenerator::createTable(const parc::definitions::Class* _class) const
{
  Q_ASSERT(_class->uniques().isEmpty());
  QString q("CREATE TABLE " + quote(tableName(_class)) + "(");
  QString foreign_keys;
  bool first = true;
  foreach(const parc::definitions::Field* field, _class->fields())
  {
    if(field->sqlType()->type() != parc::SqlType::SQLQUERY)
    {
      if(not first)
      {
        q += ",";
      } else {
        first = false;
      }
      q += quote(fieldName(field)) + " " + sqltype(field->sqlType());
      if(field->options().testFlag(parc::definitions::Field::Key))
      {
        q += " PRIMARY KEY";
      }
      if(field->options().testFlag(parc::definitions::Field::Unique))
      {
        q += " UNIQUE";
      }
      if(field->options().testFlag(parc::definitions::Field::Parent))
      {
        const parc::definitions::Class* pkl = static_cast<const parc::definitions::Class*>(field->sqlType());
        foreign_keys += ", FOREIGN KEY (" + quote(fieldName(field)) + ") REFERENCES " + quote(tableName(pkl)) + "(" + quote(fieldName(pkl->keys().first())) + ")";
      }
      if(field->options().testFlag(parc::definitions::Field::Auto))
      {
        q += " AUTOINCREMENT";
      }
    }
  }
  
  return q + foreign_keys + ")";
//   qFatal("fails");
}

QString QueryGenerator::createUpdatedTrigger(const definitions::Class* _class) const
{
  Q_UNUSED(_class)
  qFatal("Internal error: unsupported");
}

QString QueryGenerator::deleteUpdatedTrigger(const definitions::Class* _class) const
{
  Q_UNUSED(_class)
  qFatal("Internal error: unsupported");
}

QString QueryGenerator::generateCopyQuery(const parc::definitions::Class* _klass, bool _include_auto_field_in_query) const
{
  Q_UNUSED(_klass)
  Q_UNUSED(_include_auto_field_in_query)
  qFatal("No copy query in SQLITE");
}

QString QueryGenerator::upsert(const QString& _body, UpsertType _upsertType) const
{
  Q_UNUSED(_upsertType)
  return "INSERT OR REPLACE INTO " + _body;
}

QString QueryGenerator::generateTrue() const
{
  return "1";
}

QString QueryGenerator::generateLockQuery() const
{
  return "";
}

QString QueryGenerator::generateGetRowID(const definitions::Class* _class) const
{
  QString q = "SELECT rowid FROM " + quote(tableName(_class)) + " WHERE (";
  bool first = true;
  for(const definitions::Field* field : _class->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      if(first)
      {
        first = false;
      } else {
        q += " AND ";
      }
      q += quote(fieldName(field)) + "=:" + field->name();
    }
  }
  
  return q + ")";  
}

QString QueryGenerator::generateSelectFromRowID(const definitions::Class* _class) const
{
  QStringList fieldnames;
  for(const definitions::Field* field : _class->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      fieldnames.append(fieldName(field));
    }
  }
  return "SELECT " + fieldnames.join(", ") + " FROM " + quote(tableName(_class)) + " WHERE rowid = :rowid";
}
