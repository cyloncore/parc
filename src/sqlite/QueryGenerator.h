#include "../sql/QueryGenerator.h"

namespace parc
{
  namespace sqlite
  {
    class QueryGenerator : public sql::QueryGenerator
    {
    public:
      QueryGenerator();
      virtual ~QueryGenerator();
      QStringList initialisationQueries() const override;
      QString checkTableExistence(const definitions::Class* _class) const override;
      QString createInfoTable() const override;
      QString createTable(const definitions::Class* _class) const override;
      QString createUpdatedTrigger(const definitions::Class * _class) const override;
      QString deleteUpdatedTrigger(const definitions::Class * _class) const override;
      QString generateCopyQuery(const definitions::Class* _klass, bool _include_auto_field_in_query) const override;
      QString generateTrue() const override;
      QString generateLockQuery() const override;
      QString generateGetRowID(const definitions::Class* _klass) const override;
      QString generateSelectFromRowID(const definitions::Class* _klass) const override;
    protected:
      QString sqltype(const parc::SqlType* sqlType) const override;
      QString upsert(const QString& _body, UpsertType _upsertType) const override;
    };
  }
}
