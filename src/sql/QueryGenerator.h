/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Query Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _SQL_QUERYGENERATOR_H_
#define _SQL_QUERYGENERATOR_H_

#include "../../parc/AbstractQueryGenerator.h"

namespace parc
{
  class SqlType;
  namespace sql
  {
    class QueryGenerator : public AbstractQueryGenerator
    {
    public:
      QueryGenerator(Features _features);
      ~QueryGenerator();
    public:
      QString versionQuery() const override;
      QString setVersionQuery() const override;
      QString createIndex(const definitions::Class* _class, const QList<const definitions::Field*>& _field) const override;
      QString createMappingTable(const definitions::Mapping* _mapping) const override;
      QString createMappingJournalTable(const definitions::Mapping* _mapping) const override;
      QString createTable(const definitions::Class* _class) const override;
      QString dropTable(const definitions::Class* _class) const override;
      QString createView(const definitions::View* _view) const override;
      QString createJournalTable(const definitions::Class* _class) const override;
      QString createTreeTable(const definitions::Class* klass) const override;
      QString createJournalTreeTable(const definitions::Class* klass) const override;
      QString generateRemoveAll(const definitions::Class * _class) const override;
      QString generateSelect(const definitions::AbstractClass* _class) const override;
      QString generateSelectCount(const definitions::AbstractClass* _class) const override;
      QString generateSelectOrderBy(const definitions::AbstractClass* _class) const override;
      QString generateLimit() const override;
      QString generateOffsetLimit() const override;
      QString generateOffset() const override;
      QString generateRandomOrder() const override;
      QString generateOrderAsc(const definitions::Field* _field) const override;
      QString generateOrderDsc(const definitions::Field* _field) const override;
      QString generateRefreshQuery(const definitions::Class* _class) const override;
      QString generateRecordQuery(const definitions::Class* _class) const override;
      QString generateDeleteQuery(const definitions::Class* _class) const override;
      QString generateSelectJournalQuery(const definitions::Class* _class) const override;
      QString generateRecordJournalUpdateQuery(const definitions::Class* _class) const override;
      QString generateRecordJournalInsertQuery(const definitions::Class* _class) const override;
      QString generateRecordJournalDeleteQuery(const definitions::Class* _class) const override;
      QString generateExistQuery(const definitions::Class* _class) const override;
      QString generateWhereLike() const override;
      QString generateWhereNEq() const override;
      QString generateWhereGTE() const override;
      QString generateWhereLTE() const override;
      QString generateWhereLT() const override;
      QString generateWhereGT() const override;
      QString generateWhereEq() const override;
      QString generateDateTimeToQuery() const override;
      QString generateCreateQuery(const definitions::Class* _klass, bool _include_auto_field_in_query) const override;
      bool createReturnValues() const override;
      QString generateTreeCreateQuery(const definitions::Class* _klass) const override;
      QString generateTreeChildrenQuery(const definitions::Class* _klass) const override;
      QString generateTreeParentsQuery(const definitions::Class* _klass) const override;
      QString generateTreeAddChildQuery(const definitions::Class* _klass) const override;
      QString generateTreeAddChildcJournalQuery(const definitions::Class* _klass) const override;
      QString generateTreeIsChildQuery(const definitions::Class* _klass) const override;
      QString generateGetMappingsQuery(const definitions::Mapping* _mapping, const definitions::Class* _class) const override;
      QString generateRemoveMappingQuery(const definitions::Mapping* _mapping, const definitions::Class* _class) const override;
      QString generateAddMappingQuery(const definitions::Mapping* _mapping, const definitions::Class* _class) const override;
      QString generateHasMappingQuery(const definitions::Mapping * _mapping, const definitions::Class * _class) const override;
      QString generateMappingUpdateJournalQuery(const definitions::Mapping* _mapping) const override;
      QString tableName(const definitions::Class* _class) const override;
      QString viewName(const definitions::View* _view) const override;
      QString fieldName(const definitions::Field* _field) const override;
      QString generateGetRowID(const definitions::Class* _klass) const override;
      QString generateSelectFromRowID(const definitions::Class* _klass) const override;
    protected:
      QString tableOrViewName(const definitions::AbstractClass* _class) const;
      inline QString quote(const QString& _name) const { return "\\\"" + _name + "\\\""; }
      virtual QString mappingFieldName(const definitions::Class* _class) const;
      virtual QString rawTableName(const definitions::Class* _class) const;
      virtual QString rawViewName(const definitions::View* _view) const;
      QString tableJournalName(const definitions::Class* _class) const;
      QString mappingTableName(const parc::definitions::Mapping* _mapping) const;
      QString mappingJournalTableName(const parc::definitions::Mapping* _mapping) const;
      QString treeTableName(const definitions::Class* _class) const;
      QString treeJournalTableName(const definitions::Class* _class) const;
      virtual QString sqltype(const parc::SqlType* sqlType) const = 0;
      enum class UpsertType {
        Version, Mapping, Tree
      };
      virtual QString upsert(const QString& _body, UpsertType _upsertType) const = 0;
    private:
      struct Private;
      Private* const d;
    };
  }
}

#endif
