/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Query Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "QueryGenerator.h"

#include <QString>

#include "../utils_p.h"

#include "../../parc/definitions/Mapping.h"
#include "../../parc/definitions/Class.h"
#include "../../parc/definitions/View.h"
#include "../../parc/definitions/Field.h"


using namespace parc::sql;

struct QueryGenerator::Private
{
  
};

QueryGenerator::QueryGenerator(parc::AbstractQueryGenerator::Features _features) : AbstractQueryGenerator(_features), d(new Private)
{

}

QueryGenerator::~QueryGenerator()
{
  delete d;
}

QString QueryGenerator::versionQuery() const
{
  return "SELECT value FROM info WHERE name = 'version'";
}

QString QueryGenerator::setVersionQuery() const
{
  return upsert("info(name, value) VALUES ('version', :version)", UpsertType::Version);
}

QString QueryGenerator::createIndex(const parc::definitions::Class* _class, const QList<const parc::definitions::Field*>& _field) const
{
  QString name;
  QString fieldlist;
  foreach(const parc::definitions::Field* field, _field)
  {
    name += field->name() + "_";
    if(not fieldlist.isEmpty())
    {
      fieldlist += ",";
    }
    fieldlist += quote(fieldName(field));
  }
  return "CREATE INDEX " + rawTableName(_class) + "_" + name + "index ON " + quote(tableName(_class)) + "(" + fieldlist + ")";
}

QString QueryGenerator::createMappingTable(const parc::definitions::Mapping* _mapping) const
{
  QString a_f = quote(mappingFieldName(_mapping->a()));
  QString b_f = quote(mappingFieldName(_mapping->b()));
  return "CREATE TABLE " + quote(mappingTableName(_mapping)) + "(" + a_f + " INTEGER, " + b_f + " INTEGER, PRIMARY KEY("
         + a_f + ", " + b_f + "), FOREIGN KEY (" + a_f + ") REFERENCES " + quote(tableName(_mapping->a())) +
         "(" + quote(fieldName(_mapping->a()->keys().first())) + "), FOREIGN KEY (" + b_f + ") REFERENCES " + quote(tableName(_mapping->b())) + "(" + quote(fieldName(_mapping->b()->keys().first())) + ") )";
}

QString QueryGenerator::createMappingJournalTable(const parc::definitions::Mapping* _mapping) const
{
  QString a_f = quote(mappingFieldName(_mapping->a()));
  QString b_f = quote(mappingFieldName(_mapping->b()));
  return "CREATE TABLE " + quote(mappingJournalTableName(_mapping)) + "(__journal_id INTEGER PRIMARY KEY AUTOINCREMENT, __journal_entry_type INTEGER, " + a_f + " INTEGER, " + b_f + " INTEGER)";
}

QString QueryGenerator::createTable(const parc::definitions::Class* _class) const
{
  QString q("CREATE TABLE " + quote(tableName(_class)) + "(");
  QString foreign_keys;
  bool first = true;
  foreach(const parc::definitions::Field* field, _class->fields())
  {
    if(not first)
    {
      q += ",";
    } else {
      first = false;
    }
    q += quote(fieldName(field)) + " " + sqltype(field->sqlType());
    if(field->options().testFlag(parc::definitions::Field::Key))
    {
      q += " PRIMARY KEY";
    }
    if(field->options().testFlag(parc::definitions::Field::Unique))
    {
      q += " UNIQUE";
    }
    if(field->options().testFlag(parc::definitions::Field::Parent))
    {
      const parc::definitions::Class* pkl = static_cast<const parc::definitions::Class*>(field->sqlType());
      foreign_keys += ", FOREIGN KEY (" + quote(fieldName(field)) + ") REFERENCES " + quote(tableName(pkl)) + "(" + quote(fieldName(pkl->keys().first())) + ")";
    }
    if(field->options().testFlag(parc::definitions::Field::Auto))
    {
      if(not field->options().testFlag(parc::definitions::Field::Key))
      {
        q += " AUTOINCREMENT";
      }
    }
  }
  
  return q + foreign_keys + ")";
//   qFatal("fails");
}

QString QueryGenerator::dropTable(const definitions::Class* _class) const
{
  return "DROP TABLE " + quote(tableName(_class));
}

QString QueryGenerator::createView(const definitions::View* _view) const
{
  QStringList union_q;
  int idx = 0;
  for(const definitions::View::Entry* e : _view->entries())
  {
    QStringList f;
    f.append(QString("%1 AS entryclass").arg(idx++));
    for(int i = 0; i < e->fields.size(); ++i)
    {
      f.append(e->fields[i] + " AS " + _view->fields()[i]->name());
    }
    union_q.append("SELECT " + f.join(",") + " FROM '" + tableName(e->klass) + "'");
  }
  
  return "CREATE VIEW " + quote(viewName(_view)) + " AS " + union_q.join(" UNION ");
}


QString QueryGenerator::createJournalTable(const parc::definitions::Class* _class) const
{
  QString q("CREATE TABLE " + tableJournalName(_class) + "(__journal_id INTEGER PRIMARY KEY AUTOINCREMENT, __journal_entry_type INTEGER, __journal_modified_fields INTEGER DEFAULT 9223372036854775807");

  foreach(const parc::definitions::Field* field, _class->fields())
  {
    if(field->options().testFlag(parc::definitions::Field::Key) or field->isJournaled())
    {
      q += ", " + quote(fieldName(field)) + " " + sqltype(field->sqlType());
    }
  }
  
  return q + ")";
}

QString QueryGenerator::createTreeTable(const parc::definitions::Class* klass) const
{
  QString tablename = quote(tableName(klass));
  QString keyfield  = quote(fieldName(klass->keys().first()));
  
  return "CREATE TABLE " + quote(treeTableName(klass)) + " (ancestor INTEGER, descendant INTEGER, distance INTEGER, PRIMARY KEY(ancestor, descendant), FOREIGN KEY (ancestor) REFERENCES "
       + tablename + "(" + keyfield + "), FOREIGN KEY (descendant) REFERENCES " + tablename + "(" + keyfield + "))";
}

QString QueryGenerator::createJournalTreeTable(const parc::definitions::Class* klass) const
{
  return "CREATE TABLE " + treeJournalTableName(klass) + " (__journal_id INTEGER PRIMARY KEY AUTOINCREMENT, __journal_entry_type INTEGER, ancestor INTEGER, descendant INTEGER)";
}

QString QueryGenerator::generateRemoveAll(const definitions::Class* _class) const
{
  return "DELETE FROM " + tableName(_class);
}

QString QueryGenerator::generateSelect(const parc::definitions::AbstractClass* _class) const
{
  QString q = "SELECT ";
  bool first = true;
  const parc::definitions::View* view = dynamic_cast<const parc::definitions::View*>(_class);
  if(view)
  {
    q += "entryclass";
    first = false;
  }
  for(const parc::definitions::Field* field : _class->fields())
  {
    if(first)
    {
      first = false;
    } else {
      q += ", ";
    }
    q += quote(fieldName(field));
  }
  return q + " FROM " + quote(tableOrViewName(_class)) + " WHERE (%2) %3";
}

QString QueryGenerator::generateSelectCount(const definitions::AbstractClass* _class) const
{
  return "SELECT COUNT(*) FROM " + quote(tableOrViewName(_class)) + " WHERE (%2) %3";
}

QString QueryGenerator::generateSelectOrderBy(const parc::definitions::AbstractClass* _class) const
{
  QString slc = generateSelect(_class);
  slc.chop(2);
  return slc + " ORDER BY %4 %5";
}

QString QueryGenerator::generateLimit() const
{
  return "LIMIT %2";
}

QString QueryGenerator::generateOffsetLimit() const
{
  return "LIMIT %2, %3";
}

QString QueryGenerator::generateOffset() const
{
  return "OFFSET %2";
}

QString QueryGenerator::generateRandomOrder() const
{
  return "RANDOM()";
}

QString QueryGenerator::generateOrderAsc(const parc::definitions::Field* _field) const
{
  return quote(fieldName(_field)) + " ASC";
}

QString QueryGenerator::generateOrderDsc(const parc::definitions::Field* _field) const
{
  return quote(fieldName(_field)) + " DESC";
}

QString QueryGenerator::generateRefreshQuery(const definitions::Class* _class) const
{
  QStringList fieldNames;
  for(const definitions::Field* field : _class->fields())
  {
    fieldNames.append(field->name());
  }
  QString q = "SELECT " + fieldNames.join(", ") + " FROM " + quote(tableName(_class)) + " WHERE (";
  bool first = true;
  foreach(const definitions::Field* field, _class->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      if(first)
      {
        first = false;
      } else {
        q += " AND ";
      }
      q += quote(fieldName(field)) + "=:" + field->name();
    }
  }
  
  return q + ")";
}

QString QueryGenerator::generateRecordQuery(const definitions::Class* _class) const
{
  QString w;
  foreach(const definitions::Field* field, _class->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      if(not w.isEmpty())
      {
        w += " AND ";
      }
      w += quote(fieldName(field)) + "=:" + field->name();
    }
  }
  
  return "UPDATE " + quote(tableName(_class)) + " SET %2 WHERE (" + w+ ")";
}

QString QueryGenerator::generateDeleteQuery(const definitions::Class* _class) const
{
  QString w;
  foreach(const definitions::Field* field, _class->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      if(not w.isEmpty())
      {
        w += " AND ";
      }
      w += quote(fieldName(field)) + "=:" + field->name();
    }
  }
  
  return "DELETE FROM " + quote(tableName(_class)) + " WHERE (" + w + ")";
}

QString QueryGenerator::generateSelectJournalQuery(const definitions::Class* _class) const
{
  return "SELECT * FROM " + tableJournalName(_class) + " WHERE (id >= :id) SORT BY id LIMIT :limit";
}

QString QueryGenerator::generateRecordJournalUpdateQuery(const definitions::Class* _class) const
{
  QString fields, values;
  
  foreach(const definitions::Field* field, _class->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      fields += ", ";
      values += ", :";
      fields += quote(fieldName(field));
      values += field->name();
    }
  }
  
  return "INSERT INTO " + tableJournalName(_class) + "(__journal_entry_type, __journal_modified_fields" + fields + "%2) VALUES(:__journal_entry_type, :__journal_modified_fields" + values + "%3)";
}

QString QueryGenerator::generateRecordJournalInsertQuery(const parc::definitions::Class* _class) const
{
  QString fields, values;
  
  foreach(const definitions::Field* field, _class->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      fields += ", ";
      values += ", :";
      fields += quote(fieldName(field));
      values += field->name();
    }
  }
  return "INSERT INTO " + tableJournalName(_class) + "(__journal_entry_type" + fields + ") VALUES(:__journal_entry_type" + values + ")";
}

QString QueryGenerator::generateRecordJournalDeleteQuery(const definitions::Class* _class) const
{
  QString fields, values;
  
  foreach(const definitions::Field* field, _class->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      fields += ", ";
      values += ", :";
      fields += quote(fieldName(field));
      values += field->name();
    }
  }
  return "INSERT INTO " + tableJournalName(_class) + "(__journal_entry_type" + fields + ") VALUES(:__journal_entry_type" + values + ")";  
}

QString QueryGenerator::generateExistQuery(const definitions::Class* _class) const
{
  QString q = "SELECT (" + quote(fieldName(_class->fields().first())) + ") FROM " + quote(tableName(_class)) + " WHERE (";
  bool first = true;
  foreach(const definitions::Field* field, _class->fields())
  {
    if(field->options().testFlag(definitions::Field::Key))
    {
      if(first)
      {
        first = false;
      } else {
        q += " AND ";
      }
      q += quote(fieldName(field)) + "=:" + field->name();
    }
  }
  
  return q + ")";  
}

QString QueryGenerator::generateWhereEq() const
{
  return "\\\"%2\\\" = %3";
}
QString QueryGenerator::generateWhereGT() const
{
  return "\\\"%2\\\" > %3";
}

QString QueryGenerator::generateWhereGTE() const
{
  return "\\\"%2\\\" >= %3";
}

QString QueryGenerator::generateWhereLike() const
{
  return "\\\"%2\\\"  LIKE %3";
}

QString QueryGenerator::generateWhereLT() const
{
  return "\\\"%2\\\" < %3";
}

QString QueryGenerator::generateWhereLTE() const
{
  return "\\\"%2\\\" <= %3";
}

QString QueryGenerator::generateWhereNEq() const
{
  return "\\\"%2\\\" != %3";
}

QString QueryGenerator::generateDateTimeToQuery() const
{
  return "datetime(%2, 'unixepoch')";
}

QString QueryGenerator::generateCreateQuery(const definitions::Class* _klass, bool _include_auto_field_in_query) const
{
  QString q = "INSERT INTO " + quote(tableName(_klass)) + "(";
  QString vs;
  bool first = true;
  
  for(const definitions::Field* field : _klass->fields())
  {
    if( (_include_auto_field_in_query or not field->options().testFlag(definitions::Field::Auto))
        and not field->options().testFlag(definitions::Field::Dependent) )
    {
      if(first)
      {
        first = false;
      } else {
        q  += ", ";
        vs += ", ";
      }
      q   += quote(fieldName(field));
      vs  += ":" + field->name();
    }
  }
  
  return q + ") VALUES (" + vs + ")";
}

bool QueryGenerator::createReturnValues() const
{
  return false;
}

using namespace parc::utils;

QString QueryGenerator::tableName(const parc::definitions::Class* _class) const
{
  return rawTableName(_class);
}

QString QueryGenerator::viewName(const definitions::View* _view) const
{
  return rawViewName(_view);
}

QString QueryGenerator::tableOrViewName(const definitions::AbstractClass* _class) const
{
  const definitions::Class* klass = dynamic_cast<const definitions::Class*>(_class);
  if(klass)
  {
    return tableName(klass);
  }
  const definitions::View* view = dynamic_cast<const definitions::View*>(_class);
  if(view)
  {
    return viewName(view);
  }
  qFatal("Not a class or a view?");
}

QString QueryGenerator::rawTableName(const definitions::Class* _class) const
{
  QString name = plural(_class->name());
  if(_class->classOfContainerDefinition())
  {
    name += "_%1";
  }
  return name;
}

QString QueryGenerator::rawViewName(const definitions::View* _view) const
{
  return plural(_view->name());
}

QString QueryGenerator::tableJournalName(const definitions::Class* _class) const
{
  return rawTableName(_class) + "_journal";
}

QString QueryGenerator::treeJournalTableName(const definitions::Class* _class) const
{
  return rawTableName(_class) + "_tree_journal";
}

QString QueryGenerator::treeTableName(const definitions::Class* _class) const
{
  return rawTableName(_class) + "_tree";
}

QString QueryGenerator::fieldName(const parc::definitions::Field* _field) const
{
  return _field->name();
}

QString QueryGenerator::mappingFieldName(const definitions::Class* _class) const
{
  return utils::uncapitalize(_class->name());
}

QString QueryGenerator::mappingTableName(const parc::definitions::Mapping* _mapping) const
{
  return uncapitalize(plural(_mapping->a()->name())) + "2" + capitalize(plural(_mapping->b()->name()));
}

QString QueryGenerator::mappingJournalTableName(const parc::definitions::Mapping* _mapping) const
{
  return uncapitalize(plural(_mapping->a()->name())) + "2" + capitalize(plural(_mapping->b()->name())) + "_journal";
}

QString QueryGenerator::generateTreeCreateQuery(const definitions::Class* _klass) const
{
  return "INSERT INTO " + quote(treeTableName(_klass)) + " (ancestor, descendant, distance) VALUES (:id1, :id2, 0)";
}

QString QueryGenerator::generateTreeChildrenQuery(const parc::definitions::Class* _klass) const
{
  return "SELECT descendant FROM " + quote(treeTableName(_klass)) + " WHERE ancestor = :id AND distance = 1";

}

QString QueryGenerator::generateTreeParentsQuery(const parc::definitions::Class* _klass) const
{
  return "SELECT ancestor FROM " + quote(treeTableName(_klass)) + " WHERE descendant = :id AND distance = 1";
}

QString QueryGenerator::generateTreeAddChildQuery(const definitions::Class* _klass) const
{
  return upsert(quote(treeTableName(_klass)) + " (ancestor, descendant, distance) "
                    "SELECT supertree.ancestor, subtree.descendant, MIN(supertree.distance + subtree.distance + 1, "
                    " COALESCE((SELECT distance FROM " + quote(treeTableName(_klass)) + " WHERE ancestor = supertree.ancestor AND descendant = subtree.descendant), 9223372036854775808)) "
                    "FROM " + quote(treeTableName(_klass)) + " AS supertree "
                    "CROSS JOIN " + quote(treeTableName(_klass)) + " AS subtree "
                    "WHERE supertree.descendant = :parent "
                    "AND subtree.ancestor = :child;", UpsertType::Tree);
}

QString QueryGenerator::generateTreeAddChildcJournalQuery(const parc::definitions::Class* _klass) const
{
  return "INSERT INTO " + quote(treeJournalTableName(_klass)) + " (__journal_entry_type, ancestor, descendant) VALUES(:__journal_entry_type, :ancestor, :descendant)";
}

QString QueryGenerator::generateTreeIsChildQuery(const definitions::Class* _klass) const
{
  return "SELECT * FROM " + quote(treeTableName(_klass)) + " WHERE ancestor = :parent AND descendant = :child AND distance <= :distance";
}

QString QueryGenerator::generateGetMappingsQuery(const parc::definitions::Mapping* _mapping, const parc::definitions::Class* _class) const
{
  QString a_n = quote(mappingFieldName(_mapping->a()));
  QString b_n = quote(mappingFieldName(_mapping->b()));

  if(_class == _mapping->b())
  {
    std::swap(a_n, b_n);
  }
  return "SELECT " + b_n + " FROM " + quote(mappingTableName(_mapping)) + " WHERE ( " + a_n + "= :id_self)";
}

QString QueryGenerator::generateRemoveMappingQuery(const parc::definitions::Mapping* _mapping, const parc::definitions::Class* _class) const
{
  QString a_n, b_n;
  
  if(_class == _mapping->a())
  {
    a_n = ":id_self";
    b_n = ":id_other";
  } else {
    a_n = ":id_other";
    b_n = ":id_self";
  }
  QString a_f = quote(mappingFieldName(_mapping->a()));
  QString b_f = quote(mappingFieldName(_mapping->b()));
  
  return "DELETE FROM " + quote(mappingTableName(_mapping)) + " WHERE (" + a_f + "=" + a_n + " AND " + b_f + "=" + b_n + ")";
}

QString QueryGenerator::generateAddMappingQuery(const parc::definitions::Mapping* _mapping, const parc::definitions::Class* _class) const
{
  QString a_n, b_n;
  
  if(_class == _mapping->a())
  {
    a_n = ":id_self";
    b_n = ":id_other";
  } else {
    a_n = ":id_other";
    b_n = ":id_self";
  }
  QString a_f = quote(mappingFieldName(_mapping->a()));
  QString b_f = quote(mappingFieldName(_mapping->b()));
  
  return upsert(quote(mappingTableName(_mapping)) + " (" + a_f + ", " + b_f + ") VALUES (" + a_n + ", " + b_n + ")", UpsertType::Mapping);
}

QString QueryGenerator::generateHasMappingQuery(const parc::definitions::Mapping* _mapping, const parc::definitions::Class* _class) const
{
  QString a_n, b_n;
  
  if(_class == _mapping->a())
  {
    a_n = ":id_self";
    b_n = ":id_other";
  } else {
    a_n = ":id_other";
    b_n = ":id_self";
  }
  QString a_f = quote(mappingFieldName(_mapping->a()));
  QString b_f = quote(mappingFieldName(_mapping->b()));

  return "SELECT COUNT(*) FROM " + quote(mappingTableName(_mapping)) + " WHERE (" + a_f + "=" + a_n + " AND " + b_f + "=" + b_n + ")";
}

QString QueryGenerator::generateMappingUpdateJournalQuery(const definitions::Mapping* _mapping) const
{
  QString a_f = quote(mappingFieldName(_mapping->a()));
  QString b_f = quote(mappingFieldName(_mapping->b()));

  return "INSERT INTO " + quote(mappingJournalTableName(_mapping)) + "(__journal_entry_type, " + a_f + ", " + b_f + ") VALUES (:__journal_entry_type, :id_a, :id_b)";
}

QString QueryGenerator::generateGetRowID(const definitions::Class*) const
{
  qFatal("No generate get rowid for this query generator.");
}

QString QueryGenerator::generateSelectFromRowID(const definitions::Class*) const
{
  qFatal("No generate get rowid for this query generator.");
}
