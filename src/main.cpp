#include <iostream>

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QPluginLoader>

#include "Lexer.h"
#include "Parser.h"
#include "Error.h"
#include "../parc/Generators.h"


#include "qtcpp/CodeGenerator.h"
#include "postgres/QueryGenerator.h"
#include "sqlite/QueryGenerator.h"

int main(int _argc, char** _argv)
{
  QCoreApplication app(_argc, _argv);
  QCoreApplication::setApplicationName("parc");
  QCoreApplication::setApplicationVersion("0.9.0");
  
  QCommandLineParser cmd_parser;
  cmd_parser.addHelpOption();
  cmd_parser.addVersionOption();
  cmd_parser.addPositionalArgument("codebackend", "Name of the backend used to generate the code");
  cmd_parser.addPositionalArgument("dbbackend", "Name of the backend used to generate the database queries");
  cmd_parser.addPositionalArgument("input", "Filename describing the active records");
  cmd_parser.addPositionalArgument("output", "Output directory for the generated files");
  
  QCommandLineOption pluginsDirOption("plugins-dirs", "<plugins_dirs> is a semicolon seperated list of directory containing plugins for parc", "plugins_dirs");
  cmd_parser.addOption(pluginsDirOption);
  cmd_parser.process(app);

  parc::Generators::add<parc::qtcpp::CodeGenerator>("qtcpp",                               parc::qtcpp::CodeGenerator::Type::QtSQL,        false,  false);
  parc::Generators::add<parc::qtcpp::CodeGenerator>("cyqlops-db-sqlite-cpp",               parc::qtcpp::CodeGenerator::Type::CyqlopsDBSQLite,    false,  false);
  parc::Generators::add<parc::qtcpp::CodeGenerator>("cyqlops-db-sqlite-qtobject-cpp",      parc::qtcpp::CodeGenerator::Type::CyqlopsDBSQLite,    true,   false);
  parc::Generators::add<parc::qtcpp::CodeGenerator>("cyqlops-db-sqlite-qml-cpp",           parc::qtcpp::CodeGenerator::Type::CyqlopsDBSQLite,    true,   true);
  parc::Generators::add<parc::qtcpp::CodeGenerator>("cyqlops-db-postgresql-cpp",           parc::qtcpp::CodeGenerator::Type::CyqlopsDBPostgreSQL, false,  false);
  parc::Generators::add<parc::qtcpp::CodeGenerator>("cyqlops-db-postgresql-qtobject-cpp",  parc::qtcpp::CodeGenerator::Type::CyqlopsDBPostgreSQL, true,   false);
  parc::Generators::add<parc::qtcpp::CodeGenerator>("cyqlops-db-postgresql-qml-cpp",       parc::qtcpp::CodeGenerator::Type::CyqlopsDBPostgreSQL, true,   true);
  parc::Generators::add<parc::sqlite::QueryGenerator>("sqlite");
  parc::Generators::add<parc::postgres::QueryGenerator>("postgres");
  
  QList<QDir> plugins_dirs;
  plugins_dirs << QDir(QCoreApplication::applicationDirPath() + "/../lib/parc");
  
  QString plugins_dirs_option = cmd_parser.value(pluginsDirOption);
  foreach(const QString& dir_name, plugins_dirs_option.split(";"))
  {
    if(not dir_name.isEmpty())
    {
      plugins_dirs << QDir(dir_name);
    }
  }
  
  QList<QPluginLoader*> plugins;
  foreach(const QDir& dir, plugins_dirs)
  {
    foreach(const QFileInfo& file, dir.entryInfoList(QDir::Files))
    {
      QPluginLoader* plugin = new QPluginLoader(file.absoluteFilePath());
      if(plugin->load())
      {
        plugins.append(plugin);
      } else if(file.suffix() == "so" or file.suffix() == "dll" or file.suffix() == "dylib") {
        qWarning() << "Failed to load " << file.absoluteFilePath() << " with error " << plugin->errorString();
      }
    }
  }
  
  const QStringList args = cmd_parser.positionalArguments();

  if(args.size() != 4)
  {
    cmd_parser.showHelp(-1);
  }
  
  QString codebackend_str = args[0];
  QString   dbbackend_str = args[1];
  QString input_filename  = args[2];
  QString output_dirname  = args[3];
  
  QFile file(input_filename);
  file.open(QIODevice::ReadOnly);
  
  parc::AbstractQueryGenerator* aqg = parc::Generators::createQueryGenerator(dbbackend_str);
  if(not aqg)
  {
    qWarning() << "Unknown query generator " << dbbackend_str;
    return -1;
  }
  parc::AbstractCodeGenerator* acg = parc::Generators::createCodeGenerator(codebackend_str, aqg);
  if(not acg)
  {
    qWarning() << "Unknown code generator " << codebackend_str;
    return -1;
  }
  
  parc::Lexer lexer(&file);
  parc::Parser parser(&lexer, input_filename);
  parc::definitions::Database* dbdef = parser.parse();
  
  QDir().mkpath(output_dirname);
  
  if(dbdef)
  {
    QString reason;
    if(acg->canGenerate(dbdef, &reason))
    {
      acg->generate(dbdef, output_dirname);
      return 0;
    } else {
      std::cout << "Cannot generate: " << qPrintable(reason) << std::endl;
      return -1;
    }
  } else {
    QList<parc::Error> errors = parser.errors();
    std::cout << "Compilation failed, with " << errors.size() << " error(s): " << std::endl;
    foreach(const parc::Error& error, errors)
    {
      std::cout << qPrintable(error.filename) << "(" << error.line << "," << error.column << "): " << qPrintable(error.message) << std::endl;
    }
    return -1;
  }
  
}
