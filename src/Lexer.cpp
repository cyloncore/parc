/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Lexer.h"

#include <QDebug>
#include <QIODevice>

#include "Token.h"

using namespace parc;

struct Lexer::Private {
  QIODevice* stream;
  int lastChar;
  int col;
  int line;
  int followingnewline;
};

Lexer::Lexer(QIODevice* sstream) : d(new Private)
{
  d->col = 1;
  d->line = 1;
  d->followingnewline = 1;
  d->stream = sstream;
}

Lexer::~Lexer()
{
  delete d;
}

#define IDENTIFIER_IS_KEYWORD( tokenname, tokenid) \
  if( identifierStr == tokenname) \
  { \
    return Token(Token::tokenid, line(), initial_col); \
  }

#define CHAR_IS_TOKEN( tokenchar, tokenid ) \
  if( lastChar == tokenchar ) \
  { \
    return Token(Token::tokenid, line(), initial_col); \
  }

#define CHAR_IS_TOKEN_OR_TOKEN( tokenchar, tokendecidechar, tokenid_1, tokenid_2 ) \
  if( lastChar == tokenchar  ) \
  { \
    if( getNextChar() == tokendecidechar ) \
    { \
      return Token(Token::tokenid_2, line(), initial_col); \
    } else { \
      unget(); \
      return Token(Token::tokenid_1, line(), initial_col); \
    } \
  }

#define CHAR_IS_TOKEN_OR_TOKEN_OR_TOKEN( tokenchar_1, tokenchar_2, tokenchar_3, tokenid_1, tokenid_2, tokenid_3 ) \
  if( lastChar == tokenchar_1 ) \
  { \
    int nextChar = getNextChar(); \
    if( nextChar == tokenchar_2 ) \
    { \
      return Token(Token::tokenid_2, line(), initial_col); \
    } else if( nextChar  == tokenchar_3 ) \
    { \
      return Token(Token::tokenid_3, line(), initial_col); \
    } else { \
      unget(); \
      return Token(Token::tokenid_1, line(), initial_col); \
    } \
  }

Token Lexer::nextToken()
{
  int lastChar = getNextNonSeparatorChar();
  int initial_line = line() - 1;
  int initial_col = column() - 1;
  if( eof() ) return Token(Token::END_OF_FILE, line(), initial_col);
  QString identifierStr;
  // Test for comment
  Token commentToken;
  if( ignoreComment( commentToken, lastChar ) )
  {
    return commentToken;
  }
  //
  if(lastChar == '@')
  {
    lastChar = getNextChar();
    if(isalpha(lastChar) or lastChar == '_')
    {
      identifierStr = getIdentifier(lastChar);
      return Token(Token::IDENTIFIER, identifierStr, line(), initial_col);
    } else {
      return Token(Token::INVALID_IDENTIFIER, line(), initial_col);
    }
  }
  // if it is alpha, it's an identifier or a keyword
  if(isalpha(lastChar) or lastChar == '_')
  {
    identifierStr = getIdentifier(lastChar);
    
    if(identifierStr == "true")
    {
      return Token(Token::CONSTANT_EXPRESSION, "true", line(), initial_col);
    }
    if(identifierStr == "false")
    {
      return Token(Token::CONSTANT_EXPRESSION, "false", line(), initial_col);
    }
    
    IDENTIFIER_IS_KEYWORD( "class", CLASS );
    IDENTIFIER_IS_KEYWORD( "key", KEY );
    IDENTIFIER_IS_KEYWORD( "bool", BOOLEAN );
    IDENTIFIER_IS_KEYWORD( "boolean", BOOLEAN );
    IDENTIFIER_IS_KEYWORD( "int", INT );
    IDENTIFIER_IS_KEYWORD( "float", FLOAT );
    IDENTIFIER_IS_KEYWORD( "double", DOUBLE );
    IDENTIFIER_IS_KEYWORD( "blob", BLOB );
    IDENTIFIER_IS_KEYWORD( "of", OF );
    IDENTIFIER_IS_KEYWORD( "tree", TREE );
    IDENTIFIER_IS_KEYWORD( "string", STRING );
    IDENTIFIER_IS_KEYWORD( "datetime", DATETIME );
    IDENTIFIER_IS_KEYWORD( "parent", PARENT );
    IDENTIFIER_IS_KEYWORD( "required", REQUIRED );
    IDENTIFIER_IS_KEYWORD( "namespace", NAMESPACE );
    IDENTIFIER_IS_KEYWORD( "map", MAP );
    IDENTIFIER_IS_KEYWORD( "with", WITH );
    IDENTIFIER_IS_KEYWORD( "as", AS );
    IDENTIFIER_IS_KEYWORD( "typedef", TYPEDEF );
    IDENTIFIER_IS_KEYWORD( "many", MANY);
    IDENTIFIER_IS_KEYWORD( "public", PUBLIC);
    IDENTIFIER_IS_KEYWORD( "private", PRIVATE);
    IDENTIFIER_IS_KEYWORD( "protected", PROTECTED);
    IDENTIFIER_IS_KEYWORD( "const", CONST);
    IDENTIFIER_IS_KEYWORD( "auto", AUTO);
    IDENTIFIER_IS_KEYWORD( "unique", UNIQUE);
    IDENTIFIER_IS_KEYWORD( "forward", FORWARD);
    IDENTIFIER_IS_KEYWORD( "journaled", JOURNALED);
    IDENTIFIER_IS_KEYWORD( "database", DATABASE);
    IDENTIFIER_IS_KEYWORD( "implements", IMPLEMENTS);
    IDENTIFIER_IS_KEYWORD( "indexed", INDEXED);
    IDENTIFIER_IS_KEYWORD( "define", DEFINE);
    IDENTIFIER_IS_KEYWORD( "enum", ENUM);
    IDENTIFIER_IS_KEYWORD( "flags", FLAGS);
    IDENTIFIER_IS_KEYWORD( "dependent", DEPENDENT);
    IDENTIFIER_IS_KEYWORD( "and", AND );
    IDENTIFIER_IS_KEYWORD( "view", VIEW );
    IDENTIFIER_IS_KEYWORD( "include", INCLUDE );
    return Token(Token::IDENTIFIER, identifierStr, line(), initial_col);
  } else if( isdigit(lastChar) )
  { // if it's a digit 
    return getDigit(lastChar);
  } else if( lastChar == '"' ) {
    return getString(lastChar);
  } else {
    CHAR_IS_TOKEN(';', SEMI );
    CHAR_IS_TOKEN_OR_TOKEN(':', ':', COLON, COLONCOLON );
    CHAR_IS_TOKEN( '=', EQUAL );
    CHAR_IS_TOKEN( '<', INFERIOR );
    CHAR_IS_TOKEN( '>', SUPPERIOR );
    CHAR_IS_TOKEN( '{', STARTBRACE );
    CHAR_IS_TOKEN( '}', ENDBRACE );
    CHAR_IS_TOKEN( '(', STARTBRACKET );
    CHAR_IS_TOKEN( ')', ENDBRACKET );
    CHAR_IS_TOKEN( ',', COMMA );
  }
  if( lastChar > 128 ) return nextToken();
  identifierStr = ushort(lastChar);
  qDebug() << "Unknown token : " << lastChar << " '" << identifierStr << "' at " << initial_line << "," << initial_col;
  Q_ASSERT( not isspace(lastChar));
  return Token(Token::UNKNOWN, initial_line, initial_col);
}

ushort Lexer::getNextNonSeparatorChar()
{
  ushort lastChar = ' ';
  while( not eof() and isspace(lastChar = getNextChar() )  )
  { // Ignore space
  }
  return lastChar;
}

ushort Lexer::getNextChar()
{
  char nc;
  if(d->stream->getChar(&nc))
  {
    d->lastChar = nc;
    if( nc == '\n' )
    {
      ++d->line;
      ++d->followingnewline;
      d->col = 1;
    } else {
      ++d->col;
      d->followingnewline = 0;
    }
  } else {
    return 0;
  }
  return nc;
}

void Lexer::unget()
{
  --d->col;
  d->stream->ungetChar(d->lastChar);
  if( d->followingnewline > 0 )
  {
    --d->followingnewline;
    --d->line;
  }
}

bool Lexer::eof() const
{
  return d->stream->atEnd();
}

int Lexer::line() const
{
  return d->line;
}
int Lexer::column() const
{
  return d->col;
}

bool Lexer::ignoreComment(Token& _token, ushort _lastChar )
{
  if( _lastChar == '/' )
  {
    int initial_line = line();
    int initial_col = column();
    int nextChar = getNextChar();
    if( nextChar == '/' )
    { // Mono line comment
      while( not eof() and getNextChar() != '\n' )
      {
      }
      _token = nextToken();
      return true;
    } else if( nextChar == '*' )
    { // Multi line comment
      while( not eof() )
      {
        int nextChar = getNextChar();
        if( nextChar == '*' )
        {
          if( getNextChar() == '/' )
          {
            _token = nextToken();
            return true;
          } else {
            unget();
          }
        }
      }
      _token = Token(Token::UNFINISHED_COMMENT, initial_line, initial_col);
      return true;
    } else {
      unget();
    }
  }
  return false;
}

QString Lexer::getIdentifier(ushort lastChar)
{
  QString identifierStr;
  if( lastChar != 0) {
    identifierStr = lastChar;
  }
  while (not eof() )
  {
    lastChar = getNextChar();
    if( isalnum(lastChar) or lastChar == '_')
    {
      identifierStr += lastChar;
    } else {
      unget();
      break;
    }
  }
  return identifierStr;
}

Token Lexer::getDigit(ushort lastChar)
{
  int initial_col = column();
  QString identifierStr;
  identifierStr= lastChar;
  bool integer = true;
  bool exponent = false;
  while (not eof()
         and ( isdigit((lastChar = getNextChar()))
               or lastChar == '.' or (lastChar == 'e' and not exponent )
               or (exponent and lastChar =='-' ) ) )
  {
    identifierStr += lastChar;
    if( lastChar == '.' )
    {
      integer = false;
    }
    if( lastChar == 'e' )
    {
      integer = false;
      exponent = true;
    }
  }
  unget();
  if(integer)
  {
    return Token(Token::CONSTANT_EXPRESSION, identifierStr, line(), initial_col );
  } else {
    return Token(Token::CONSTANT_EXPRESSION, identifierStr, line(), initial_col );
  }
}

Token Lexer::getString(ushort lastChar)
{
  int initial_col = column();
  int previousChar = lastChar;
  QString identifierStr = "";
  while( not eof() )
  {
    ushort nextChar = getNextChar();
    if( nextChar == '"' and previousChar != '\\')
    {
      return Token( Token::STRING_CONSTANT, identifierStr, line(), initial_col );
    } else {
      previousChar = nextChar;
      identifierStr += nextChar;
    }
  }
  return Token( Token::UNFINISHED_STRING, line(), initial_col);
}

QString Lexer::readUntil(QChar endChar)
{
  QString str;
  while(not eof())
  {
    ushort nextChar = getNextChar();
    if(nextChar == endChar)
    {
      unget();
      return str;
    }
    str += QChar(nextChar);
  }
  return str;
}

QString Lexer::readOneArgument()
{
  QString str;
  bool in_string = false;
  bool skip = false;
  while(not eof())
  {
    int nextChar = getNextChar();
    if(nextChar == '\"')
    {
      in_string = !in_string;
    } else if(not in_string)
    {
      if(nextChar == '\\')
      {
        skip = true;
      } else if(skip)
      {
        skip = false;
      } else if(nextChar == ',' or nextChar == ')')
      {
        unget();
        return str;
      }
    }
    if(not skip)
    {
      str += QChar(nextChar);
    }
  }
  return str;
  
}
