/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "Parser.h"

#include <QDebug>
#include <QList>
#include <QStringList>

#include "Error.h"
#include "Lexer.h"

#include "../parc/definitions/Access.h"
#include "../parc/definitions/Class.h"
#include "../parc/definitions/Database.h"
#include "../parc/definitions/Enum.h"
#include "../parc/definitions/Field.h"
#include "../parc/definitions/Flags.h"
#include "../parc/definitions/Forward.h"
#include "../parc/definitions/Index.h"
#include "../parc/definitions/Object.h"
#include "../parc/definitions/Property.h"
#include "../parc/definitions/Typedef.h"
#include "../parc/definitions/Unique.h"
#include "../parc/definitions/View.h"

using namespace parc;

struct Parser::Private
{
  Lexer* lexer;
  Token tok;
  QList<Error> errors;
  QString fileName;
  definitions::Database* db;
};

Parser::Parser(Lexer* _lexer, const QString& _filename) : d(new Private)
{
  d->lexer    = _lexer;
  d->fileName = _filename;
  d->db       = 0;
}

Parser::~Parser()
{
  delete d;
}

definitions::Database* Parser::parse()
{
  d->db = new definitions::Database;
  getNextToken();
  
  while(d->tok.type != Token::END_OF_FILE)
  {
    
    switch(d->tok.type)
    {
      case Token::TREE:
      {
        getNextToken();
        if(isOfType(Token::OF))
        {
          getNextToken();
          parseClass(true);
        } else {
          reachNext(Token::ENDBRACE);
        }
        break;
      }
      case Token::CLASS:
        parseClass(false);
        break;
      case Token::VIEW:
        parseView();
        break;
      case Token::TYPEDEF:
        d->db->add(parseTypeDef());
        break;
      case Token::NAMESPACE:
        parseNamespaces();
        break;
      case Token::MAP:
        parseMapping();
        break;
      case Token::DATABASE:
        parseDatabase();
        break;
      case Token::FORWARD:
        parseForward();
        break;
      default:
        reportUnexpected(d->tok);
        reachNext(Token::SEMI);
        break;
    }
    isOfType(Token::SEMI);
    getNextToken();
  }
  
  
  if(d->errors.empty())
  {
    return d->db;
  } else {
    delete d->db;
    return 0;
  }
}

void Parser::parseClass(bool _set_on_tree)
{
  isOfType(Token::CLASS);
  getNextToken();
  if(d->tok.type == Token::STARTBRACE)
  {
    if(_set_on_tree)
    {
      reportError(d->tok, "'Class of class' is not compatible with 'tree of'");
    }
    Token mother_kl_t = d->tok;
    definitions::Class* mother_kl = new definitions::Class(d->db, QString());
    parseClassBody(mother_kl);
    QList<const definitions::Field*> keys = mother_kl->keys();
    if(keys.size() > 1)
    {
      reportError(mother_kl_t, "Class of class definitions can only have one key.");
    } else if(keys.size() < 1)
    {
      reportError(mother_kl_t, "Class of class definitions needs at least one key.");
    } else if(keys.front()->sqlType()->type() == SqlType::REFERENCE)
    {
      QList<const definitions::Field*> ref_keys = ((definitions::Class*)keys.front()->sqlType())->keys();
      if(ref_keys.size() != 1 and (ref_keys.empty() or (ref_keys.front()->sqlType()->type() != SqlType::INTEGER and ref_keys.front()->sqlType()->type() != SqlType::REFERENCE)))
      {
        reportError(mother_kl_t, "Parent class needs to have an integer or reference key.");
      }
    } else if(keys.front()->sqlType()->type() != SqlType::INTEGER)
    {
      reportError(mother_kl_t, "Key need to be integer or a reference.");
    }
    
    isOfType(Token::ENDBRACE);
    getNextToken();
    isOfType(Token::OF);
    getNextToken();
    isOfType(Token::CLASS);
    getNextToken();
    isOfType(Token::IDENTIFIER);
    QString className = d->tok.string;
    definitions::Class* child_kl = new definitions::Class(d->db, className);
    getNextToken();
    parseClassBody(child_kl);
    d->db->add(mother_kl, child_kl);
    if(isOfType(Token::ENDBRACE))
    {
      getNextToken();
      return;
    }
  } else if(isOfType(Token::IDENTIFIER))
  {
    QString className = d->tok.string;
    definitions::Class* klass = d->db->getOrCreateClass(className);
    if(_set_on_tree)
    {
      klass->setTree(true);
    }
    getNextToken();
    if(d->tok.type == Token::JOURNALED)
    {
      klass->markJournaled();
      getNextToken();
    }
    
    if(d->tok.type == Token::SEMI) // Forward declaration
    {
      return;
    } else {
      parseClassBody(klass);
    }
    if(isOfType(Token::ENDBRACE))
    {
      getNextToken();
      if(d->tok.type == Token::OF)
      {
        while(d->tok.type == Token::OF or d->tok.type == Token::AND)
        {
          getNextToken();
          isOfType(Token::CLASS);
          getNextToken();
          isOfType(Token::IDENTIFIER);
          QString className = d->tok.string;
          definitions::Class* child_kl = new definitions::Class(d->db, className);
          getNextToken();
          parseClassBody(child_kl);
          klass->addClassOfDefinition(child_kl);
          if(isOfType(Token::ENDBRACE))
          {
            getNextToken();
          } else {
            reachNext(Token::ENDBRACE);
            getNextToken();
            return;
          }
        }
        return;
      } else {
        return;
      }
    }
  }
  reachNext(Token::ENDBRACE);
  getNextToken();
  return;
}

void Parser::parseView()
{
  isOfType(Token::VIEW);
  getNextToken();
  isOfType(Token::IDENTIFIER);
  definitions::View* view = new definitions::View(d->db, d->tok.string);
  getNextToken();

  if(d->tok.type == Token::IMPLEMENTS)
  {
    getNextToken();
    isOfType(Token::IDENTIFIER);
    while(d->tok.type == Token::IDENTIFIER)
    {
      view->markImplement(interface(d->tok));
      getNextToken();
      if(d->tok.type == Token::COMMA)
      {
        getNextToken();
        isOfType(Token::IDENTIFIER);
      } else if(d->tok.type == Token::STARTBRACE)
      {
        break;
      } else {
        reportUnexpected(d->tok);
      }
    }
  }
    
  if(d->tok.type == Token::STARTBRACE)
  {
    getNextToken(); // skip {
    while(d->tok.type != Token::INCLUDE and d->tok.type != Token::END_OF_FILE)
    {
      qDebug() << "field" << d->tok.type << Token::IDENTIFIER << Token::END_OF_FILE;

      const SqlType* sqlType = nullptr;
      if(d->tok.isSqlType())
      {
        sqlType = parseSqlType();
        getNextToken();
      } else if(d->tok.type == Token::IDENTIFIER)
      {
        sqlType = d->db->sqlType(d->tok.string);
        if(not sqlType)
        {
          reportError(d->tok, QString("Unknown type '%1'").arg(d->tok.string));
        }
        getNextToken();
      }

      if(sqlType)
      {
        isOfType(Token::IDENTIFIER);
        QString name = d->tok.string;
        view->addField(name, sqlType);
        getNextToken();
        isOfType(Token::SEMI);
      } else {
        reportUnexpected(d->tok);
        reachNext(Token::SEMI);        
      }
      getNextToken();
    }
    while(d->tok.type == Token::INCLUDE)
    {
      qDebug() << "incl" << d->tok.type << Token::INCLUDE << Token::END_OF_FILE;
      getNextToken();
      isOfType(Token::IDENTIFIER);
      definitions::Class* kl = d->db->getClass(d->tok.string);
      if(kl)
      {
        getNextToken();
        isOfType(Token::STARTBRACKET);
        getNextToken();
        QStringList fields;
        while(d->tok.type == Token::IDENTIFIER)
        {
          fields.append(d->tok.string);
          getNextToken();
          if(d->tok.type == Token::COMMA)
          {
            getNextToken();
            isOfType(Token::IDENTIFIER);
          }
        }
        isOfType(Token::ENDBRACKET);
        getNextToken();
        isOfType(Token::SEMI);
        getNextToken();
        
        if(fields.size() == view->fields().size())
        {
          for(int i = 0; i < fields.size(); ++i)
          {
            const definitions::Field* kl_field = kl->field(fields[i]);
            if(not kl_field)
            {
              reportError(d->tok, QString("No such field %1.").arg(fields[i]));
            } else if(kl_field->sqlType() != view->fields()[i]->sqlType())
            {
              reportError(d->tok, QString("Field type %1 does not match view field type.").arg(fields[i]));
            }
          }
          view->addEntry(kl, fields);
        } else {
          reportError(d->tok, "Invalid number of fields in include, must match view.");
        }
      } else {
        reportError(d->tok, QString("Unknown class '%1'").arg(d->tok.string));
        reachNext(Token::SEMI);
        getNextToken();
      }
    }
    isOfType(Token::ENDBRACE);
    getNextToken();
    d->db->add(view);
  }
}

definitions::Interface Parser::interface(const Token& _token)
{
  definitions::Interface interface = definitions::INVALID_INTERFACE;
  if(_token.string == "init")
  {
    interface = definitions::INIT;
  } else if(_token.string == "cleanup")
  {
    interface = definitions::CLEANUP;
  } else if(_token.string == "extended")
  {
    interface = definitions::EXTENDED;
  } else if(_token.string == "notifications")
  {
    interface = definitions::NOTIFICATIONS;
  } else if(_token.string == "update_notification")
  {
    interface = definitions::UPDATENOTIFICATION;
  } else {
    reportError(_token, QString("Unknown interface '%1'.").arg(_token.string));
  }
  return interface;
}

void Parser::parseClassBody(definitions::Class* klass)
{
  if(d->tok.type == Token::IMPLEMENTS)
  {
    getNextToken();
    isOfType(Token::IDENTIFIER);
    while(d->tok.type == Token::IDENTIFIER)
    {
      klass->markImplement(interface(d->tok));
      getNextToken();
      if(d->tok.type == Token::COMMA)
      {
        getNextToken();
        isOfType(Token::IDENTIFIER);
      } else if(d->tok.type == Token::STARTBRACE)
      {
        break;
      } else {
        reportUnexpected(d->tok);
      }
    }
  }
  
  definitions::Journaled journaled = klass->journaled();
  if(journaled == definitions::HAS_JOURNALED_CHILDREN)
  {
    journaled = definitions::JOURNALED;
  }
  isOfType(Token::STARTBRACE);
  getNextToken();
  while(d->tok.type != Token::ENDBRACE and d->tok.type != Token::END_OF_FILE)
  {
    definitions::Access access = definitions::Access::Public;
    switch(d->tok.type)
    {
      case Token::PRIVATE:
        access = definitions::Access::Private;
        getNextToken();
        break;
      case Token::PROTECTED:
        access = definitions::Access::Protected;
        getNextToken();
        break;
      case Token::PUBLIC:
        access = definitions::Access::Public;
        getNextToken();
        break;
      default:
        break;
    }
  
    definitions::Field::Options options;
    const SqlType*              sqltype   = 0;
    const definitions::Typedef* typedef_  = 0;
    
    if(d->tok.type == Token::ENUM)
    {
      getNextToken();
      isOfType(Token::IDENTIFIER);
      QString enum_name = d->tok.string;
      getNextToken();
      isOfType(Token::STARTBRACE);
      getNextToken();
      QStringList values;
      while(d->tok.type == Token::IDENTIFIER)
      {
        values.append(d->tok.string);
        getNextToken();
        if(d->tok.type == Token::COMMA)
        {
          getNextToken();
        } else if(isOfType(Token::ENDBRACE))
        {
          break;
        }
      }
      isOfType(Token::ENDBRACE);
      getNextToken();
      isOfType(Token::SEMI);
      getNextToken();
      definitions::Enum* e = new definitions::Enum(enum_name, values, klass);
      klass->addEnum(e);
      d->db->add(new definitions::Typedef(e->fullName(), klass->database()->namespaces().join("::") + "::" + e->fullName(), klass->database(), e));
    } else if(d->tok.type == Token::FLAGS)
    {
      getNextToken();
      isOfType(Token::IDENTIFIER);
      QString name = d->tok.string;
      getNextToken();
      isOfType(Token::STARTBRACKET);
      getNextToken();
      isOfType(Token::IDENTIFIER);
      QString enum_name = d->tok.string;
      getNextToken();
      isOfType(Token::ENDBRACKET);
      getNextToken();
      isOfType(Token::STARTBRACE);
      getNextToken();
      QStringList values;
      while(d->tok.type == Token::IDENTIFIER)
      {
        values.append(d->tok.string);
        getNextToken();
        if(d->tok.type == Token::COMMA)
        {
          getNextToken();
        } else if(isOfType(Token::ENDBRACE))
        {
          break;
        }
      }
      isOfType(Token::ENDBRACE);
      getNextToken();
      isOfType(Token::SEMI);
      getNextToken();
      definitions::Flags* e = new definitions::Flags(name, enum_name, values, klass);
      klass->addFlags(e);
      d->db->add(new definitions::Typedef(e->fullName(), klass->database()->namespaces().join("::") + "::" + e->fullName(), klass->database(), e));
    } else if(d->tok.type == Token::DEFINE)
    {
      getNextToken();
      QString def = d->tok.string;
      if(d->tok.type == Token::UNIQUE)
      {
        def = "unique";
      }
      getNextToken();
      if(isOfType(Token::STARTBRACKET))
      {
        getNextToken();
        if(def == "index" or def == "unique")
        {
          QList<const definitions::Field*> fields;
          while(d->tok.type != Token::END_OF_FILE)
          {
            if(isOfType(Token::IDENTIFIER))
            {
              const definitions::Field* f = 0;
              foreach(const definitions::Field* fi, klass->fields())
              {
                if(fi->name() == d->tok.string)
                {
                  f = fi;
                  break;
                }
              }
              if(f == 0)
              {
                fields.append(f);
                reportError(d->tok, "Unknown field: " + d->tok.string);
                reachNext(Token::SEMI);
                break;
              }
              fields.append(f);
              getNextToken();
              if(d->tok.type == Token::COMMA)
              {
                getNextToken();
              } else if(d->tok.type == Token::ENDBRACKET)
              {
                getNextToken();
                break;
              } else {
                reportUnexpected(d->tok);
                reachNext(Token::SEMI);
                break;
              }
            } else {
              reachNext(Token::SEMI);
              break;
            }
          }
          if(def == "index")
          {
            klass->add(new definitions::Index(fields));
          } else {
            Q_ASSERT(def == "unique");
            klass->add(new definitions::Unique(fields));
          }
        } else {
          reportError(d->tok, "Unknown definition '" + def + "'");
          reachNext(Token::SEMI);
        }
      } else {
        reachNext(Token::SEMI);
      }
      if(not isOfType(Token::SEMI))
      {
        reachNext(Token::SEMI);
      }
      getNextToken();
    } else {
      while(d->tok.type != Token::END_OF_FILE)
      {
        switch(d->tok.type)
        {
          case Token::UNIQUE:
            options |= definitions::Field::Unique;
            break;
          case Token::PARENT:
            options |= definitions::Field::Parent;
            break;
          case Token::KEY:
            options |= definitions::Field::Key;
            break;
          case Token::MANY:
            options |= definitions::Field::Many;
            break;
          case Token::CONST:
            options |= definitions::Field::Constant;
            break;
          case Token::DEPENDENT:
            options |= definitions::Field::Dependent;
            break;
          case Token::AUTO:
            options |= definitions::Field::Auto;
            break;
          case Token::REQUIRED:
            options |= definitions::Field::Required;
            break;
          case Token::INDEXED:
            options |= definitions::Field::Indexed;
            break;
          case Token::IDENTIFIER:
            goto end_of_while_double_break;
          default:
          {
            if(d->tok.isSqlType())
            {
              sqltype = parseSqlType();
            } else {
              reportUnexpected(d->tok);
              reachNext(Token::SEMI);
              goto end_of_while_double_break;
            }
          }
        }
        getNextToken();
      }
  end_of_while_double_break:
      if(isOfType(Token::IDENTIFIER))
      {
        Token identifier = d->tok;
        QString typename_ = d->tok.string;
        getNextToken();
        if(d->tok.type == Token::COLONCOLON)
        {
          getNextToken();
          isOfType(Token::IDENTIFIER);
          typename_ += "::" + d->tok.string;
          getNextToken();
        }
        QString fieldname;
        if(d->tok.type == Token::IDENTIFIER)
        {
          if(sqltype != 0)
          {
            reportUnexpected(d->tok);
            reachNext(Token::SEMI);
          } else {
            fieldname = d->tok.string;
            getNextToken();
            typedef_ = d->db->typedef_(typename_);
            if(typedef_)
            {
              sqltype = typedef_->sqltype();
            } else {
              sqltype = d->db->sqlType(typename_);
              if(not sqltype)
              {
                sqltype = klass->type(typename_);
                if(not sqltype)
                {
                  reportUnknownIndentifier(identifier);
                  reachNext(Token::SEMI);
                }
              }
            }
          }
        } else {
          fieldname = identifier.string;
        }
        if(sqltype == 0)
        {
          if(options & definitions::Field::Key)
          {
            sqltype = SqlType::integer;
          } else {
            reportError(d->tok, QString("Missing type or key for field '%1'.").arg(fieldname));
          }
        }
        if(sqltype != 0)
        {
          if(options.testFlag(definitions::Field::Parent))
          {
            if(sqltype->type() != SqlType::REFERENCE)
            {
              reportError(d->tok, "Parent type needs to be an other class.");
            } else if(static_cast<const definitions::Class*>(sqltype)->children().contains(klass)) {
              reportError(d->tok, "Can only have one parent of the same type.");
            } else {
              definitions::Class* pkl = static_cast<definitions::Class*>(const_cast<SqlType*>(sqltype));
              pkl->addChild(klass);
              checkSingleKeyInteger(d->tok, pkl);
            }
          }
          definitions::Journaled field_journaled = journaled;
          if(options.testFlag(definitions::Field::Key)
              or options.testFlag(definitions::Field::Constant))
          {
            field_journaled = definitions::NOT_JOURNALED;
          }
          QString defaultValue;
          if(d->tok.type == Token::EQUAL)
          {
            getNextToken();
            switch(d->tok.type)
            {
              case Token::STARTBRACE:
                defaultValue = d->lexer->readUntil('}');
                getNextToken();
                if(d->tok.type != Token::ENDBRACE)
                {
                  reportUnexpected(d->tok);
                }
                break;
              case Token::CONSTANT_EXPRESSION:
                defaultValue = d->tok.string;
                break;
              case Token::STRING_CONSTANT:
                defaultValue = "\"" + d->tok.string + "\"";
                break;
              default:
                reportUnexpected(d->tok);
                break;
            }
            getNextToken();
          }
          
          definitions::Field* field = new definitions::Field(fieldname, sqltype, options, typedef_, field_journaled, defaultValue, access);
          klass->add(field);
        }
        if(not isOfType(Token::SEMI))
        {
          break;
        }
        getNextToken();
      } else {
        break;
      }
    }
  }
  isOfType(Token::ENDBRACE);
  if(klass->isTree())
  {
    checkSingleKeyInteger(d->tok, klass);
  }
}

definitions::Typedef* Parser::parseTypeDef()
{
  isOfType(Token::TYPEDEF);
  getNextToken();
  if(d->tok.type == Token::IDENTIFIER or d->tok.type == Token::STRING_CONSTANT)
  {
    QString cppname = d->tok.string;
    getNextToken();
    if(d->tok.type == Token::IDENTIFIER or d->tok.type == Token::STRING_CONSTANT)
    {
      QString modelname = d->tok.string;
      getNextToken();
      const SqlType* sqlname = 0;
      switch(d->tok.type)
      {
        case Token::AS:
        {
          getNextToken();
          if(d->tok.isSqlType())
          {
            sqlname = parseSqlType();
          } else if(d->tok.type == Token::IDENTIFIER or d->tok.type == Token::STRING_CONSTANT) {
            QString sqlname_str = d->tok.string;
            sqlname = SqlType::createCustom(sqlname_str, cppname);
          }
          break;
        }
        case Token::WITH:
        {
          getNextToken();
          isOfType(Token::STRING_CONSTANT);
          sqlname = SqlType::createSqlQuery(d->tok.string, cppname);
          break;
        }
        default:
        {
          reportUnexpected(d->tok);
          return nullptr;
        }
      }
      getNextToken();
      return new definitions::Typedef(modelname, cppname, d->db, sqlname);
    } else {
      reportUnexpected(d->tok);
    }
  } else {
    reportUnexpected(d->tok);
  }
  reachNext(Token::SEMI);
  return 0;
}

void Parser::parseNamespaces()
{
  isOfType(Token::NAMESPACE);
  getNextToken();
  if(isOfType(Token::IDENTIFIER))
  {
    QStringList nss;
    
    while(d->tok.type != Token::END_OF_FILE)
    {
      isOfType(Token::IDENTIFIER);
      nss.append(d->tok.string);
      getNextToken();
      if(d->tok.type == Token::SEMI)
      {
        break;
      } else if(isOfType(Token::COLONCOLON))
      {
        getNextToken();
      } else {
        reachNext(Token::SEMI);
        return;
      }
    }
    
    d->db->setNamespaces(nss);
  } else {
    reachNext(Token::SEMI);
  }
}

const SqlType* Parser::parseSqlType()
{
  if(not d->tok.isSqlType())
  {
    reportError(d->tok, "Expecting sql type.");
    return 0;
  }
  switch(d->tok.type)
  {
  case Token::BOOLEAN:
    return SqlType::boolean;
  case Token::STRING:
    return SqlType::string;
  case Token::DATETIME:
    return SqlType::datetime;
  case Token::INT:
    return SqlType::integer;
  case Token::DOUBLE:
    return SqlType::float64;
  case Token::FLOAT:
    return SqlType::float32;
  case Token::BLOB:
    return SqlType::blob;
    default:
      qFatal("Missing SqlType");
      return 0;
  }
}

void Parser::parseMapping()
{
  isOfType(Token::MAP);
  getNextToken();
  if(isOfType(Token::IDENTIFIER))
  {
    definitions::Class* a = d->db->getOrCreateClass(d->tok.string);
    if(a)
    {
      checkSingleKeyInteger(d->tok, a);
      getNextToken();
      isOfType(Token::WITH);
      getNextToken();
      if(isOfType(Token::IDENTIFIER))
      {
        definitions::Class* b = d->db->getOrCreateClass(d->tok.string);
        if(b)
        {
          checkSingleKeyInteger(d->tok, b);
          getNextToken();
          
          definitions::Journaled journaled = definitions::NOT_JOURNALED;
          if(d->tok.type == Token::JOURNALED)
          {
            journaled = definitions::JOURNALED;
            getNextToken();
          }
          
          d->db->map(a, b, journaled);
          return;
        } else {
          reportUnknownIndentifier(d->tok);
        }
      }
    } else {
      reportUnknownIndentifier(d->tok);
    }
  }
  reachNext(Token::SEMI);
}

void Parser::parseDatabase()
{
  isOfType(Token::DATABASE);
  getNextToken();
  if(d->tok.type == Token::IMPLEMENTS)
  {
    getNextToken();
    isOfType(Token::IDENTIFIER);
    while(d->tok.type == Token::IDENTIFIER)
    {
      d->db->markImplement(interface(d->tok));
      getNextToken();
      if(d->tok.type == Token::COMMA)
      {
        getNextToken();
        isOfType(Token::IDENTIFIER);
      } else if(d->tok.type == Token::STARTBRACE)
      {
        break;
      } else {
        reportUnexpected(d->tok);
      }
    }
  }
  isOfType(Token::STARTBRACE);
  getNextToken();
  while(d->tok.type != Token::ENDBRACE)
  {
    switch(d->tok.type)
    {
      case Token::CONST:
      case Token::IDENTIFIER:
        parseObject();
        isOfType(Token::SEMI);
        getNextToken();
        break;
      case Token::ENDBRACE:
        break;
      default:
        reportUnexpected(d->tok);
        return;
    }
  }
  isOfType(Token::ENDBRACE);
  getNextToken();
}

void Parser::parseObject()
{
  bool constant = false;
  if(d->tok.type == Token::CONST)
  {
    constant = true;
    getNextToken();
  }
  const definitions::Class*   klass           = 0;
  const SqlType*              sqlType         = 0;
  const definitions::Typedef* typedefinition  = 0;
  if(d->tok.isSqlType())
  {
    sqlType = parseSqlType();
  } else {
    if(isOfType(Token::IDENTIFIER))
    {
      klass = d->db->klass(d->tok.string);
      if(not klass)
      {
        typedefinition = d->db->typedefinition(d->tok.string);
        if(not typedefinition)
        {
          reportUnknownIndentifier(d->tok);
        }
      }
    }
  }
  if(klass or sqlType or typedefinition)
  {
    getNextToken();
    if(isOfType(Token::IDENTIFIER))
    {
      QString name = d->tok.string;
      QStringList arguments;
      getNextToken();
      if(d->tok.type == Token::STARTBRACKET)
      {
        while(d->tok.type != Token::ENDBRACKET)
        {
          arguments.push_back(d->lexer->readOneArgument());
          getNextToken();
          
          if(d->tok.type == Token::COMMA)
          {
          } else if(d->tok.type != Token::ENDBRACKET)
          {
            reportUnexpected(d->tok);
            break;
          }
        }
        getNextToken();
      } else
      {
        isOfType(Token::SEMI);
      }
      if(klass)
      {
        d->db->add(new definitions::Object(klass, name, arguments));
      } else if(sqlType)
      {
        d->db->add(new definitions::Property(sqlType, name, arguments, constant));
      } else {
        Q_ASSERT(typedefinition);
        d->db->add(new definitions::Property(typedefinition, name, arguments, constant));
      }
      return;
    }
  }
  reachNext(Token::SEMI);
  
}

void Parser::parseForward()
{
  isOfType(Token::FORWARD);
  getNextToken();
  QStringList namespaces;
  QString name;
  while(isOfType(Token::IDENTIFIER))
  {
    name = d->tok.string;
    getNextToken();
    switch(d->tok.type)
    {
      case Token::COLONCOLON:
      {
        getNextToken();
        namespaces.append(name);
        name.clear();
        break;
      }
      case Token::SEMI:
      {
        d->db->add(new definitions::Forward(namespaces, name));
        return;
      }
      default:
        reportUnexpected(d->tok);
        reachNext(Token::SEMI);
        return;
    }
  }
}

void Parser::getNextToken()
{
  d->tok = d->lexer->nextToken();
}

QList<Error> Parser::errors() const
{
  return d->errors;
}

void Parser::reportError(const Token& _token, const QString& _errorMsg)
{
  Error err;
  err.line      = _token.line;
  err.column    = _token.column;
  err.filename  = d->fileName;
  err.message   = _errorMsg;
  d->errors.append(err);
}

void Parser::reportUnexpected(const Token& _token)
{
  reportError(_token, QString("Unexpected token %1").arg(Token::typeToString(_token.type)));
}

void Parser::reportUnknownIndentifier(const Token& _token)
{
  Q_ASSERT(_token.type == Token::IDENTIFIER);
  reportError(_token, QString("Unknown identifier %1").arg(_token.string));
}

bool Parser::isOfType(const Token& _token, Token::Type _type)
{
  if(_token.type == _type) return true;
  reportError(_token, QString("Expected token %1 got %2").arg(Token::typeToString(_type)).arg(Token::typeToString(_token.type)));
  return false;
}

bool Parser::isOfType(Token::Type _type)
{
  return isOfType(d->tok, _type);
}

void Parser::reachNext(Token::Type _type)
{
  while(d->tok.type != Token::END_OF_FILE and d->tok.type != _type)
  {
    getNextToken();
  }
}

void Parser::checkSingleKeyInteger(const Token& _token, const definitions::Class* _klass)
{
  QList<const definitions::Field*> aks = _klass->keys();
  if( aks.size() != 1 or (aks.size() == 1 and aks.first()->sqlType()->type() != SqlType::INTEGER))
  {
    reportError(_token, "require single integer key for class '" + _klass->name() + "'");
  }
}
