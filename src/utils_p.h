/*
 *  Copyright (c) 2013 Cyrille Berger <cberger@cberger.net>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QString>

namespace parc
{
  namespace utils
  {
    inline QString plural(const QString& _s)
    {
      if(_s[_s.size() - 1] == 's')
      {
        return _s + "es";
      }
      return _s + "s";
    }
    inline QString capitalize(const QString& _s)
    {
      QString s = _s;
      s[0] = s[0].toUpper();
      return s;
    }
    inline QString uncapitalize(const QString& _s)
    {
      QString s = _s;
      s[0] = s[0].toLower();
      return s;
    }
  }
}
